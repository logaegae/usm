-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: usm
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_signin_log`
--

DROP TABLE IF EXISTS `admin_signin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `admin_signin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` varchar(45) NOT NULL,
  `cDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(45) DEFAULT NULL,
  `userAgent` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_signin_log`
--

LOCK TABLES `admin_signin_log` WRITE;
/*!40000 ALTER TABLE `admin_signin_log` DISABLE KEYS */;
INSERT INTO `admin_signin_log` VALUES (1,'superAdmin','2019-06-10 02:56:33','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(2,'superAdmin','2019-06-10 18:54:24','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(3,'superAdmin','2019-06-10 18:54:54','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(4,'superAdmin','2019-06-10 19:00:14','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(5,'superAdmin','2019-06-10 20:30:59','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(6,'superAdmin','2019-06-10 20:37:54','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(7,'superAdmin','2019-06-10 20:38:28','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(8,'superAdmin','2019-06-10 20:38:53','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(9,'superAdmin','2019-06-10 20:39:19','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(10,'superAdmin','2019-06-10 20:39:49','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(11,'superAdmin','2019-06-10 20:40:04','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(12,'superAdmin','2019-06-10 20:42:53','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(13,'superAdmin','2019-06-10 21:50:44','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(14,'superAdmin','2019-06-10 21:52:21','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(15,'superAdmin','2019-06-10 22:12:24','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(16,'superAdmin','2019-06-10 22:13:05','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(17,'superAdmin','2019-06-11 22:24:14','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(18,'superAdmin','2019-06-11 23:20:20','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(19,'superAdmin','2019-06-11 23:34:59','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(20,'superAdmin','2019-06-13 07:32:39','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(21,'superAdmin','2019-06-13 07:33:29','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(22,'superAdmin','2019-06-13 07:33:49','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(23,'superAdmin','2019-06-13 07:34:52','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(24,'superAdmin','2019-06-14 22:57:40','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(25,'superAdmin','2019-06-15 00:32:00','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(26,'superAdmin','2019-06-15 00:36:58','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(27,'superAdmin','2019-06-15 09:28:45','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(28,'superAdmin','2019-06-15 10:57:54','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(29,'superAdmin','2019-06-15 11:12:51','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(30,'superAdmin','2019-06-16 21:02:38','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(31,'superAdmin','2019-06-16 21:20:20','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(32,'superAdmin','2019-06-17 00:19:13','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(33,'superAdmin','2019-06-17 02:16:11','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(34,'superAdmin','2019-06-18 04:20:34','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(35,'superAdmin','2019-06-18 05:23:31','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(36,'superAdmin','2019-06-22 13:17:52','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(37,'superAdmin','2019-06-22 21:12:21','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(38,'superAdmin','2019-06-23 01:57:38','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(39,'superAdmin','2019-06-23 02:06:17','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(40,'superAdmin','2019-06-23 02:07:25','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(41,'superAdmin','2019-06-23 21:02:55','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(42,'superAdmin','2019-06-25 20:21:38','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(43,'superAdmin','2019-06-25 20:46:03','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(44,'superAdmin','2019-06-25 21:28:41','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(45,'superAdmin','2019-06-25 21:47:15','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(46,'superAdmin','2019-06-25 23:22:20','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(47,'superAdmin','2019-06-26 01:56:29','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(48,'superAdmin','2019-06-29 12:35:48','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(49,'superAdmin','2019-06-29 23:02:16','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(50,'superAdmin','2019-07-13 14:34:16','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(51,'superAdmin','2019-07-13 20:44:01','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(52,'superAdmin','2019-07-13 21:56:09','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(53,'superAdmin','2019-07-13 22:14:45','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(54,'superAdmin','2019-07-13 22:16:59','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(55,'superAdmin','2019-07-13 23:30:36','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(56,'superAdmin','2019-07-14 10:13:39','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(57,'superAdmin','2019-07-14 10:26:38','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(58,'superAdmin','2019-07-14 23:18:21','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(59,'superAdmin','2019-07-16 07:38:13','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(60,'superAdmin','2019-07-17 07:47:12','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(61,'superAdmin','2019-07-17 07:54:55','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(62,'superAdmin','2019-07-21 11:32:39','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(63,'superAdmin','2019-07-21 20:30:51','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(64,'superAdmin','2019-07-21 21:09:45','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(65,'superAdmin','2019-07-24 03:26:19','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
/*!40000 ALTER TABLE `admin_signin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user`
--

DROP TABLE IF EXISTS `admin_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `admin_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `group` varchar(100) DEFAULT NULL,
  `createdBy` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `needPwChange` char(1) DEFAULT 'N',
  `lastPwUpdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `memo` varchar(1000) DEFAULT NULL,
  `updatedBy` varchar(100) DEFAULT NULL,
  `uDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user`
--

LOCK TABLES `admin_user` WRITE;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
INSERT INTO `admin_user` VALUES (1,'superAdmin','18006e2ca1c2129392c66d87334bd2452c572058d406b4e85f43c1f72def10f5','2019-06-10 02:47:50','admin','SYSTEM','logaegae@gmail.com','N','2019-06-12 22:45:48','테스트','superAdmin','2019-06-23 21:30:09');
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attach_file`
--

DROP TABLE IF EXISTS `attach_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `attach_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `processId` varchar(45) NOT NULL,
  `referenceId` int(10) unsigned NOT NULL,
  `path` varchar(500) NOT NULL,
  `cDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delYn` char(1) DEFAULT 'N',
  `createdBy` varchar(100) DEFAULT NULL,
  `fileName` varchar(200) NOT NULL,
  `originalName` varchar(200) NOT NULL,
  `mimeType` varchar(100) NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `order` int(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PATH_INDEX` (`path`,`referenceId`,`processId`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attach_file`
--

LOCK TABLES `attach_file` WRITE;
/*!40000 ALTER TABLE `attach_file` DISABLE KEYS */;
INSERT INTO `attach_file` VALUES (17,'sellerProfileAttachment',14,'/uploads/seller-profile/logaegae@gmail.com/36d6779ac47fe1eaa5477513880d44b91563230327552.txt','2019-07-16 07:38:47','N','superAdmin','36d6779ac47fe1eaa5477513880d44b91563230327552.txt','expressions.txt','text/plain',23144,NULL),(39,'sellerProductImage',6,'/uploads/seller-profile/logaegae@gmail.com/194c70743c91f4d7247a8d54de28da741564840849723.jpeg','2019-08-05 00:48:54','N','seller-profile/logaegae@gmail.com','194c70743c91f4d7247a8d54de28da741564840849723.jpeg','boudewijn-huysmans-757457-unsplash.jpg','image/jpeg',1350599,1),(40,'sellerProductDetailImage',6,'/uploads/seller-profile/logaegae@gmail.com/1ebe09f780f5bd3aecc1427a2d5d88271564840855448.jpeg','2019-08-05 00:48:55','N','seller-profile/logaegae@gmail.com','1ebe09f780f5bd3aecc1427a2d5d88271564840855448.jpeg','chuttersnap-758539-unsplash.jpg','image/jpeg',2246513,1);
/*!40000 ALTER TABLE `attach_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_order`
--

DROP TABLE IF EXISTS `customer_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderLogId` int(10) unsigned NOT NULL COMMENT '주문상태',
  `status` varchar(45) NOT NULL COMMENT '최종 상태코드',
  `email` varchar(100) DEFAULT NULL COMMENT '이메일',
  `name1` varchar(45) DEFAULT NULL COMMENT '이름',
  `platformChannel` varchar(100) DEFAULT NULL COMMENT '주문자 채널',
  `platformOrderId` varchar(100) DEFAULT NULL COMMENT '주문자 플랫폼 주문번호',
  `platformUserId` varchar(100) DEFAULT NULL COMMENT '주문자 플랫폼 아이디',
  `name2` varchar(45) NOT NULL COMMENT '수령자명',
  `zipcode` int(10) unsigned DEFAULT NULL COMMENT '우편번호',
  `address1` varchar(100) DEFAULT NULL COMMENT '주소1',
  `address2` varchar(100) DEFAULT NULL COMMENT '주소2',
  `phone1` varchar(45) DEFAULT NULL COMMENT '연락처1',
  `phone2` varchar(45) DEFAULT NULL COMMENT '연락처2',
  `deriveryMemo` varchar(200) DEFAULT NULL COMMENT '배송시 메모',
  `payType` varchar(45) DEFAULT NULL COMMENT '결제방식',
  `payStatus` varchar(45) DEFAULT NULL COMMENT '결제상태',
  `price` bigint(12) unsigned DEFAULT NULL COMMENT '총 결제 금액',
  `totalDiscount` bigint(12) unsigned DEFAULT NULL COMMENT '총 할인 금액',
  `totalDiscountDetail` varchar(1000) NOT NULL COMMENT '총 할인 내용',
  `totalCost` bigint(12) unsigned DEFAULT NULL COMMENT '총 부가결제금액 금액',
  `totalCostDetail` varchar(1000) DEFAULT NULL COMMENT '총 부가결제금액 내용',
  `deliveryCompany` varchar(50) DEFAULT NULL COMMENT '배송택배사',
  `deliveryCode` bigint(20) unsigned DEFAULT NULL COMMENT '송장번호',
  `requirement` varchar(4000) DEFAULT NULL COMMENT '고객요청사항',
  `memo` varchar(4000) DEFAULT NULL COMMENT '고객상담메모',
  `cashReceiptYn` char(1) DEFAULT 'N' COMMENT '현금영수증발행여부',
  `taxReceiptYn` char(1) DEFAULT 'N' COMMENT '세금계산서발행여부',
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `uDate` datetime DEFAULT NULL,
  `delYn` char(1) DEFAULT 'N',
  `createdBy` varchar(100) DEFAULT NULL,
  `updatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `INDEX` (`orderLogId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_order`
--

LOCK TABLES `customer_order` WRITE;
/*!40000 ALTER TABLE `customer_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_order_product`
--

DROP TABLE IF EXISTS `customer_order_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer_order_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customerOrderId` int(10) unsigned NOT NULL COMMENT '주문내용',
  `productId` int(10) unsigned NOT NULL COMMENT '주문 상품',
  `ea` int(10) unsigned DEFAULT NULL COMMENT '주문갯수',
  `status` varchar(45) DEFAULT NULL,
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `uDate` datetime DEFAULT NULL,
  `delYn` char(1) DEFAULT 'N',
  `createdBy` varchar(100) DEFAULT NULL,
  `updatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `INDEX` (`customerOrderId`,`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_order_product`
--

LOCK TABLES `customer_order_product` WRITE;
/*!40000 ALTER TABLE `customer_order_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_order_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dormant_seller_user`
--

DROP TABLE IF EXISTS `dormant_seller_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dormant_seller_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL COMMENT '이메일',
  `companyName` varchar(100) NOT NULL COMMENT '상호',
  `password` varchar(100) DEFAULT NULL COMMENT '비밀번호',
  `isMailCertified` char(1) DEFAULT 'N' COMMENT '메일인증여부',
  `isDormant` char(1) DEFAULT 'N' COMMENT '휴면계정여부',
  `sentDormantMail` char(1) DEFAULT 'N' COMMENT '휴면계정메일발송여부',
  `needPwChange` char(1) DEFAULT 'N' COMMENT '비밀번호변경필요여부',
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일',
  `from` varchar(50) DEFAULT NULL,
  `dormantDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '휴면전환일',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `INDEX` (`email`,`password`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='판매 회원 휴면 계정';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dormant_seller_user`
--

LOCK TABLES `dormant_seller_user` WRITE;
/*!40000 ALTER TABLE `dormant_seller_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `dormant_seller_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_log`
--

DROP TABLE IF EXISTS `mail_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mail_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `toAddress` varchar(100) NOT NULL COMMENT '발송주소',
  `code` varchar(200) NOT NULL COMMENT '인증코드',
  `cDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '생성일',
  `expireDate` datetime NOT NULL COMMENT '만료일',
  `isUsed` char(1) DEFAULT 'N' COMMENT '사용여부',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_log`
--

LOCK TABLES `mail_log` WRITE;
/*!40000 ALTER TABLE `mail_log` DISABLE KEYS */;
INSERT INTO `mail_log` VALUES (1,'logaegae@gmail.com','1020147','2019-06-05 05:06:50','2019-06-05 05:11:50','N'),(2,'logaegae@gmail.com','941857','2019-06-05 05:24:19','2019-06-05 05:29:19','Y'),(3,'logaegae@gmail.com','711298','2019-06-06 15:22:32','2019-06-07 15:22:32','N'),(4,'logaegae@gmail.com','501045','2019-06-06 15:43:54','2019-06-07 15:43:54','N'),(5,'logaegae@gmail.com','451137','2019-06-07 00:12:12','2019-06-08 00:12:12','N'),(6,'logaegae@gmail.com','452766','2019-06-07 00:45:47','2019-06-08 00:45:47','Y'),(7,'logaegae@gmail.com','401cc5cd2adb04357280fe930d89a1cde3aa6ec55629acf8fadecfffa9cd047f','2019-06-07 00:59:14','2019-06-08 00:59:14','N'),(8,'logaegae@gmail.com','df3f2b69c54d8ca6864c2bdab4cc4fad8ca8fd2e011e90494dae3ff8a276f58f','2019-06-07 01:44:24','2019-06-08 01:44:24','Y'),(9,'logaegae@gmail.com','265823','2019-06-25 16:56:50','2019-06-25 17:01:50','N'),(10,'logaegae@gmail.com','402696','2019-06-25 17:01:48','2019-06-25 17:06:48','N'),(11,'logaegae@gmail.com','622809','2019-06-25 17:09:06','2019-06-25 17:14:06','N'),(12,'logaegae@gmail.com','837536','2019-06-25 17:10:26','2019-06-25 17:15:26','N'),(13,'logaegae@gmail.com','558629','2019-06-25 17:18:08','2019-06-25 17:23:08','N'),(14,'logaegae@gmail.com','453655','2019-06-25 17:26:50','2019-06-25 17:31:50','N'),(15,'logaegae@gmail.com','107838','2019-06-25 17:38:17','2019-06-25 17:43:17','N'),(16,'logaegae@gmail.com','777010','2019-06-26 02:44:18','2019-06-26 02:49:18','Y');
/*!40000 ALTER TABLE `mail_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice_item_group`
--

DROP TABLE IF EXISTS `notice_item_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notice_item_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `json` text NOT NULL,
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `createdBy` varchar(100) DEFAULT NULL,
  `uDate` datetime DEFAULT NULL,
  `updatedBy` varchar(100) DEFAULT NULL,
  `delYn` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice_item_group`
--

LOCK TABLES `notice_item_group` WRITE;
/*!40000 ALTER TABLE `notice_item_group` DISABLE KEYS */;
INSERT INTO `notice_item_group` VALUES (1,'의류','[{\"NoticeItemCode\":\"1-1\",\"NoticeItemGroupNo\":1,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품소재\",\"GuideText\":\"섬유의 조성 또는 혼용률을 백분율로 입력, 기능성인 경우 성적서 또는 허가서 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"1-10\",\"NoticeItemGroupNo\":1,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"1-2\",\"NoticeItemGroupNo\":1,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"색상\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"1-3\",\"NoticeItemGroupNo\":1,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"치수\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"1-4\",\"NoticeItemGroupNo\":1,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"1-5\",\"NoticeItemGroupNo\":1,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"1-6\",\"NoticeItemGroupNo\":1,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"세탁방법 및 취급시 주의사항\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"1-7\",\"NoticeItemGroupNo\":1,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조연월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"1-8\",\"NoticeItemGroupNo\":1,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"1-9\",\"NoticeItemGroupNo\":1,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:11:19',NULL,NULL,NULL,'N'),(2,'구두/신발','[{\"NoticeItemCode\":\"2-1\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품의 주소재\",\"GuideText\":\"운동화인 경우에는 겉감, 안감을 구분하여 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"2-2\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"색상\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"2-3\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"치수\",\"GuideText\":\"발길이 : 해외사이즈 입력시 국내사이즈 병행입력(mm)\\r\\n굽높이 : 굽 재료를 사용하는 여성화에 한함 (cm)\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"2-4\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"2-5\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"2-6\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취급시 주의사항\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"2-7\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"2-8\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"2-9\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:11:19',NULL,NULL,NULL,'N'),(3,'가방','[{\"NoticeItemCode\":\"3-1\",\"NoticeItemGroupNo\":3,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"종류\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"3-10\",\"NoticeItemGroupNo\":3,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"3-2\",\"NoticeItemGroupNo\":3,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소재\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"3-3\",\"NoticeItemGroupNo\":3,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"색상\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"3-4\",\"NoticeItemGroupNo\":3,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"3-5\",\"NoticeItemGroupNo\":3,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"3-6\",\"NoticeItemGroupNo\":3,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"3-7\",\"NoticeItemGroupNo\":3,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취급시 주의사항\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"3-8\",\"NoticeItemGroupNo\":3,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"3-9\",\"NoticeItemGroupNo\":3,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:11:19',NULL,NULL,NULL,'N'),(4,'패션잡화(모자/밸트/악세서리)','[{\"NoticeItemCode\":\"4-1\",\"NoticeItemGroupNo\":4,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"종류\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"4-2\",\"NoticeItemGroupNo\":4,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소재\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"4-3\",\"NoticeItemGroupNo\":4,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"치수\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"4-4\",\"NoticeItemGroupNo\":4,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"4-5\",\"NoticeItemGroupNo\":4,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"4-6\",\"NoticeItemGroupNo\":4,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취급시 주의사항\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"4-7\",\"NoticeItemGroupNo\":4,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"4-8\",\"NoticeItemGroupNo\":4,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"4-9\",\"NoticeItemGroupNo\":4,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:12:36',NULL,NULL,NULL,'N'),(5,'침구류/커튼','[{\"NoticeItemCode\":\"5-1\",\"NoticeItemGroupNo\":5,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품소재\",\"GuideText\":\"섬유의 조성 또는 혼용률을 백분율로 입력, 충전재를 사용한 제품은 충전재를 함께 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"5-10\",\"NoticeItemGroupNo\":5,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"5-2\",\"NoticeItemGroupNo\":5,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"색상\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"5-3\",\"NoticeItemGroupNo\":5,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"치수\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"5-4\",\"NoticeItemGroupNo\":5,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품구성\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"5-5\",\"NoticeItemGroupNo\":5,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"5-6\",\"NoticeItemGroupNo\":5,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"5-7\",\"NoticeItemGroupNo\":5,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"세탁방법 및 취급시 주의사항\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"5-8\",\"NoticeItemGroupNo\":5,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"5-9\",\"NoticeItemGroupNo\":5,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:12:36',NULL,NULL,NULL,'N'),(6,'가구(침대/소파/싱크대/DIY제품)','[{\"NoticeItemCode\":\"6-1\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-10\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-11\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-12\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-2\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"품질경영 및 공산품안전관리법 상 안전·품질표시대상 공산품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-3\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"색상\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-4\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"구성품\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-5\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주요소재\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-6\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\\r\\n구성품 별 제조자가 다른 경우 각 구성품의 제조자, 수입자 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-7\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"구성품 별 제조국이 다른 경우 각 구성품의 제조국\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-8\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"6-9\",\"NoticeItemGroupNo\":6,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"배송/설치비용\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:12:36',NULL,NULL,NULL,'N'),(7,'영상가전(TV류)','[{\"NoticeItemCode\":\"7-1\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-10\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-11\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-12\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"에너지소비효율등급\",\"GuideText\":\"에너지이용합리화법 상 외무대상상품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-2\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"전기용품 안전관리법 상 안전인증대상전기용품, 안전확인대상전기용품,공급자적합성확인대상전기용품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-3\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"정격전압/소비전력\",\"GuideText\":\"\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-4\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-5\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-6\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-7\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기\",\"GuideText\":\"형태포함\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-8\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"화면사양\",\"GuideText\":\"크기, 해상도, 화면비율 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"7-9\",\"NoticeItemGroupNo\":7,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:15:14',NULL,NULL,NULL,'N'),(8,'가전용 전기제품(냉장고/세탁기/식기세척기/전자레인지)','[{\"NoticeItemCode\":\"8-1\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"8-10\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"8-11\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"에너지소비효율등급\",\"GuideText\":\"에너지이용합리화법 상 외무대상상품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"8-2\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"전기용품 안전관리법 상 안전인증대상전기용품, 안전확인대상전기용품,공급자적합성확인대상전기용품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"8-3\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"정격전압/소비전력\",\"GuideText\":\"\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"8-4\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"8-5\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"8-6\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"8-7\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기\",\"GuideText\":\"용량(중량), 형태포함\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"8-8\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"8-9\",\"NoticeItemGroupNo\":8,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:15:14',NULL,NULL,NULL,'N'),(9,'계절가전(에어컨/온풍기)','[{\"NoticeItemCode\":\"9-1\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-10\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-11\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-12\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-13\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"에너지소비효율등급\",\"GuideText\":\"에너지이용합리화법 상 외무대상상품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-2\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"전기용품 안전관리법 상 안전인증대상전기용품, 안전확인대상전기용품,공급자적합성확인대상전기용품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-3\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"정격전압/소비전력\",\"GuideText\":\"\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-4\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-5\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-6\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-7\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기\",\"GuideText\":\"형태 및 실외기 포함\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-8\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"냉난방면적\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"9-9\",\"NoticeItemGroupNo\":9,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"추가설치비용\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:15:14',NULL,NULL,NULL,'N'),(10,'사무용기기','[{\"NoticeItemCode\":\"10-1\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-10\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-11\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-12\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"에너지소비효율등급\",\"GuideText\":\"에너지이용합리화법 상 외무대상상품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-2\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"전파법 상 정보통신기자재등의 적합성평가인증대상 상품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-3\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"정격전압/소비전력\",\"GuideText\":\"\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-4\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-5\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-6\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-7\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기/무게\",\"GuideText\":\"무게는 노트북에 한함\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-8\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주요사양\",\"GuideText\":\"컴퓨터와 노트북의 경우 성능, 용량(중량), 운영체제 포함여부 등 / 프린터의 경우 인쇄 속도 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"10-9\",\"NoticeItemGroupNo\":10,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:15:14',NULL,NULL,NULL,'N'),(11,'광학기기(디지털카메라/캠코더)','[{\"NoticeItemCode\":\"11-1\",\"NoticeItemGroupNo\":11,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"11-10\",\"NoticeItemGroupNo\":11,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"11-2\",\"NoticeItemGroupNo\":11,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"전파법 상 정보통신기자재등의 적합성평가인증대상 상품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"11-3\",\"NoticeItemGroupNo\":11,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"11-4\",\"NoticeItemGroupNo\":11,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"11-5\",\"NoticeItemGroupNo\":11,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"11-6\",\"NoticeItemGroupNo\":11,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기/무게\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"11-7\",\"NoticeItemGroupNo\":11,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주요 사양\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"11-8\",\"NoticeItemGroupNo\":11,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"11-9\",\"NoticeItemGroupNo\":11,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:15:14',NULL,NULL,NULL,'N'),(12,'소형전자(MP3/전자사전 등)','[{\"NoticeItemCode\":\"12-1\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"12-10\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"12-11\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"12-2\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"전파법 상 인증대상상품에 한함, MIC 인증 필 혼용 가능\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"12-3\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"정격전압/소비전력\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"12-4\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"12-5\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"12-6\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"12-7\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기/무게\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"12-8\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주요 사양\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"12-9\",\"NoticeItemGroupNo\":12,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:18:32',NULL,NULL,NULL,'N'),(13,'휴대폰','[{\"NoticeItemCode\":\"13-1\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-10\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주요사양\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-11\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-12\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-13\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-2\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"전파법 상 정보통신기자재등의 적합성평가인증대상 상품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-3\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-4\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-5\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-6\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기/무게\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-7\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"이동통신사\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-8\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"가입절차\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"13-9\",\"NoticeItemGroupNo\":13,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자의 추가적인 부담사항\",\"GuideText\":\"가입비, 유심카드 구입비 등 추가로 부담하여야 할 금액, 부가서비스, 의무사용기간, 위약금 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:18:32',NULL,NULL,NULL,'N'),(14,'네비게이션','[{\"NoticeItemCode\":\"14-1\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-10\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-11\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-12\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-2\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"전파법 상 정보통신기자재등의 적합성평가인증대상 상품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-3\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"정격전압, 소비전력\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-4\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-5\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-6\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-7\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기/무게\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-8\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주요 사양\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"14-9\",\"NoticeItemGroupNo\":14,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"맵 업데이트 비용 및 무상기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:18:32',NULL,NULL,NULL,'N'),(15,'자동차용품','[{\"NoticeItemCode\":\"15-1\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"15-10\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"15-11\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품사용으로 인한 위험 및 유의사항\",\"GuideText\":\"연료절감장치에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"15-2\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"15-3\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"자동차관리법에 따른 자기인증 대상 자동차부품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"15-4\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"15-5\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"15-6\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"15-7\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"적용차종\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"15-8\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"15-9\",\"NoticeItemGroupNo\":15,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:18:32',NULL,NULL,NULL,'N'),(16,'의료기기','[{\"NoticeItemCode\":\"16-1\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-10\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취급시 주의사항\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-11\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-12\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-13\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-2\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"의료기기법상 허가번호\",\"GuideText\":\"의료기기법상 허가/신고 대상 의료기기에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-4\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"전기용품 안전 관리법상 안전인증 또는 자율안전확인 대상 전기용품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-5\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"정격전압/소비전력\",\"GuideText\":\"전기용품에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-6\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-7\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-8\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"16-9\",\"NoticeItemGroupNo\":16,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품의 사용목적 및 사용방법\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:18:32',NULL,NULL,NULL,'N'),(17,'주방용품','[{\"NoticeItemCode\":\"17-1\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"17-10\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"17-11\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"17-2\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"재질\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"17-3\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"구성품\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"17-4\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"17-5\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"17-6\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"17-7\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"17-8\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"수입 기구/용기 \",\"GuideText\":\"식품위생법에 따른 수입 기구/용기의 경우 식품위생법에 따른 수입신고를 필함으로 입력\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"17-9\",\"NoticeItemGroupNo\":17,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:18:32',NULL,NULL,NULL,'N'),(18,'화장품','[{\"NoticeItemCode\":\"18-1\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"용량(중량) 또는 중량\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-10\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-11\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-12\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-2\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품 주요 사양\",\"GuideText\":\"피부타입, 색상(호,번) 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-3\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"사용기한 또는 개봉 후 사용기간\",\"GuideText\":\"개봉 후 사용기간을 기재할 경우에는 제조연월일을 병행입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-4\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"사용방법\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-5\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조업자 및 책임판매업자\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-6\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-7\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"화장품법에 따라 기재/표시하여야 하는 모든 성분\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-8\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"기능성 화장품 심사 필 유무\",\"GuideText\":\"기능성 화장품의 경우 화장품법에 따른 식품의약품안전처 심사 필 유무 입력  (미백, 주름개선, 자외선차단 등)\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"18-9\",\"NoticeItemGroupNo\":18,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"사용할 때 주의사항\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:18:32',NULL,NULL,NULL,'N'),(19,'귀금속/보석/시계류','[{\"NoticeItemCode\":\"19-1\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소재/순도/밴드재질\",\"GuideText\":\"시계의 경우에만 밴드재질도 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"19-10\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"19-11\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"19-2\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"중량\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"19-3\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"19-4\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"원산지와 가공지 등이 다를 경우 함께 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"19-5\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"치수\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"19-6\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"착용 시 주의사항\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"19-7\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주요 사양\",\"GuideText\":\"귀금속, 보석류 - 등급\\r\\n시계 - 기능, 방수 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"19-8\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"보증서 제공여부\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"19-9\",\"NoticeItemGroupNo\":19,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:18:32',NULL,NULL,NULL,'N'),(20,'식품(농·축·수산물)','[{\"NoticeItemCode\":\"20-1\",\"NoticeItemGroupNo\":20,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"포장단위별 용량(중량)/수량/크기\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"20-10\",\"NoticeItemGroupNo\":20,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"20-2\",\"NoticeItemGroupNo\":20,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"생산자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"20-3\",\"NoticeItemGroupNo\":20,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"원산지\",\"GuideText\":\"농수산물의 원산지 표시에 관한 법률에 따른 원산지로 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"20-4\",\"NoticeItemGroupNo\":20,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조연월일\",\"GuideText\":\"포장일 또는 생산연도로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"20-5\",\"NoticeItemGroupNo\":20,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"유통기한 또는 품질유지기한\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"20-6\",\"NoticeItemGroupNo\":20,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"관련법상 표시사항\",\"GuideText\":\"농산물 - 농수산물 품질관리법상 유전자변형농산물 표시, 지리적 표시 축산물 - 축산법에 따른 등급 표시, 쇠고기의 경우 이력관리에 따른 표시 유무 수산물 - 농수산물품질관리법상 유전자변형수산물 표시, 지리적 표시 수입식품 - 식품위생법에 따른 수입신고를 필함 의 문구 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"20-7\",\"NoticeItemGroupNo\":20,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"상품구성\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"20-8\",\"NoticeItemGroupNo\":20,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"보관방법 또는 취급방법\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"20-9\",\"NoticeItemGroupNo\":20,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:18:32',NULL,NULL,NULL,'N'),(21,'가공식품','[{\"NoticeItemCode\":\"21-1\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"식품의 유형\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-10\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"수입식품 여부\",\"GuideText\":\"수입식품에 해당하는 경우 식품위생법에 따른 수입신고를 필함의 문구 입력\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-11\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-12\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-2\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"생산자 및 소재지\",\"GuideText\":\"수입품의 경우 생산자, 수입자 및 제조국\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-3\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조연월일\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-4\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"유통기한 또는 품질유지기한\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-5\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"포장단위별 용량(중량)/수량\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-6\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"원재료명 및 함량\",\"GuideText\":\"농수산물의 원산지 표시에 관한 법률에 따른 원산지로 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-7\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"영양성분\",\"GuideText\":\"식품위생법에 따른 영양성분 표시대상 식품에 한함\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-8\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"유전자변형식품 유무\",\"GuideText\":\"유전자변형식품에 해당하는 경우 표시 필요\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"21-9\",\"NoticeItemGroupNo\":21,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"표시광고 사전심의 유무\",\"GuideText\":\"영유아식 또는 체중조절식품 등에 해당하는 경우 표시광고 사전심의필 입력 및 부작용 발생 가능성\",\"IsEssencial\":false,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(22,'건강기능식품','[{\"NoticeItemCode\":\"22-1\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"식품의 유형\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-10\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"유전자변형건강기능식품 유무\",\"GuideText\":\"유전자변형건강기능식품에 해당하는 경우 표시 필요\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-11\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"표시광고 사전심의 유무\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-12\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"수입식품 여부\",\"GuideText\":\"수입식품에 해당하는 경우 건강기능식품에 따른 수입신고를 필함의 문구 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-13\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-14\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-2\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조업소의 명칭과 소재지\",\"GuideText\":\"수입품의 경우 수입업소명,제조업소명 및 수출국명\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-3\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조연월일\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-4\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"유통기한 또는 품질유지기한\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-5\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"포장단위별 용량(중량)/수량\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-6\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"원재료명 및 함량\",\"GuideText\":\"농수산물의 원산지 표시에 관한 법률에 따른 원산지로 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-7\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"영양성분\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-8\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"기능정보\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"22-9\",\"NoticeItemGroupNo\":22,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"섭취량, 섭취방법, 섭취시 주의사항 및 부작용 발생 가능성\",\"GuideText\":\"질병의 예방 및 치료를 위한 의약품이 아니라는 내용도 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(23,'영유아용품','[{\"NoticeItemCode\":\"23-1\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-10\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취급방법 및 취급시 주의사항,안전표시\",\"GuideText\":\"주의, 경고 등 \",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-11\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-12\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-13\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-2\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"KC 인증 필 유무\",\"GuideText\":\"어린이제품 안전 특별법 상 안전인증대상어린이제품, 안전확인대상어린이제품, 공급자적합성확인대상어린이제품에 대한 KC인증 필 유무\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-3\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기/중량\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-4\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"색상\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-5\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"재질\",\"GuideText\":\"섬유의 경우 혼용률\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-6\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"사용연령 또는 체중범위\",\"GuideText\":\"어린이제품 안전 특별법에 따라 표시해야 하는 사항은 반드시 표기\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-7\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-8\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"23-9\",\"NoticeItemGroupNo\":23,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(24,'악기','[{\"NoticeItemCode\":\"24-1\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-10\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-11\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-12\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-2\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-3\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"색상\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-4\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"재질\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-5\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품 구성\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-6\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-7\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-8\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"24-9\",\"NoticeItemGroupNo\":24,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"상품별 세부 사양\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(25,'스포츠용품','[{\"NoticeItemCode\":\"25-1\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-10\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-11\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-12\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-2\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기/중량\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-3\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"색상\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-4\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"재질\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-5\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품 구성\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-6\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"동일모델의 출시년월\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-7\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-8\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"25-9\",\"NoticeItemGroupNo\":25,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"상품별 세부 사양\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(26,'서적','[{\"NoticeItemCode\":\"26-1\",\"NoticeItemGroupNo\":26,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"도서명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"26-2\",\"NoticeItemGroupNo\":26,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"저자/출판사\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"26-3\",\"NoticeItemGroupNo\":26,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"크기\",\"GuideText\":\"전자책의 경우 파일의 용량을 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"26-4\",\"NoticeItemGroupNo\":26,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"쪽수\",\"GuideText\":\"전자착의 경우 제외\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"26-5\",\"NoticeItemGroupNo\":26,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품 구성\",\"GuideText\":\"전집 또는 세트일 경우 낱권 수성, CD 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"26-6\",\"NoticeItemGroupNo\":26,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"출간일\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"26-7\",\"NoticeItemGroupNo\":26,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"목차 또는 책소개\",\"GuideText\":\"아동용 학습교재의 경우 사용연령을 포함\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"26-8\",\"NoticeItemGroupNo\":26,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(27,'호텔/펜션 예약','[{\"NoticeItemCode\":\"27-1\",\"NoticeItemGroupNo\":27,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"국가 또는 지역명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"27-2\",\"NoticeItemGroupNo\":27,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"숙소형태\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"27-3\",\"NoticeItemGroupNo\":27,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"등급, 객실타입\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"27-4\",\"NoticeItemGroupNo\":27,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"사용가능 인원, 인원 추가 시 비용\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"27-5\",\"NoticeItemGroupNo\":27,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"부대시설/제공서비스 \",\"GuideText\":\"조식 등 \",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"27-6\",\"NoticeItemGroupNo\":27,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취소 규정\",\"GuideText\":\"환불, 위약금 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"27-7\",\"NoticeItemGroupNo\":27,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"예약담당 연락처\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"27-8\",\"NoticeItemGroupNo\":27,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(28,'여행상품','[{\"NoticeItemCode\":\"28-1\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"여행사\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"28-10\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"예약담당 연락처\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"28-11\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"28-2\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"이용항공편\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"28-3\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"여행기간 및 일정\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"28-4\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"총 예정 인원, 출발 가능 인원\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"28-5\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"숙박정보\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"28-6\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"여행상품 가격\",\"GuideText\":\"유류할증료, 공항이용료, 전쟁보험료, 관광진흥개발기금, 운송요금, 숙박요금, 식사요금, 가이드경비, 여행자보험료, 현지관광입장료 등 소비자가 특정 여행상품을 선택할 경우 부담해야 하는 모든 경비가 포함되어야 함. 다만, 가이드 경비를 현지에서 지불하여야 하는 경우 별도로 그 금액을 표시할 수 있으며, 현지에서 별도로 지불해야 한다는 점을 표시하여야 함\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"28-7\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"선택경비 유무 및 세부 내용\",\"GuideText\":\"*선택경비 유무 등- 선택관광경비 등 현지에서 개별 구매자의 필요나 선택에 의하여 지출하게 되는 경비가 있는지 여부 및 소비자의 선택에 따라 자유롭게 지불할 수 있다는 점을 함께 표시\\r\\n*선택관광 및 대체일정 - 선택관광 경비금액 및 선택관광을 선택하지 않을 경우의 대체 일정을 함께 표시\\r\\n*가이드 팁 - 가이드 팁에 대하여 기재할 경우 가이드 경비와 구별하여 자유롭게 지불 여부를 결정할 수 있음을 표시. 다만, 정액으로 지불을 권장하는등 소비자가 필수적으로 지불하여야 하는 경비인 것처럼 오인하지 않도록 표시\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"28-8\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취소 규정\",\"GuideText\":\"환불, 위약금에 관한 사항들 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"28-9\",\"NoticeItemGroupNo\":28,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"여행경보단계\",\"GuideText\":\"해외여행의 경우 외교부가 지정하는 여행경보단계 입력\",\"IsEssencial\":false,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(29,'항공권','[{\"NoticeItemCode\":\"29-1\",\"NoticeItemGroupNo\":29,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"요금조건, 왕복/편도여부\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"29-2\",\"NoticeItemGroupNo\":29,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"유효기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"29-3\",\"NoticeItemGroupNo\":29,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제한사항\",\"GuideText\":\"출발일, 귀국일 변경가능 여부 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"29-4\",\"NoticeItemGroupNo\":29,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"티켓수령방법\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"29-5\",\"NoticeItemGroupNo\":29,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"좌석종류\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"29-6\",\"NoticeItemGroupNo\":29,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"가격에 포함되지 않은 내역 및 금액\",\"GuideText\":\"유류할증료, 공항이용료 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"29-7\",\"NoticeItemGroupNo\":29,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취소 규정\",\"GuideText\":\"환불, 위약금 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"29-8\",\"NoticeItemGroupNo\":29,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"예약담당 연락처\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"29-9\",\"NoticeItemGroupNo\":29,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(30,'자동차 대여 서비스','[{\"NoticeItemCode\":\"30-1\",\"NoticeItemGroupNo\":30,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"차종\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"30-2\",\"NoticeItemGroupNo\":30,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소유권 이전 조건\",\"GuideText\":\"소유권이 이전되는 경우에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"30-3\",\"NoticeItemGroupNo\":30,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"추가 선택 시 비용\",\"GuideText\":\"자차면책제도, 내비게이션 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"30-4\",\"NoticeItemGroupNo\":30,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"차량 반환 시 연료대금 정산 방법\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"30-5\",\"NoticeItemGroupNo\":30,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"차량의 고장/훼손 시 소비자 책임\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"30-6\",\"NoticeItemGroupNo\":30,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"예약 취소 또는 중도 해약 시 환불 기준\",\"GuideText\":\"예약 취소 또는 중도 해약 시 환불 기준\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"30-7\",\"NoticeItemGroupNo\":30,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"30-8\",\"NoticeItemGroupNo\":30,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(31,'물품대여 서비스(정수기/에어컨/공기청정기 등)','[{\"NoticeItemCode\":\"31-1\",\"NoticeItemGroupNo\":31,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"31-2\",\"NoticeItemGroupNo\":31,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소유권 이전 조건\",\"GuideText\":\"소유권이 이전되는 경우에 한하며, 소유권 이전에 필요한 렌탈기간 또는 총 렌탈금액 등 요건을 구체적으로 명시\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"31-3\",\"NoticeItemGroupNo\":31,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"유지보수 조건\",\"GuideText\":\"점검/필터교환 주기, 추가비용 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"31-4\",\"NoticeItemGroupNo\":31,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자 책임\",\"GuideText\":\"상품의 고장/분실/훼손 시 소비자 책임\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"31-5\",\"NoticeItemGroupNo\":31,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"중도 해약 시 환불 기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"31-6\",\"NoticeItemGroupNo\":31,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품 사양\",\"GuideText\":\"용량, 소비전력 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"31-7\",\"NoticeItemGroupNo\":31,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"31-8\",\"NoticeItemGroupNo\":31,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(32,'물품대여 서비스(서적/유아용품/행사용품 등)','[{\"NoticeItemCode\":\"32-1\",\"NoticeItemGroupNo\":32,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"32-2\",\"NoticeItemGroupNo\":32,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소유권 이전 조건\",\"GuideText\":\"소유권이 이전되는 경우에 한하며, 소유권 이전에 필요한 렌탈기간 또는 총 렌탈금액 등 요건을 구체적으로 명시\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"32-3\",\"NoticeItemGroupNo\":32,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자 책임\",\"GuideText\":\"상품의 고장/분실/훼손 시 소비자 책임\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"32-4\",\"NoticeItemGroupNo\":32,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"중도 해약 시 환불 기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"32-5\",\"NoticeItemGroupNo\":32,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"32-6\",\"NoticeItemGroupNo\":32,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:22:52',NULL,NULL,NULL,'N'),(33,'디지털콘텐츠(음원/게임/인터넷강의 등)','[{\"NoticeItemCode\":\"33-1\",\"NoticeItemGroupNo\":33,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제작자 또는 공급자\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"33-2\",\"NoticeItemGroupNo\":33,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"이용조건/이용기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"33-3\",\"NoticeItemGroupNo\":33,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"상품 제공 방식\",\"GuideText\":\"CD, 다운로드, 실시간 스트리밍 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"33-4\",\"NoticeItemGroupNo\":33,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"최소 시스템 사양, 필수 소프트웨어\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"33-5\",\"NoticeItemGroupNo\":33,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"청약철회 또는 계약해제/해지\",\"GuideText\":\"청약철회 또는 계약해제/해지에 따른 효과\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"33-6\",\"NoticeItemGroupNo\":33,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"33-7\",\"NoticeItemGroupNo\":33,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:25:37',NULL,NULL,NULL,'N'),(34,'상품권쿠폰','[{\"NoticeItemCode\":\"34-1\",\"NoticeItemGroupNo\":34,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"발행자\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"34-2\",\"NoticeItemGroupNo\":34,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"유효기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"34-3\",\"NoticeItemGroupNo\":34,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"이용조건\",\"GuideText\":\"유효기간 경과 시 보상기준, 사용제한품목 및 기간 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"34-4\",\"NoticeItemGroupNo\":34,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"이용 가능 매장\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"34-5\",\"NoticeItemGroupNo\":34,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"잔액 환급 조건\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"34-6\",\"NoticeItemGroupNo\":34,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"34-7\",\"NoticeItemGroupNo\":34,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:25:37',NULL,NULL,NULL,'N'),(35,'기타','[{\"NoticeItemCode\":\"35-1\",\"NoticeItemGroupNo\":35,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품명 및 모델명\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"35-2\",\"NoticeItemGroupNo\":35,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"허가 관련\",\"GuideText\":\"법에 의한 인증/허가 등을 받았음을 확인할 수 있는 경우 그에 대한 사항\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"35-3\",\"NoticeItemGroupNo\":35,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국 또는 원산지\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"35-4\",\"NoticeItemGroupNo\":35,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"35-5\",\"NoticeItemGroupNo\":35,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"관련 연락처\",\"GuideText\":\"A/S 책임자와 전화번호 또는 소비자상담 관련 전화번호\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"35-6\",\"NoticeItemGroupNo\":35,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:25:37',NULL,NULL,NULL,'N'),(36,'직접입력','[{\"NoticeItemCode\":\"36-1\",\"NoticeItemGroupNo\":36,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"직접입력\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:25:37',NULL,NULL,NULL,'N'),(37,'모바일쿠폰','[{\"NoticeItemCode\":\"37-1\",\"NoticeItemGroupNo\":37,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"발행자\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"37-2\",\"NoticeItemGroupNo\":37,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"유효기간, 이용조건\",\"GuideText\":\"유효기간 경과시 보상 기준 포함\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"37-3\",\"NoticeItemGroupNo\":37,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"이용 가능 매장\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"37-4\",\"NoticeItemGroupNo\":37,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"환불조건 및 방법\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"37-5\",\"NoticeItemGroupNo\":37,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:25:37',NULL,NULL,NULL,'N'),(38,'영화/공연','[{\"NoticeItemCode\":\"38-1\",\"NoticeItemGroupNo\":38,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주연\",\"GuideText\":\"공연에 한함\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"38-2\",\"NoticeItemGroupNo\":38,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"관람등급\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"38-3\",\"NoticeItemGroupNo\":38,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"상영/공연시간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"38-4\",\"NoticeItemGroupNo\":38,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"상영/공연장소\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"38-5\",\"NoticeItemGroupNo\":38,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"예매 취소 조건\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"38-6\",\"NoticeItemGroupNo\":38,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취소/환불방법\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"38-7\",\"NoticeItemGroupNo\":38,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:25:37',NULL,NULL,NULL,'N'),(39,'기타 서비스','[{\"NoticeItemCode\":\"39-1\",\"NoticeItemGroupNo\":39,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"서비스 제공 사업자\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"39-2\",\"NoticeItemGroupNo\":39,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"법에 의한 인증/허가 등을 받았음을 확인할 수 있는 경우 그에 대한 사항\",\"GuideText\":\"\",\"IsEssencial\":false,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"39-3\",\"NoticeItemGroupNo\":39,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"이용조건\",\"GuideText\":\"이용가능 기간/장소/추가비용 등\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"39-4\",\"NoticeItemGroupNo\":39,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취소/중도해약/해지 조건 및 환불기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"39-5\",\"NoticeItemGroupNo\":39,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취소/환불방법\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true},{\"NoticeItemCode\":\"39-6\",\"NoticeItemGroupNo\":39,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"소비자상담 관련 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true}]','2019-07-28 12:25:37',NULL,NULL,NULL,'N');
/*!40000 ALTER TABLE `notice_item_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operator_call`
--

DROP TABLE IF EXISTS `operator_call`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `operator_call` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message` varchar(500) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `processId` varchar(100) DEFAULT NULL,
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `checked` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operator_call`
--

LOCK TABLES `operator_call` WRITE;
/*!40000 ALTER TABLE `operator_call` DISABLE KEYS */;
INSERT INTO `operator_call` VALUES (1,'logaegae@gmail.com님이 사업자 인증을 요청하였습니다','/pages/seller-profile/2','changeSellerInfo','2019-06-30 22:34:05','N'),(2,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-01 02:07:26','N'),(3,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-01 02:07:57','N'),(4,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-01 02:08:04','N'),(5,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-01 02:08:16','N'),(6,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-01 02:08:28','N'),(7,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-07 00:25:07','N'),(8,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-07 00:26:28','N'),(9,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-07 00:29:53','N'),(10,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-07 12:01:40','N'),(11,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-07 12:06:45','N'),(12,'logaegae@gmail.com님이 사업자 인증을 요청하였습니다','/pages/seller-profile/2','changeSellerInfo','2019-07-07 14:10:58','N'),(13,'logaegae@gmail.com님이 사업자 인증을 요청하였습니다','/pages/seller-profile/2','changeSellerInfo','2019-07-07 14:22:18','N'),(14,'logaegae@gmail.com님이 사업자 인증을 요청하였습니다','/pages/seller-profile/2','changeSellerInfo','2019-07-07 14:34:27','N'),(15,'logaegae@gmail.com님이 사업자 인증을 요청하였습니다','/pages/seller-profile/2','changeSellerInfo','2019-07-07 14:41:41','N'),(16,'logaegae@gmail.com님이 사업자 인증을 요청하였습니다','/pages/seller-profile/2','changeSellerInfo','2019-07-07 14:42:47','N'),(17,'logaegae@gmail.com님이 사업자 인증을 재요청하였습니다','/pages/seller-profile/2','changeSellerInfo','2019-07-07 15:21:04','N'),(18,'logaegae@gmail.com님이 신규 계정 정보 인증을 요청하였습니다','/pages/seller-profile/2','changeSellerAccount','2019-07-10 02:45:24','N'),(19,'logaegae@gmail.com님이 신규 계정 정보 인증을 요청하였습니다','/pages/seller-profile/2','changeSellerAccount','2019-07-10 03:00:20','N'),(20,'logaegae@gmail.com님이 계정 정보 변경을 요청하였습니다','/pages/seller-profile/2','changeSellerAccount','2019-07-13 12:06:08','N'),(21,'logaegae@gmail.com님이 계정 정보 변경을 요청하였습니다','/pages/seller-profile/2','changeSellerAccount','2019-07-13 12:08:09','N'),(22,'logaegae@gmail.com님이 계정 정보 변경을 요청하였습니다','/pages/seller-profile/2','changeSellerAccount','2019-07-13 13:40:48','N'),(23,'logaegae@gmail.com님이 계정 정보 변경을 요청하였습니다','/pages/seller-profile/2','changeSellerAccount','2019-07-13 14:07:26','N'),(24,'logaegae@gmail.com님이 계정 정보 변경을 요청하였습니다','/pages/seller-profile/2','changeSellerAccount','2019-07-13 14:09:19','N'),(25,'logaegae@gmail.com님이 계정 정보 변경을 요청하였습니다','/pages/seller-profile/2','changeSellerAccount','2019-07-13 14:10:06','N'),(26,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-13 14:10:22','N'),(27,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-13 14:16:23','N'),(28,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-13 14:18:03','N'),(29,'logaegae@gmail.com님이 연락처 정보를 변경하였습니다','/pages/seller-profile/2','changeSellerPhone','2019-07-13 14:19:19','N');
/*!40000 ALTER TABLE `operator_call` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_log`
--

DROP TABLE IF EXISTS `order_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `order_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` int(10) unsigned NOT NULL COMMENT '주문 번호',
  `productId` int(10) unsigned NOT NULL COMMENT '주문 상품',
  `beforeStatus` varchar(45) NOT NULL COMMENT '이전상태코드',
  `nextStatus` varchar(45) NOT NULL COMMENT '변경상태코드',
  `comment` varchar(1000) NOT NULL COMMENT '상태 코멘트',
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '상태변경일',
  `uDate` datetime DEFAULT NULL,
  `delYn` char(1) DEFAULT 'N',
  `createdBy` varchar(100) DEFAULT NULL COMMENT '처리자',
  `ip` varchar(100) DEFAULT NULL COMMENT '처리자 ip',
  `updatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PATH_INDEX` (`orderId`,`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_log`
--

LOCK TABLES `order_log` WRITE;
/*!40000 ALTER TABLE `order_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller_account`
--

DROP TABLE IF EXISTS `seller_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `seller_account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `referenceId` int(10) unsigned DEFAULT NULL,
  `bankName` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ownerName` varchar(45) CHARACTER SET utf8 NOT NULL,
  `bankAccount` varchar(50) CHARACTER SET utf8 NOT NULL,
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `uDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `createdBy` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `updatedBy` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seller_user_fk_idx` (`referenceId`),
  CONSTRAINT `seller_user_fk` FOREIGN KEY (`referenceId`) REFERENCES `seller_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller_account`
--

LOCK TABLES `seller_account` WRITE;
/*!40000 ALTER TABLE `seller_account` DISABLE KEYS */;
INSERT INTO `seller_account` VALUES (2,14,'국민','김범영','123456','2019-07-15 02:04:53','2019-07-15 02:04:53','SYSTEM',NULL);
/*!40000 ALTER TABLE `seller_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller_auth`
--

DROP TABLE IF EXISTS `seller_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `seller_auth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `referenceId` int(10) unsigned NOT NULL,
  `isPhoneCertified` char(1) DEFAULT 'N',
  `isSellerCertified` char(1) DEFAULT 'N',
  `isAccountCertified` char(1) DEFAULT 'N',
  `phoneCertifiedDate` datetime DEFAULT NULL,
  `accountCertifiedDate` datetime DEFAULT NULL,
  `sellerCertifiedDate` datetime DEFAULT NULL,
  `phoneCertifiedBy` varchar(45) DEFAULT NULL,
  `accountCertifiedBy` varchar(45) DEFAULT NULL,
  `sellerCertifiedBy` varchar(45) DEFAULT NULL,
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `isSentDocument` char(1) DEFAULT 'N',
  `isSentAccount` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `reference_fk_idx` (`referenceId`),
  CONSTRAINT `reference_fk` FOREIGN KEY (`referenceId`) REFERENCES `seller_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller_auth`
--

LOCK TABLES `seller_auth` WRITE;
/*!40000 ALTER TABLE `seller_auth` DISABLE KEYS */;
INSERT INTO `seller_auth` VALUES (12,14,'Y','Y','Y','2019-07-16 07:38:47','2019-07-21 22:30:59','2019-07-24 03:39:34','superAdmin','superAdmin','superAdmin','2019-07-15 02:04:53','N','N');
/*!40000 ALTER TABLE `seller_auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller_product`
--

DROP TABLE IF EXISTS `seller_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `seller_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL COMMENT '등록 사용자',
  `title` varchar(500) NOT NULL COMMENT '제목',
  `gift` varchar(500) DEFAULT NULL COMMENT '재고 설정 여부',
  `price` bigint(12) NOT NULL COMMENT '상품가격',
  `info` varchar(4000) DEFAULT NULL COMMENT '공시정보',
  `comment` varchar(4000) DEFAULT NULL COMMENT '상품 코멘트',
  `memo` varchar(4000) DEFAULT NULL COMMENT '고객상담메모',
  `madein` varchar(100) DEFAULT NULL COMMENT '택배사',
  `stock` int(10) DEFAULT NULL COMMENT '재고',
  `maximumAllow` int(10) DEFAULT NULL COMMENT '배송비',
  `modelName` varchar(100) DEFAULT NULL,
  `medicalHealthProduct` tinyint(1) DEFAULT '0' COMMENT '상태변경 요청 여부',
  `needCertifiedProduct` tinyint(1) DEFAULT '0',
  `safetyCertiInfo` tinyint(1) DEFAULT '0',
  `optionList` text,
  `sellerDiscount` text,
  `noticeId` int(11) DEFAULT NULL,
  `noticeInfo` text,
  `status` varchar(45) NOT NULL COMMENT '상태 코드',
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `uDate` datetime DEFAULT NULL,
  `delYn` char(1) DEFAULT 'N',
  `createdBy` varchar(100) DEFAULT NULL,
  `updatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `USERID_INDEX` (`userId`),
  CONSTRAINT `userid_fk` FOREIGN KEY (`userId`) REFERENCES `seller_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller_product`
--

LOCK TABLES `seller_product` WRITE;
/*!40000 ALTER TABLE `seller_product` DISABLE KEYS */;
INSERT INTO `seller_product` VALUES (6,14,'ㅅㄷㄴㅅ',NULL,50500,'','test입니다',NULL,'korea',5555,555,'m-1234',0,1,0,'[]','{\"checked\":true,\"type\":\"1\",\"value\":70}',2,'[{\"NoticeItemCode\":\"2-1\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제품의 주소재\",\"GuideText\":\"운동화인 경우에는 겉감, 안감을 구분하여 입력\",\"IsEssencial\":true,\"IsEssencialSpecified\":true,\"value\":\"1\"},{\"NoticeItemCode\":\"2-2\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"색상\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true,\"value\":\"2\"},{\"NoticeItemCode\":\"2-3\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"치수\",\"GuideText\":\"발길이 : 해외사이즈 입력시 국내사이즈 병행입력(mm)\\r\\n굽높이 : 굽 재료를 사용하는 여성화에 한함 (cm)\",\"IsEssencial\":true,\"IsEssencialSpecified\":true,\"value\":\"3\"},{\"NoticeItemCode\":\"2-4\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조자/수입자\",\"GuideText\":\"수입품의 경우 수입자를 함께 입력, 병행수입의 경우 병행수입 여부로 대체 가능\",\"IsEssencial\":true,\"IsEssencialSpecified\":true,\"value\":\"4\"},{\"NoticeItemCode\":\"2-5\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"제조국\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true,\"value\":\"5\"},{\"NoticeItemCode\":\"2-6\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"취급시 주의사항\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true,\"value\":\"6\"},{\"NoticeItemCode\":\"2-7\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"품질보증기준\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true,\"value\":\"7\"},{\"NoticeItemCode\":\"2-8\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"A/S 책임자와 전화번호\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true,\"value\":\"8\"},{\"NoticeItemCode\":\"2-9\",\"NoticeItemGroupNo\":2,\"NoticeItemGroupNoSpecified\":true,\"NoticeItemCodeName\":\"주문후 예상 배송기간\",\"GuideText\":\"\",\"IsEssencial\":true,\"IsEssencialSpecified\":true,\"value\":\"9\"}]','WAIT','2019-08-03 23:12:29','2019-08-05 00:48:54','N',NULL,'logaegae@gmail.com');
/*!40000 ALTER TABLE `seller_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller_product_platform`
--

DROP TABLE IF EXISTS `seller_product_platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `seller_product_platform` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productId` int(10) unsigned NOT NULL COMMENT '주문 상품',
  `platformChannel` varchar(50) DEFAULT NULL COMMENT '등록된 플랫폼 채널',
  `platformId` varchar(30) DEFAULT NULL COMMENT '플랫폼 상품코드',
  `status` varchar(45) DEFAULT NULL COMMENT '플랫폼 내 상품상태',
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `uDate` datetime DEFAULT NULL,
  `delYn` char(1) DEFAULT 'N',
  `createdBy` varchar(100) DEFAULT NULL,
  `updatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `INDEX` (`productId`,`platformChannel`),
  CONSTRAINT `product_fk` FOREIGN KEY (`productId`) REFERENCES `seller_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller_product_platform`
--

LOCK TABLES `seller_product_platform` WRITE;
/*!40000 ALTER TABLE `seller_product_platform` DISABLE KEYS */;
/*!40000 ALTER TABLE `seller_product_platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller_user`
--

DROP TABLE IF EXISTS `seller_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `seller_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL COMMENT '이메일',
  `companyName` varchar(100) NOT NULL COMMENT '상호',
  `representation` varchar(50) DEFAULT NULL,
  `companyPhone` varchar(45) DEFAULT NULL,
  `sellerPhone` varchar(45) DEFAULT NULL,
  `sellerAlarmEmail` varchar(100) DEFAULT NULL,
  `sector` varchar(45) DEFAULT NULL,
  `business` varchar(45) DEFAULT NULL,
  `companyNumber` varchar(45) DEFAULT NULL,
  `corporateNumber` varchar(45) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL COMMENT '비밀번호',
  `isMailCertified` char(1) DEFAULT 'N' COMMENT '메일인증여부',
  `isDormant` char(1) DEFAULT 'N' COMMENT '휴면계정여부',
  `sentDormantMail` char(1) DEFAULT 'N' COMMENT '휴면계정메일발송여부',
  `needPwChange` char(1) DEFAULT 'N' COMMENT '비밀번호변경필요여부',
  `cDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일',
  `uDate` datetime DEFAULT NULL,
  `lastPwUpdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `from` varchar(50) DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `INDEX` (`email`,`password`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='판매 회원';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller_user`
--

LOCK TABLES `seller_user` WRITE;
/*!40000 ALTER TABLE `seller_user` DISABLE KEYS */;
INSERT INTO `seller_user` VALUES (14,'logaegae@gmail.com','테스트 상호','q','5','0109004',NULL,'e','r','1123','','18006e2ca1c2129392c66d87334bd2452c572058d406b4e85f43c1f72def10f5','N','N','N','N','2019-07-15 02:04:53','2019-07-24 03:39:34','2019-07-15 02:04:53','SYSTEM','superAdmin');
/*!40000 ALTER TABLE `seller_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signin_log`
--

DROP TABLE IF EXISTS `signin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `signin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `cDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(45) DEFAULT NULL,
  `userAgent` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signin_log`
--

LOCK TABLES `signin_log` WRITE;
/*!40000 ALTER TABLE `signin_log` DISABLE KEYS */;
INSERT INTO `signin_log` VALUES (1,'logaegae@gmail.com','2018-06-05 16:06:15','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(2,'logaegae@gmail.com','2017-06-05 16:08:16','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(3,'logaegae@gmail.com','2019-06-06 12:51:33','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(4,'logaegae@gmail.com','2019-06-06 12:58:04','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(5,'logaegae@gmail.com','2019-06-06 12:58:18','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(6,'logaegae@gmail.com','2019-06-06 13:01:14','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(7,'logaegae@gmail.com','2019-06-06 13:01:53','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(8,'logaegae@gmail.com','2019-06-06 13:01:59','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(9,'logaegae@gmail.com','2019-06-06 13:04:18','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(10,'logaegae@gmail.com','2019-06-06 13:05:03','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(11,'logaegae@gmail.com','2019-06-06 13:06:16','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(12,'logaegae@gmail.com','2019-06-06 13:06:39','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(13,'logaegae@gmail.com','2019-06-06 13:07:19','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(14,'logaegae@gmail.com','2019-06-06 13:41:13','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(15,'logaegae@gmail.com','2019-06-06 13:41:34','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(16,'logaegae@gmail.com','2019-06-06 13:41:49','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(17,'logaegae@gmail.com','2019-06-06 13:42:08','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(18,'logaegae@gmail.com','2019-06-06 13:42:26','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(19,'logaegae@gmail.com','2019-06-06 13:43:29','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(20,'logaegae@gmail.com','2019-06-06 14:02:18','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(21,'logaegae@gmail.com','2019-06-06 17:54:21','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(22,'logaegae@gmail.com','2019-06-06 17:55:36','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(23,'logaegae@gmail.com','2019-06-06 17:57:22','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(24,'logaegae@gmail.com','2019-06-06 17:57:50','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(25,'logaegae@gmail.com','2019-06-06 19:00:01','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(26,'logaegae@gmail.com','2019-06-06 19:09:46','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(27,'logaegae@gmail.com','2019-06-06 20:52:04','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(28,'logaegae@gmail.com','2019-06-06 20:52:15','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(29,'logaegae@gmail.com','2019-06-06 21:15:21','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(30,'logaegae@gmail.com','2019-06-06 21:24:06','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(31,'logaegae@gmail.com','2019-06-06 21:34:53','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(32,'logaegae@gmail.com','2019-06-06 21:35:51','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(33,'logaegae@gmail.com','2019-06-06 21:35:56','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(34,'logaegae@gmail.com','2019-06-06 21:43:29','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(35,'logaegae@gmail.com','2019-06-06 21:47:51','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(36,'logaegae@gmail.com','2019-06-06 21:48:22','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(37,'logaegae@gmail.com','2019-06-06 21:48:53','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(38,'logaegae@gmail.com','2019-06-07 10:54:24','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(39,'logaegae@gmail.com','2019-06-07 10:58:39','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(40,'logaegae@gmail.com','2019-06-07 12:17:43','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(41,'logaegae@gmail.com','2019-06-07 12:24:49','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(42,'logaegae@gmail.com','2019-06-07 12:25:07','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(43,'logaegae@gmail.com','2019-06-07 20:43:44','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(44,'logaegae@gmail.com','2019-06-08 22:21:36','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(45,'logaegae@gmail.com','2019-06-08 22:26:29','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(46,'logaegae@gmail.com','2019-06-08 22:28:18','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(47,'logaegae@gmail.com','2019-06-08 22:31:21','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(48,'logaegae@gmail.com','2019-06-08 23:18:43','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(49,'logaegae@gmail.com','2019-06-08 23:20:14','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(50,'logaegae@gmail.com','2019-06-08 23:30:45','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(51,'logaegae@gmail.com','2019-06-08 23:40:54','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(52,'logaegae@gmail.com','2019-06-10 01:02:31','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(53,'logaegae@gmail.com','2019-06-10 01:55:44','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(54,'logaegae@gmail.com','2019-06-10 01:57:51','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(55,'logaegae@gmail.com','2019-06-10 01:58:08','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(56,'logaegae@gmail.com','2019-06-10 02:03:35','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(57,'logaegae@gmail.com','2019-06-10 02:31:45','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'),(58,'logaegae@gmail.com','2019-06-26 02:45:54','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(59,'logaegae@gmail.com','2019-06-27 00:16:03','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(60,'logaegae@gmail.com','2019-06-27 00:16:23','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(61,'logaegae@gmail.com','2019-06-27 00:17:06','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(62,'logaegae@gmail.com','2019-06-27 00:18:13','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(63,'logaegae@gmail.com','2019-06-27 00:20:11','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(64,'logaegae@gmail.com','2019-06-27 00:22:53','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(65,'logaegae@gmail.com','2019-06-27 00:27:47','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(66,'logaegae@gmail.com','2019-06-27 00:31:08','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(67,'logaegae@gmail.com','2019-06-27 00:35:17','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(68,'logaegae@gmail.com','2019-06-27 00:36:18','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(69,'logaegae@gmail.com','2019-06-27 00:37:49','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(70,'logaegae@gmail.com','2019-06-27 00:38:19','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(71,'logaegae@gmail.com','2019-06-27 00:40:41','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(72,'logaegae@gmail.com','2019-06-27 00:41:36','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(73,'logaegae@gmail.com','2019-06-27 00:41:55','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(74,'logaegae@gmail.com','2019-06-29 12:36:06','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(75,'logaegae@gmail.com','2019-06-29 14:02:14','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(76,'logaegae@gmail.com','2019-06-29 14:08:47','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(77,'logaegae@gmail.com','2019-06-29 23:10:07','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(78,'logaegae@gmail.com','2019-07-01 01:56:38','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(79,'logaegae@gmail.com','2019-07-04 00:48:02','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(80,'logaegae@gmail.com','2019-07-06 22:53:18','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(81,'logaegae@gmail.com','2019-07-08 00:58:34','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(82,'logaegae@gmail.com','2019-07-10 01:12:25','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(83,'logaegae@gmail.com','2019-07-13 11:24:21','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(84,'logaegae@gmail.com','2019-07-13 23:29:54','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(85,'logaegae@gmail.com','2019-07-21 12:39:20','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(86,'logaegae@gmail.com','2019-07-21 12:52:33','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(87,'logaegae@gmail.com','2019-07-21 12:53:28','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(88,'logaegae@gmail.com','2019-07-21 12:55:40','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(89,'logaegae@gmail.com','2019-07-21 23:36:23','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(90,'logaegae@gmail.com','2019-07-24 03:40:23','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(91,'logaegae@gmail.com','2019-07-24 06:03:55','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(92,'logaegae@gmail.com','2019-07-27 09:23:52','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(93,'logaegae@gmail.com','2019-07-27 11:51:28','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'),(94,'logaegae@gmail.com','2019-07-28 12:41:43','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'),(95,'logaegae@gmail.com','2019-07-28 12:42:38','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'),(96,'logaegae@gmail.com','2019-07-29 23:57:31','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'),(97,'logaegae@gmail.com','2019-08-03 17:07:36','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'),(98,'logaegae@gmail.com','2019-08-04 12:46:51','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36');
/*!40000 ALTER TABLE `signin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_code`
--

DROP TABLE IF EXISTS `site_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `site_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `memo` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  KEY `code_INDEX` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_code`
--

LOCK TABLES `site_code` WRITE;
/*!40000 ALTER TABLE `site_code` DISABLE KEYS */;
INSERT INTO `site_code` VALUES (1,'admin','USER GROUP','사이트 관리자',NULL),(2,'seller','USER GROUP','판매자 회원',NULL),(3,'operator','USER GROUP','사이트 운영자',NULL),(4,'visitor','USER GROUP','방문 회원',NULL),(5,'WAIT','PRODUCT STATUS','대기중',NULL),(6,'SELLING','PRODUCT STATUS','판매중',NULL),(7,'STOP','PRODUCT STATUS','판매중지',NULL),(8,'REMOVED','PRODUCT STATUS','삭제',NULL);
/*!40000 ALTER TABLE `site_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'usm'
--

--
-- Dumping routines for database 'usm'
--
/*!50003 DROP PROCEDURE IF EXISTS `ISP_SELECT_CHECK_MAIL_CODE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ISP_SELECT_CHECK_MAIL_CODE`(
	IN 	in_email	varchar(100)	CHARSET UTF8,
    IN 	in_code		varchar(200)	CHARSET UTF8,
    IN 	in_date		datetime,
    OUT result		int,
    OUT message		varchar(50)		CHARSET UTF8
)
BEGIN
	-- 이메일 인증 번호 검색
    
    SELECT count(*), 	tb1.id,		tb1.isUsed, 	CASE WHEN tb1.expireDate > in_date THEN 'N' ELSE 'Y' END
		INTO @count,	@id,		@isUsed, 		@isExpired
		FROM mail_log AS tb1
        WHERE tb1.toAddress = in_email AND
			tb1.code = in_code AND
            tb1.cDate < in_date 
		ORDER BY tb1.id DESC
        LIMIT 1;
        
	IF @count = 0 THEN
		SET result = 0;
		SET message = '코드를 다시 확인해 주세요';
	ELSEIF @isUsed = 'Y' THEN
		SET result = 0;
        SET message = '이미 사용된 코드입니다';
	ELSEIF @isExpired = 'Y' THEN
		SET result = 0;
        SET message = '사용기한이 만료된 코드입니다';
	ELSE
		UPDATE mail_log
			SET isUsed = 'Y'
            WHERE id = @id;
            
		SET result = @id;
        SET message = '인증되었습니다';
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_INSERT_MAIL_LOG` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_INSERT_MAIL_LOG`(
	IN		in_email		varchar(100),
    IN		in_code			varchar(200),
    IN		in_cDate		datetime,
    IN		in_expireMinute	int
)
BEGIN
	-- 인증메일 기록
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    IF in_cDate IS NULL THEN
		SET in_cDate = NOW();
	END IF;
    
    INSERT INTO mail_log (toAddress, code, cDate, expireDate) VALUES (in_email, in_code, in_cDate, DATE_ADD(in_cDate, INTERVAL in_expireMinute MINUTE));
    
    IF LAST_INSERT_ID() != 0 THEN
		SET result = 1;
	END IF;
    
    SELECT result, message;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_INSERT_SELLER_USER` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_INSERT_SELLER_USER`(
	IN	in_company_name		varchar(100)	CHARSET UTF8,
    IN 	in_email			varchar(100),
    IN	in_password			varchar(100),
    IN	in_code				int(10),
    IN	in_code_id			int,
    IN	in_from				varchar(50)
)
BEGIN
	-- 회원 가입
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    SELECT count(*)
		INTO @count
        FROM seller_user
        WHERE email = in_email;
        
	IF @count = 0 THEN
		-- 코드 인증
        SELECT count(*), tb1.isUsed
			INTO @count, @isUsed
            FROM mail_log AS tb1
            WHERE tb1.code = in_code
				AND tb1.id = in_code_id;
                
		IF @count = 0 THEN
			SET result = 0;
            SET message = '유효하지 않은 코드입니다';
            
		ELSEIF @isUsed = 'N' THEN
			SET result = 0;
            SET message = '이메일 인증을 완료해주세요';
            
		ELSE
			INSERT INTO seller_user
				(`companyName`, `email`, `password`, `isMailCertified`, `from`)
				VALUES (in_company_name, in_email, in_password, 'Y', in_from);
            
            SET result = 1;
            SET message = '가입되었습니다';
            
        END IF;
    ELSE
		SET result = 0;
        SET message = '이미 가입된 이메일입니다';
    END IF;
    
    SELECT result, message;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_MERGE_ADMIN_USER` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_MERGE_ADMIN_USER`(
	IN 	in_type			varchar(45),
	IN 	in_userId		varchar(45),
	IN	in_email		varchar(100),
    IN	in_password		varchar(100),
    IN 	in_group		varchar(100)	CHARSET UTF8,
    IN 	in_memo			varchar(1000)	CHARSET UTF8,
    IN 	in_updater		varchar(100)
)
BEGIN
	-- 어드민 회원 정보 업데이트
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
    END;
    
    START TRANSACTION;

		IF in_type = "INSERT" THEN
		
			INSERT INTO admin_user
					(`userId`, `email`, `password`, `group`, `memo`, `cDate`, `createdBy`)
					VALUES (in_id, in_email, in_password, in_group, in_memo, NOW(), in_updater);
				SET result = 1;
				SET message = '생성되었습니다';
				
		ELSEIF in_type = "UPDATE" THEN
		
			SELECT COUNT(*)
				INTO @count
				FROM admin_user
				WHERE userId = in_userId;
				
			IF @count = 0 THEN
				SET result = 0;
				SET message = '잘못된 계정입니다';
			ELSE
				IF in_password != '' THEN
					UPDATE admin_user AS tb1
						SET tb1.email = in_email, 
							tb1.group = in_group, 
							tb1.memo = in_memo, 
							tb1.password = in_password, 
							tb1.uDate = NOW(), 
							tb1.updatedBy = in_updater
						WHERE tb1.userId = in_userId;
				ELSE
					UPDATE admin_user AS tb2
						SET tb2.email = in_email, 
							tb2.group = in_group, 
							tb2.memo = in_memo, 
							tb2.uDate = NOW(), 
							tb2.updatedBy = in_updater
						WHERE tb2.userId = in_userId;
				END IF;
				
				SET result = 1;
				SET message = '변경되었습니다';
			END IF;
		END IF;

    COMMIT;
    
    SELECT result, message;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_MERGE_CUSTOMER_ORDER` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_MERGE_CUSTOMER_ORDER`(
	IN  in_type     			varchar(45),
	IN  in_id       			int(10)			UNSIGNED,
	IN  in_status   			int(10)			UNSIGNED,
	IN 	in_email				varchar(100)	CHARSET UTF8,
	IN	in_name1				varchar(45)		CHARSET UTF8,
	IN	in_name2				varchar(45)		CHARSET UTF8,
	IN	in_platformChannel		varchar(45)		CHARSET UTF8,
	IN	in_platformOrderId		varchar(45) 	CHARSET UTF8,
	IN	in_platformUserId		varchar(45)		CHARSET UTF8,
	IN	in_zipcode				int(10)			UNSIGNED,
	IN	in_address1				varchar(100)	CHARSET UTF8,
	IN	in_address2				varchar(100)	CHARSET UTF8,
	IN	in_phone1				varchar(45),
	IN	in_phone2				varchar(45),
	IN	in_deriveryMemo			varchar(200)	CHARSET UTF8,
	IN	in_payType				varchar(45)		CHARSET UTF8,
	IN	in_payStatus			varchar(45)		CHARSET UTF8,
	IN	in_price				bigint(12)		UNSIGNED,
	IN	in_totalDiscount		bigint(12)		UNSIGNED,
	IN	in_totalDiscountDetail	varchar(1000),
	IN	in_totalCost			bigint(12)		UNSIGNED,
	IN	in_totalCostDetail		varchar(1000),
	IN	in_deliveryCompany		varchar(50),
	IN	in_deliveryCode			bigint(20),
	IN	in_requirement			varchar(4000)	CHARSET UTF8,
	IN	in_memo					varchar(4000)	CHARSET UTF8,
	IN	in_cashReceiptYn		char(1),
	IN	in_taxReceiptYn			char(1),
	IN	in_updater				varchar(45),
	IN  in_productIds   		varchar(100)
)
BEGIN
	
	-- 고객의 주문 정보를 저장한다
	-- 최초 관리자가 작성하며 판매자가 상태관리
	-- 구매한 상품 정보는 나중에 따로 입력
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    DECLARE 	resultId	int(10)			UNSIGNED;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
        SELECT result, message;
    END;
    
    START TRANSACTION;

    IF in_type = 'INSERT' THEN
		INSERT INTO customer_order (
			status,
			email,
			name1,
			name2,
			platformChannel,
			platformOrderId,
			platformUserId,
			zipcode,
			address1,
			address2,
			phone1,
			phone2,
			deriveryMemo,
			payType,
			payStatus,
			price,
			ea,
			totalDiscount,
			totalDiscountDetail,
			totalCost,
			totalCostDetail,
			deliveryCompany,
			deliveryCode,
			requirement,
			memo,
			cashReceiptYn,
			taxReceiptYn,
			createdBy
		)
		VALUES (
			in_status,
			in_email,
			in_name1,
			in_name2,
			in_platformChannel,
			in_platformOrderId,
			in_platformUserId,
			in_zipcode,
			in_address1,
			in_address2,
			in_phone1,
			in_phone2,
			in_deriveryMemo,
			in_payType,
			in_payStatus,
			in_price,
			in_ea,
			in_totalDiscount,
			in_totalDiscountDetail,
			in_totalCost,
			in_totalCostDetail,
			in_deliveryCompany,
			in_deliveryCode,
			in_requirement,
			in_memo,
			in_cashReceiptYn,
			in_taxReceiptYn,
			in_updater
		);
		SET result = 1;
		SET message = '등록되었습니다';
		SET resultId = LAST_INSERT_ID();
      
    ELSEIF in_type = 'UPDATE' THEN

		SELECT COUNT(*)
			INTO @count
			FROM customer_order
			WHERE id = in_id
			LIMIT 1;
		IF @count = 0 THEN

			SET result = 0;
			SET message = 'No data Error';

		ELSE
			UPDATE customer_order
				SET 
					status = in_status,
					email = in_email,
					name1 = in_name1,
					name2 = in_name2,
					platformChannel = in_platformChannel,
					platformOrderId = in_platformOrderId,
					platformUserId = in_platformUserId,
					zipcode = in_zipcode,
					address1 = in_address1,
					address2 = in_address2,
					phone1 = in_phone1,
					phone2 = in_phone2,
					deriveryMemo = in_deriveryMemo,
					payType = in_payType,
					payStatus = in_payStatus,
					price = in_price,
					ea = in_ea,
					totalDiscount = in_totalDiscount,
					totalDiscountDetail = in_totalDiscountDetail,
					totalCost = in_totalCost,
					totalCostDetail = in_totalCostDetail,
					deliveryCompany = in_deliveryCompany,
					deliveryCode = in_deliveryCode,
					requirement = in_requirement,
					memo = in_memo,
					cashReceiptYn = in_cashReceiptYn,
					taxReceiptYn = in_taxReceiptYn,
					updatedBy = in_updater,
					uDate = NOW()
				WHERE id = in_id;

			SET result = 1;
			SET message = '등록되었습니다';
			SET resultId = in_productId;
            
            -- 관리자에게 알림
			
                
		END IF;

    END IF;
    
    COMMIT;
    
    SELECT result, message, resultId AS id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_MERGE_CUSTOMER_ORDER_PRODUCT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_MERGE_CUSTOMER_ORDER_PRODUCT`(
	IN  in_id    				int(10),
	IN  in_customerOrderId    	int(10),
	IN 	in_productId			int(10),
	IN	in_ea					int(10),
	IN	in_updater				varchar(45)
)
BEGIN
	-- 주문에 상품 정보를 기록
	-- 관리자가 생성 혹은 수정
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
        SELECT result, message;
    END;
    
    START TRANSACTION;
    
		SELECT COUNT(*)
			INTO @count1
			FROM customer_order
			WHERE id = in_customerOrderId
			LIMIT 1;

		SELECT COUNT(*)
			INTO @count2
			FROM seller_product
			WHERE id = in_productId
			LIMIT 1;
                    
		IF @count1 = 1 AND @count2 = 1 THEN

			IF in_id = '' THEN
		  
				INSERT INTO customer_order_product (
					productId,
					platformChannel,
					platformId,
					ea,
					status,
					createdBy
				)
				VALUES (
					in_productId,
					in_platformChannel,
					in_platformId,
					in_ea,
					in_status,
					in_updater
				);

			ELSE

				UPDATE customer_order_product
					SET
						productId = in_productId,
						platformChannel = in_platformChannel,
						platformId = in_platformId,
                        ea = in_ea,
						status = in_status,
                        uDate = NOW(),
						updatedBy = in_updater
					WHERE id = in_id;
			END IF;
			
			SET result = 1;
			SET message = '등록되었습니다';
				
		ELSE

			SET result = 0;
			SET message = 'No data Error';

		END IF;
		COMMIT;
		SELECT result, message;
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_MERGE_SELLER_ACCOUNT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_MERGE_SELLER_ACCOUNT`(
	IN	in_email				varchar(100)	CHARSET UTF8,
    IN	in_bankName				varchar(100)	CHARSET UTF8,
    IN	in_ownerName			varchar(45)		CHARSET UTF8,
    IN	in_bankAccount			varchar(50)		CHARSET UTF8
)
BEGIN
	-- 셀러 회원 계좌번호 인증 신청 업데이트
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
    END;
    
    START TRANSACTION;
		
		SELECT COUNT(*), tb1.id, tb2.id
			INTO @count, @seller_id, @account_id
			FROM (
				seller_user as tb1
				LEFT JOIN seller_account as tb2
				ON tb1.id = tb2.referenceId
            )
			WHERE tb1.email = in_email;
		
		IF @count = 1 AND @seller_id IS NOT NULL THEN
        
			-- 계정이 없을 경우
			IF @account_id IS NULL THEN
				INSERT INTO seller_account (
						referenceId,
						bankName,
						ownerName,
						bankAccount,
						createdBy,
						isAccountCertified,
						sentAccountInfo)
					VALUES (
						@seller_id,
						in_bankName,
						in_ownerName,
						in_bankAccount,
                        in_email,
						'N',
						'Y'
					);
					
				-- 관리자에게 알림
				INSERT INTO operator_call (
						message,
						url,
						processId
					) VALUES (
						CONCAT(in_email,'님이 신규 계정 정보 인증을 요청하였습니다'),
                        CONCAT('/pages/seller-profile/',@seller_id),
                        'changeSellerAccount'
					);
            ELSE
				UPDATE seller_account AS tb3
					SET 
						tb3.bankName = in_bankName,
                        tb3.ownerName = in_ownerName,
                        tb3.bankAccount = in_bankAccount,
                        tb3.isAccountCertified = 'N',
                        tb3.sentAccountInfo = 'Y',
                        tb3.updatedBy = in_email,
                        tb3.uDate = NOW()
					WHERE tb3.id = @account_id;
					
				-- 관리자에게 알림
				INSERT INTO operator_call (
						message,
						url,
						processId
					) VALUES (
						CONCAT(in_email,'님이 계정 정보 변경을 요청하였습니다'),
                        CONCAT('/pages/seller-profile/',@seller_id),
                        'changeSellerAccount'
					);				
			END IF;
            
			SET result = 1;
			SET message = '신청되었습니다';
		ELSE
			SET result = 0;
			SET message = '잘못된 이메일입니다';
		END IF;
    COMMIT;
    
    SELECT result, message;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_MERGE_SELLER_PRODUCT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_MERGE_SELLER_PRODUCT`(
	IN 	in_type				varchar(45),
	IN	in_productId		int(10)			UNSIGNED,
	IN	in_email			varchar(100)	CHARSET UTF8,
    IN	in_title			varchar(500)	CHARSET UTF8,
    IN	in_gift				varchar(500)	CHARSET UTF8,
	IN  in_price			bigint(12)		UNSIGNED,
	IN  in_info				varchar(4000)	CHARSET UTF8,
    IN	in_comment			varchar(4000)	CHARSET UTF8,
    IN	in_madein			varchar(100)	CHARSET UTF8,
    IN	in_memo				varchar(4000)	CHARSET UTF8,
    IN  in_maximumAllow		int(10)			UNSIGNED,
    IN  in_stock			int(10)			UNSIGNED,
    IN	in_modelName		varchar(100)	CHARSET UTF8,
    IN	in_isMHP			tinyint(1)		UNSIGNED,
    IN	in_isCP				tinyint(1)		UNSIGNED,
    IN	in_isSP				tinyint(1)		UNSIGNED,
	IN	in_optionList		TEXT			CHARSET UTF8,
    IN  in_sellerDiscount	TEXT			CHARSET UTF8,
    IN  in_noticeInfo		TEXT			CHARSET UTF8,
    IN  in_noticeId			int(10)			UNSIGNED,
    IN	in_creator			varchar(100)	CHARSET UTF8,
    IN	in_status			varchar(100)	CHARSET UTF8
)
BEGIN
	-- 판매자 상품 정보 업데이트
	-- 셀러가 작성하고 관리자가 상태를 변경
	-- 관리자가 상품 관리를 위해 직접 등록 가능
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    DECLARE 	resultId	int(10)			UNSIGNED;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
        SELECT result, message;
    END;
    
    START TRANSACTION;
        -- 유저 아이디 조회
		SELECT COUNT(*), id
			INTO @count, @userid
			FROM seller_user
			WHERE email = in_email
			LIMIT 1;
                    
		IF @count = 0 THEN

			SET result = 0;
			SET message = 'ID를 조회할 수 없습니다';
		
		ELSE
			-- 생성할 경우
			IF in_type = 'INSERT' THEN

				INSERT INTO seller_product (
					userId,
					title,
					gift,
					price,
					info,
					comment,
					madein,
					memo,
					maximumAllow,
					stock,
					modelName,
                    medicalHealthProduct,
					needCertifiedProduct,
					safetyCertiInfo,
					optionList,
					sellerDiscount,
                    noticeId,
                    noticeInfo,
                    status,
                    createdBy
				) VALUES (
					@userid,
					in_title,
					in_gift,
					in_price,
					in_info,
					in_comment,
					in_madein,
					in_memo,
					in_maximumAllow,
					in_stock,
					in_modelName,
                    in_isMHP,
					in_isCP,
					in_isSP,
					in_optionList,
					in_sellerDiscount,
                    in_noticeId,
                    in_noticeInfo,
                    in_status,
                    in_creator
                );
                
				SET result = 1;
				SET message = '등록되었습니다';
				SET resultId = LAST_INSERT_ID();

			-- 업데이트 할 경우
			ELSEIF in_type = 'S_UPDATE' THEN
				
				UPDATE seller_product AS tb2
					SET 
						tb2.title = in_title,
						tb2.gift = in_gift,
						tb2.price = in_price,
						tb2.info = in_info,
						tb2.comment = in_comment,
						tb2.madein = in_madein,
						tb2.memo = in_memo,
						tb2.maximumAllow = in_maximumAllow,
						tb2.stock = in_stock,
						tb2.modelName = in_modelName,
						tb2.medicalHealthProduct = in_isMHP,
						tb2.needCertifiedProduct = in_isCP,
						tb2.safetyCertiInfo = in_isSP,
						tb2.optionList = in_optionList,
                        
						tb2.sellerDiscount = in_sellerDiscount,
						tb2.noticeInfo = in_noticeInfo,
                        tb2.noticeId = in_noticeId,
                        
						tb2.uDate = NOW(),
						tb2.updatedBy = in_creator
					WHERE tb2.id = in_productId AND tb2.userId = @userid;

				SET result = 1;
				SET message = '등록되었습니다';
				SET resultId = in_productId;

			END IF;
            
			-- 관리자에게 알림
            /*
			INSERT INTO operator_call (
					message,
					url,
					processId
				) VALUES (
					CONCAT(in_email,'님의 상품 정보가 변경되었습니다'),
					CONCAT('/pages/seller-profile/',@seller_id),
					'changeSellerAccount'
				);
			*/
		END IF;
    COMMIT;
    
    SELECT result, message, resultId AS id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_MERGE_SELLER_PRODUCT_PLATFORM` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_MERGE_SELLER_PRODUCT_PLATFORM`(
	IN  in_productId    	int(10),
	IN 	in_platformChannel	varchar(50) 	CHARSET UTF8,
	IN	in_platformId		varchar(30),
	IN	in_status			varchar(45) 	CHARSET UTF8,
	IN	in_updater			varchar(45)
)
BEGIN
	-- 판매자 상품이 등록된 플랫폼 정보 저장
	-- 관리자가 작성하여 관리
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    DECLARE 	resultId	int(10);

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
        SELECT result, message;
    END;
    
    START TRANSACTION;
        -- 플랫폼 채널을 검색하여 없으면 INSERT 있으면 UPDATE
		SELECT COUNT(*), id
			INTO @count , @platformId
			FROM seller_product_platform
			WHERE productId = in_productId AND platformChannel = in_platformChannel
			LIMIT 1;
                    
		IF @count = 0 THEN
		  -- INSERT 
		  INSERT INTO seller_product_platform (
			productId,
			platformChannel,
			platformId,
			status,
			createdBy
		  )
		  VALUES (
			in_productId,
			in_platformChannel,
			in_platformId,
			in_status,
			in_updater
		  );

			SET result = 1;
			SET message = '등록되었습니다';
			SET resultId = LAST_INSERT_ID();
		
		ELSE
			-- UPDATE
			UPDATE seller_product_platform
				SET 
					platformId = in_platformId,
					status = in_status,
					updatedBy = in_updater,
					uDate = NOW(),
					updatedBy = in_updater
				WHERE productId = in_productId AND platformChannel = in_platformChannel;

			SET result = 1;
			SET message = '등록되었습니다';
			SET resultId = in_productId;

		END IF;
        
    COMMIT;
    
    SELECT result, message, resultId AS id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_MERGE_SELLER_USER` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_MERGE_SELLER_USER`(
	IN 	in_type					varchar(45),
	IN 	in_companyName			varchar(45)		CHARSET UTF8,
	IN	in_email				varchar(100),
    IN	in_password				varchar(100),
    IN 	in_companyPhone			varchar(100),
    IN 	in_sellerPhone			varchar(100),
    IN  in_isPhoneCertified		CHAR(1),
    IN  in_isSellerCertified	CHAR(1),
    IN  in_isAccountCertified 	CHAR(1),
    IN  in_isSentAccount		CHAR(1),
    IN  in_isSentDocument	 	CHAR(1),
    IN 	in_updater				varchar(100),
    IN 	in_ownerName			varchar(45)		CHARSET UTF8,
    IN 	in_bankAccount			varchar(50)		CHARSET UTF8,
    IN 	in_bankName				varchar(50)		CHARSET UTF8,
    IN 	in_companyNumber		varchar(45)		CHARSET UTF8,
    IN 	in_corporateNumber		varchar(45)		CHARSET UTF8,
    IN 	in_representation		varchar(50)		CHARSET UTF8,
    IN 	in_sector				varchar(45)		CHARSET UTF8,
    IN 	in_business				varchar(45)		CHARSET UTF8
)
BEGIN
	-- 판매자 회원 정보 업데이트
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    DECLARE 	userid		int(10);
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
        SELECT result, message;
    END;
    
    START TRANSACTION;
        
		SELECT COUNT(*), id
			INTO @count, @id
			FROM seller_user
			WHERE email = in_email;
            
		SET userid = @id;
        
		IF in_type = "INSERT" AND @count = 0 THEN
			INSERT INTO seller_user
				(`companyName`, `email`, `password`, `companyPhone`, `sellerPhone`, `cDate`, `from`, `isMailCertified`, `representation`, `sector`, `business`, `companyNumber`, `corporateNumber`)
				VALUES (in_companyName, in_email, in_password, in_companyPhone, in_sellerPhone, NOW(), in_updater, 'N', in_representation, in_sector, in_business, in_companyNumber, in_corporateNumber);
            
            SET userid = LAST_INSERT_ID();
		END IF;
        
		-- 개인 정보 업데이트
		IF in_password != '' THEN
			UPDATE seller_user AS tb1
				SET tb1.sellerPhone = in_sellerPhone, 
					tb1.companyPhone = in_companyPhone, 
					tb1.companyName = in_companyName, 
					tb1.password = in_password,
					tb1.uDate = NOW(), 
					tb1.representation = in_representation,
					tb1.sector = in_sector,
					tb1.business = in_business,
					tb1.companyNumber = in_companyNumber,
					tb1.corporateNumber = in_corporateNumber,
					tb1.updatedBy = in_updater
				WHERE tb1.email = in_email AND tb1.id = userid;
		ELSE
			UPDATE seller_user AS tb2
				SET tb2.sellerPhone = in_sellerPhone, 
					tb2.companyPhone = in_companyPhone, 
					tb2.companyName = in_companyName,
					tb2.uDate = NOW(), 
					tb2.representation = in_representation,
					tb2.sector = in_sector,
					tb2.business = in_business,
					tb2.companyNumber = in_companyNumber,
					tb2.corporateNumber = in_corporateNumber,
					tb2.updatedBy = in_updater
				WHERE tb2.email = in_email AND tb2.id = userid;
		END IF;
        
		SELECT COUNT(*)
			INTO @count
			FROM seller_auth 
			WHERE referenceId = userid;
				
		-- 인증 정보 없을 경우 생성
		IF @count = 0 THEN
			INSERT INTO seller_auth (
				referenceId,
				isPhoneCertified, 
				isSellerCertified, 
				isAccountCertified
			)
			VALUES (
				userid,
				'N',
				'N',
				'N'
			);
		END IF;
        
        SELECT
				isPhoneCertified, 
				isSellerCertified, 
				isAccountCertified
			INTO
				@isPhoneCertified, 
				@isSellerCertified, 
				@isAccountCertified
			FROM seller_auth 
			WHERE referenceId = userid;
		
		-- 인증 정보 업데이트 & 신청 정보는 모두 확인했다 간주하므로 신청 플레그 제거
		UPDATE seller_auth
			SET isPhoneCertified = in_isPhoneCertified, 
				isSellerCertified = in_isSellerCertified, 
				isAccountCertified = in_isAccountCertified,
                isSentAccount = in_isSentAccount,
                isSentDocument = in_isSentDocument
			WHERE referenceId = userid;
			
		IF in_isPhoneCertified != @isPhoneCertified AND in_isPhoneCertified = 'Y' THEN
			UPDATE seller_auth
				SET
					phoneCertifiedDate = NOW(), 
					phoneCertifiedBy = in_updater
				WHERE referenceId = userid;
		END IF;
		IF in_isSellerCertified != @isSellerCertified AND in_isSellerCertified = 'Y' THEN
			UPDATE seller_auth
				SET
					isSentDocument = 'N',
					sellerCertifiedDate = NOW(), 
					sellerCertifiedBy = in_updater
				WHERE referenceId = userid;
		END IF;
		IF in_isAccountCertified != @isAccountCertified AND in_isAccountCertified = 'Y' THEN
			UPDATE seller_auth
				SET
					isSentAccount = 'N',
					accountCertifiedDate = NOW(), 
					accountCertifiedBy = in_updater
				WHERE referenceId = userid;
		END IF;
			
		-- 계좌 정보
		 SELECT COUNT(*)
			INTO @count
			FROM seller_account
			WHERE referenceId = userid;
		
		-- 계좌 없을 경우 생성
		IF @count = 0 THEN
			INSERT INTO seller_account (
				referenceId,
				bankName,
				bankAccount,
				ownerName,
				cDate,
				createdBy
			)
			VALUES (
				userid,
				in_bankName,
				in_ownerName,
				in_bankAccount,
				NOW(),
				in_updater
			);
		END IF;
			
		-- 계좌 정보 업데이트
		UPDATE seller_account
			SET bankName = in_bankName, 
				ownerName = in_ownerName, 
				bankAccount = in_bankAccount
			WHERE referenceId = userid;
				
		SET result = 1;
		SET message = '변경되었습니다';
        
    COMMIT;
    
    SELECT result, message, userid AS id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_MOVE_DORMANT_TO_SELLER` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_MOVE_DORMANT_TO_SELLER`(
	IN		in_email		varchar(100)		CHARSET UTF8,
    IN		in_password		varchar(100),
    IN		in_ip			varchar(45),
    IN		in_ua			varchar(200)
)
BEGIN

	-- 휴면계정 복원
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
    END;
    
	START TRANSACTION;
		
        INSERT INTO seller_user ( `id`, `email`, `password`, `companyName`, `isMailCertified`, `isDormant`, `sentDormantMail`, `needPwChange`, `cDate`, `from`)
			SELECT tb1.id, tb1.email, tb1.password, tb1.companyName, tb1.isMailCertified, 'N', tb1.sentDormantMail, tb1.needPwChange, tb1.cDate, tb1.from
					FROM dormant_seller_user AS tb1
					WHERE tb1.email = in_email AND tb1.password = in_password;
		
        SET SQL_SAFE_UPDATES = 0;
        
        DELETE FROM dormant_seller_user
			WHERE email = in_email AND password = in_password;
            
		SET SQL_SAFE_UPDATES = 1;

		SET result = 1;
        SET message = 'SUCCESS';
        
    COMMIT;
    
	SELECT result, message;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_MOVE_SELLER_TO_DORMANT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_MOVE_SELLER_TO_DORMANT`(
)
BEGIN
	-- 휴면계정 처리
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
    END;
    
	START TRANSACTION;
    
		INSERT INTO dormant_seller_user ( `id`, `email`, `password`, `companyName`, `isMailCertified`, `isDormant`, `sentDormantMail`, `needPwChange`, `cDate`, `from`)
			SELECT tb1.id, tb1.email, tb1.password, tb1.companyName, tb1.isMailCertified, 'Y', tb1.sentDormantMail, tb1.needPwChange, tb1.cDate, tb1.from
					FROM seller_user AS tb1
					LEFT JOIN (
						SELECT email, MAX(cDate) AS lastLiginDate
							FROM signin_log
							GROUP BY email
					) AS tb2
					ON tb1.email = tb2.email
					WHERE  tb2.lastLiginDate <= date_add(now(), interval -12 month);
		SET SQL_SAFE_UPDATES = 0;
		DELETE FROM seller_user
			WHERE id IN ( SELECT * FROM
				(SELECT tb3.id
					FROM seller_user AS tb3
					LEFT JOIN (
						SELECT email, MAX(cDate) AS lastLiginDate
							FROM signin_log
							GROUP BY email
					) AS tb4
					ON tb3.email = tb4.email
					WHERE  tb4.lastLiginDate <= date_add(now(), interval -12 month)
				) AS tb5
			);
		SET SQL_SAFE_UPDATES = 1;
        
        SET result = 1;
        SET message = 'SUCCESS';
        
    COMMIT;
    
	SELECT result, message;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_SEARCH_ADMIN_SIGNIN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_SEARCH_ADMIN_SIGNIN`(
	IN	in_id		varchar(45)		CHARSET UTF8,
    IN 	in_password	varchar(100),
    IN	in_ip		varchar(45),
    IN	in_ua		varchar(200)
)
BEGIN
	-- 회원 로그인
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
     SELECT count(*), tb1.id, tb1.group
		INTO @count, @id, @group
        FROM admin_user AS tb1
        WHERE userId = in_id AND password = in_password;
	
    IF @count = 0 THEN
		SET result = 0;
		SET message = '아이디와 비밀번호를 확인해주세요';
    ELSE
		-- 로그 수집
		INSERT INTO admin_signin_log (`userId`, `ip`, `userAgent`)
		VALUES	(in_id, in_ip, in_ua);
		
		SET result = 1;
		SET message = '로그인되었습니다';
    END IF;
    
    SELECT result, message, @group AS 'group';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_SEARCH_SELLER_SIGNIN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_SEARCH_SELLER_SIGNIN`(
	IN	in_email	varchar(100)	CHARSET UTF8,
    IN 	in_password	varchar(100),
    IN	in_ip		varchar(45),
    IN	in_ua		varchar(200)
)
BEGIN
	-- 회원 로그인
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    DECLARE 	userGroup	char(20)						default 'visitor';
    
     SELECT count(*), tb1.id, tb1.isDormant, tb1.needPwChange, tb2.isSellerCertified, tb1.companyName, tb2.isAccountCertified
		INTO @count, @id, @isDormant, @needPwChange, @isSellerCertified, @companyName, @isAccountCertified
        FROM seller_user AS tb1
        LEFT JOIN seller_auth AS tb2
        ON tb1.id = tb2.referenceId
        WHERE email = in_email AND password = in_password;
	
    IF @count = 0 THEN
    
		-- 휴면계정 여부
        SELECT count(*), companyName
			INTO @count
			FROM dormant_seller_user
			WHERE email = in_email AND password = in_password;
            
		IF @count = 1 THEN
			SET result = 0;
			SET message = '휴면계정입니다';
		ELSE
			SET result = 0;
			SET message = '아이디와 비밀번호를 확인해주세요';
		END IF;
    ELSE
		-- 패스워드 변경 여부
        IF @needPwChange = 'Y' THEN
			SET result = 0;
            SET message = '비밀번호를 변경해주세요';
        
        ELSE 
			-- 로그 수집
			INSERT INTO signin_log (`email`, `ip`, `userAgent`)
            VALUES	(in_email, in_ip, in_ua);
            
			SET result = 1;
			SET message = '로그인되었습니다';
            
            -- 그룹 확인
            IF @isSellerCertified = 'Y' THEN
				SET userGroup = 'seller';
            ELSE
				SET userGroup = 'visitor';
			END IF;
            
		END IF;
    END IF;
    
    SELECT 
		result,
        message,
        userGroup,
        @companyName AS companyName,
        @isSellerCertified AS isSellerCertified,
        @isAccountCertified AS isAccountCertified;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_SELECT_ADMIN_USER_ABOUT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_SELECT_ADMIN_USER_ABOUT`(
	IN	in_id	varchar(45)		CHARSET UTF8
)
BEGIN
	-- 관리자 회원 계정 정보
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    SELECT count(*), tb1.cDate, tb2.name, tb1.email, tb1.userId
		INTO @count, @cDate, @groupName, @email, @userId
        FROM admin_user AS tb1
        LEFT JOIN site_code AS tb2
        ON tb1.group = tb2.code
		WHERE tb1.userId = in_id;
        
   IF @count = 0 THEN
		SET result = 0;
        SET message = 'ID를 조회할 수 없습니다';
   ELSE
		SET result = 1;
   END IF;
   
   SELECT result, message, @cDate AS cDate, @groupName AS groupName, @email AS email, @userId AS userId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_SELECT_CHECK_MAIL_CODE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_SELECT_CHECK_MAIL_CODE`(
	IN 	in_email	varchar(100)	CHARSET UTF8,
    IN 	in_code		varchar(200)	CHARSET UTF8
)
BEGIN
	-- 이메일 인증 번호 검색
    
    CALL ISP_SELECT_CHECK_MAIL_CODE(in_email, in_code, NOW(), @result, @message);
    
    SELECT @result AS result, @message AS message;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_SELECT_SELLER_USER_ABOUT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_SELECT_SELLER_USER_ABOUT`(
	IN	in_email				varchar(45)		CHARSET UTF8,
    IN	in_isSellerCertified	char(1)
)
BEGIN
	-- 판매자 회원 계정 정보 조회
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    DECLARE 	userGroup	varchar(50)						default '';
    
    SELECT count(*)
		INTO @count
        FROM seller_user
		WHERE email = in_email;
        
   IF @count = 1 THEN
   
		IF in_isSellerCertified = 'Y' THEN
			SET userGroup = 'seller';
		ELSEIF in_isSellerCertified ='N' THEN
			SET userGroup = 'visitor';
		END IF;
        
		SELECT count(*), 
			tb1.cDate, 
            tb2.name, 
            tb1.email, 
            tb1.isMailCertified, 
            tb4.isPhoneCertified, 
            tb4.isSellerCertified, 
            tb1.sellerPhone, 
            tb1.companyPhone, 
            tb1.companyName, 
            tb1.needPwChange, 
            tb1.sector, 
            tb1.business, 
            tb1.companyNumber, 
            tb1.corporateNumber, 
            tb1.representation, 
            tb3.bankName,
            tb3.ownerName,
            tb3.bankAccount,
            CASE 
				WHEN tb4.isSentDocument IS NULL
				THEN 'N'
                ELSE tb4.isSentDocument
			END,
            CASE 
				WHEN tb4.isSentAccount IS NULL
				THEN 'N'
                ELSE tb4.isSentAccount
			END,
            CASE 
				WHEN tb4.isAccountCertified IS NULL
				THEN 'N'
                ELSE tb4.isAccountCertified
			END,
            CASE 
				WHEN tb4.isSellerCertified IS NULL
				THEN 'N'
                ELSE tb4.isSellerCertified
			END
		INTO @count, 
			@cDate, 
            @groupName, 
            @email, 
            @isMailCertified,  
            @isPhoneCertified, 
            @isSellerCertified,
            @sellerPhone, 
            @companyPhone, 
            @companyName, 
            @needPwChange, 
            @sector, 
            @business, 
            @companyNumber, 
            @corporateNumber, 
            @representation, 
            @bankName,
            @ownerName,
            @bankAccount,
            @isSentDocument,
            @isSentAccount,
            @isAccountCertified,
			@isSellerCertified
        FROM seller_user AS tb1
        LEFT JOIN site_code AS tb2
        ON userGroup = tb2.code
        LEFT JOIN seller_account AS tb3
        ON tb1.id = tb3.referenceId
        LEFT JOIN seller_auth AS tb4
        ON tb1.id = tb4.referenceId
		WHERE tb1.email = in_email;
        
		SET result = 1;
   ELSE
		SET result = 0;
        SET message = 'ID를 조회할 수 없습니다';
   END IF;
   
   SELECT 
		result, 
        message, 
        @cDate AS cDate, 
        @groupName AS groupName, 
        @email AS email, 
        @isMailCertified AS isMailCertified, 
        @isPhoneCertified AS isPhoneCertified,
        @isSellerCertified AS isSellerCertified,
        @sentDocument AS sentDocument,
        @sellerPhone AS sellerPhone,
        @companyPhone AS companyPhone,
        @companyName AS companyName,
        @needPwChange AS needPwChange,
        @sector AS sector,
        @business AS business,
        @companyNumber AS companyNumber,
        @corporateNumber AS corporateNumber,
        @representation AS representation,
        @isSentDocument AS isSentDocument,
        @isSentAccount AS isSentAccount,
        @bankName AS bankName,
		@ownerName AS ownerName,
		@bankAccount AS bankAccount,
        @isAccountCertified AS isAccountCertified,
        @sentAccountInfo AS sentAccountInfo;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_SELECT_SELLER_USER_DETAIL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_SELECT_SELLER_USER_DETAIL`(
	IN		in_id		int
)
BEGIN
	-- 판매자 회원 상세 정보
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    IF in_id IS NULL THEN
    
		SET result = 0;
        SET message = 'Input data error';
        
    ELSE
		SET result = 1;
        SET message = '';
        
		SELECT 
			result, 
            message,
			tb1.id,
            tb1.companyName,
			tb1.email,
            tb1.companyPhone,
            tb1.representation,
            tb1.sellerPhone,
            tb1.sector,
            tb1.business,
            tb1.companyNumber,
            tb1.corporateNumber,
            tb4.isPhoneCertified,
            tb4.isSellerCertified,
            tb4.isAccountCertified,
            tb1.cDate,
            tb1.updatedBy,
            tb1.uDate,
            tb2.lastLiginDate,
            tb4.isSentDocument,
            tb4.isSentAccount,
            tb3.bankAccount,
            tb3.ownerName,
            tb3.bankName,
            tb3.cDate AS accountCDate,
            tb3.createdBy AS accountCreatedBy,
            tb3.uDate AS accountUDate,
            tb3.updatedBy AS accountUpdatedBy
		FROM seller_user AS tb1
        LEFT JOIN (
			SELECT email, MAX(cDate) AS lastLiginDate
				FROM signin_log
				GROUP BY email
		) AS tb2
        ON tb1.email = tb2.email
        LEFT JOIN seller_account AS tb3
		ON tb1.id = tb3.referenceId
        LEFT JOIN seller_auth AS tb4
        ON tb1.id = tb4.referenceId
        WHERE tb1.id = in_id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_UDATE_FORGOT_USER_PASSWORD` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_UDATE_FORGOT_USER_PASSWORD`(
	IN	in_email	varchar(100)	CHARSET UTF8,
    IN	in_password varchar(100),
    IN	in_code		varchar(200)
)
BEGIN
	-- 코드 검증 & 비밀번호 업데이트
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
    END;
    
    START TRANSACTION;
		
		SELECT COUNT(*)
			INTO @count
			FROM seller_user
			WHERE email = in_email;
		
		IF @count = 0 THEN
			SET result = 0;
			SET message = '잘못된 이메일입니다';
		ELSE
			CALL ISP_SELECT_CHECK_MAIL_CODE(in_email, in_code, NOW(), @result, @message);
			
			IF @result = 0 THEN
				SET result = @result;
				SET message = @message;
			ELSE
				UPDATE seller_user
					SET password = in_password, lastPwUpdate = NOW(), needPwChange = 'N'
					WHERE email = in_email;
				SET result = 1;
                SET message = '변경되었습니다';
			END IF;
		END IF;
    COMMIT;
    
    SELECT result, message;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_UDATE_USER_PASSWORD` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_UDATE_USER_PASSWORD`(
	IN	in_id			varchar(100)	CHARSET UTF8,
    IN	in_old_password varchar(100),
    IN	in_password		varchar(100),
    IN	in_group		varchar(50)
)
BEGIN
	-- 코드 검증 & 비밀번호 업데이트
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
    END;
    
    START TRANSACTION;
    
    IF (in_group = 'admin' OR in_group = 'operator') THEN
		SELECT COUNT(*)
			INTO @count
            FROM admin_user AS tb1
            WHERE tb1.userId = in_id AND tb1.password = in_old_password;
		
        IF @count = 0 THEN
			SET result = 0;
            SET message = '비밀번호를 확인해 주세요';
        ELSE
			UPDATE admin_user AS tb2 
				SET 
					tb2.password = in_password,
					tb2.lastPwUpdate = NOW(),
                    tb2.needPwChange = 'N',
                    tb2.uDate = NOW(),
                    tb2.updatedBy = in_email
                WHERE tb2.userId = in_id AND tb2.password = in_old_password;
			
            SET result = 1;
            SET message = '변경되었습니다';
        END IF;
        
	ELSEIF (in_group = 'seller' OR in_group = 'visitor') THEN
		SELECT COUNT(*)
			INTO @count
            FROM seller_user AS tb3
            WHERE tb3.email = in_id AND tb3.password = in_old_password;
		
        IF @count = 0 THEN
			SET result = 0;
            SET message = '비밀번호를 확인해 주세요';
        ELSE
			UPDATE seller_user AS tb4
				SET 
					tb4.password = in_password,
                    tb4.lastPwUpdate = NOW(),
                    tb4.needPwChange = 'N',
                    tb4.uDate = NOW(),
                    tb4.updatedBy = in_email
                WHERE tb4.email = in_id AND tb4.password = in_old_password;
			
            SET result = 1;
            SET message = '변경되었습니다';
        END IF;
    END IF;
    
    COMMIT;
    
    SELECT result, message;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_UPDATE_SELLER_PHONE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_UPDATE_SELLER_PHONE`(
	IN	in_email				varchar(100)	CHARSET UTF8,
    IN	in_sellerPhone			varchar(45),
    IN	in_sellerAlarmEmail		varchar(100)	CHARSET UTF8
)
BEGIN
	-- 셀러 회원 판매 담당자 전화번호 업데이트
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
    END;
    
    START TRANSACTION;
		
		SELECT COUNT(*), id
			INTO @count, @id
			FROM seller_user
			WHERE email = in_email;
		
		IF @count = 1 THEN
        
			UPDATE seller_user AS tb1 
				SET 
					tb1.sellerPhone = in_sellerPhone,
                    tb1.isPhoneCertified = 'N',
                    tb1.uDate = NOW(),
                    tb1.updatedBy = in_email,
                    tb1.sellerAlarmEmail = in_sellerAlarmEmail
				WHERE tb1.email = in_email;
				
			-- 관리자에게 알림            
			INSERT INTO operator_call (message, url, processId) VALUES (CONCAT(in_email,'님이 연락처 정보를 변경하였습니다'), CONCAT('/pages/seller-profile/',@id), 'changeSellerPhone');
			
            SET result = 1;
            SET message = '변경되었습니다';
		ELSE
			SET result = 0;
			SET message = '잘못된 이메일입니다';
		END IF;
    COMMIT;
    
    SELECT result, message;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `USP_UPDATE_SELLER_PROFILE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_UPDATE_SELLER_PROFILE`(
	IN	in_email				varchar(100)	CHARSET UTF8,
    IN	in_companyName 			varchar(100)	CHARSET UTF8,
    IN	in_representation		varchar(50)		CHARSET UTF8,
    IN	in_companyPhone			varchar(45),
    IN	in_sector				varchar(45)		CHARSET UTF8,
    IN	in_business				varchar(45)		CHARSET UTF8,
    IN	in_companyNumber		varchar(45),
    IN	in_corporateNumber		varchar(45)
)
BEGIN
	-- 셀러 회원 프로필 업데이트 (파일업로드는 사전 완료 됨)
    
    DECLARE 	result 		int(1)							default 0;
    DECLARE 	message 	varchar(50)		CHARSET UTF8 	default '';
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET result = 0;
        SET message = 'Database Error';
    END;
    
    START TRANSACTION;
		
		SELECT COUNT(*), id, sentDocument, isSellerCertified
			INTO @count, @id, @sentDocument, @isSellerCertified
			FROM seller_user
			WHERE email = in_email;
		
		IF @count = 1 THEN
			-- 업데이트
			UPDATE seller_user AS tb1 
				SET tb1.companyName =in_companyName, 
					tb1.representation = in_representation, 
					tb1.companyPhone = in_companyPhone,
					tb1.sector = in_sector,
					tb1.business = in_business,
					tb1.companyNumber = in_companyNumber,
					tb1.corporateNumber = in_corporateNumber,
					tb1.isSellerCertified = 'N',
					tb1.isPhoneCertified = 'N',
					tb1.sentDocument ='Y',
                    tb1.uDate = NOW(),
                    tb1.updatedBy = in_email
				WHERE tb1.email = in_email;
                
			-- 관리자에게 알림
            IF @isSellerCertified = 'Y' THEN
				INSERT INTO operator_call (message, url, processId) VALUES (CONCAT(in_email,'님이 사업자 인증을 재요청하였습니다'), CONCAT('/pages/seller-profile/',@id), 'changeSellerInfo');
            ELSEIF @sentDocument = 'N' THEN
				INSERT INTO operator_call (message, url, processId) VALUES (CONCAT(in_email,'님이 사업자 인증을 요청하였습니다'), CONCAT('/pages/seller-profile/',@id), 'changeSellerInfo');
            ELSEIF @sentDocument = 'Y' THEN
				INSERT INTO operator_call (message, url, processId) VALUES (CONCAT(in_email,'님이 사업자 인증을 재요청하였습니다'), CONCAT('/pages/seller-profile/',@id), 'changeSellerInfo');
            END IF;
            
			SET result = 1;
            SET message = '신청이 완료되었습니다';
		ELSE
			SET result = 0;
			SET message = '잘못된 이메일입니다';
		END IF;
    COMMIT;
    
    SELECT result, message;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-05  1:05:42
