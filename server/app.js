/* =======================
    LOAD THE DEPENDENCIES
==========================*/
const express = require('express');
var path    = require('path');
const bodyParser = require('body-parser');
const expressWinston = require('express-winston');
const winston = require('winston');

/* =======================
    LOAD THE CONFIG
==========================*/
const config = require('./config');
const port = config.port || 80;

/* =======================
    EXPRESS CONFIGURATION
==========================*/
const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: false,
    parameterLimit:50000
}));

// set the secret key variable for jwt
app.set('jwt-secret',config.secret.jwt);
 
// print the request log on console and save
app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console({
            level : "info",
            json: false,
            colorize: true
        })
    ]
}));

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'content-type, Authorization');
    next();
});

//set DB connection
app.use(async (req, res, next) => {
    const mysql = require('mysql2/promise');
    const dbconfig   = require.main.require('./config').db;

    if (app.db) 
    {
        app.db.end();
    }
    
    app.db = await mysql.createPool(dbconfig);
    req.db = app.db;
        
    return next();
});

// configure api router
app.use('/api', require('./routes/api'));

// Angular
app.use(express.static(path.resolve(__dirname, './public')));

// Angular
app.use(express.static(path.resolve(__dirname, './dist')));
app.get('*', function (req, res) {
    var indexFile = path.resolve(__dirname,'./dist/index.html');
    res.sendFile(indexFile);
});

// 404 error
app.use((req, res, next) => {
    res.status(404).send('일치하는 주소가 없습니다!');
});

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('server error');
});

// open the server
app.listen(port, function() {
    console.log(`Express is running on port ${port}`);
});
