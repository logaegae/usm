/**
 * Created by deepak.m.shrma@gmail.com on 2/7/14.
 */
const nodemailer = require("nodemailer");
const async = require("async");
const smtpPool = require('nodemailer-smtp-pool');
const appRoot = require('app-root-path');
const swig = require('swig-templates');
let config = require.main.require('./config');
const models = require.main.require('./models');

function BulkMailer(setting) {

    config.mailer = {
        service: setting.email.split('@')[1].split('.')[0],
        port: setting.smtpPort,
        user: setting.email,
        password: setting.mailPassword,
    }

    this.nodemailer = nodemailer.createTransport(smtpPool({
        service: config.mailer.service,
        host: config.domain,
        port: config.mailer.port,
        auth: {
            user: config.mailer.user,
            pass: config.mailer.password,
        },
        tls: {
            rejectUnauthorize: false,
        },
        maxConnections: 5,
        maxMessages: 10,
    }));

    this.isVerbose = true;
}
BulkMailer.prototype.send = function (option, single, callback) {

    const from = option.from ? option.from : '엔케이펀딩 < ' + config.mailer.user + ' >';
    const to = option.to;
    const subject = option.subject;

    let info = {
        from,
        to,
        subject,
    };

    if (option.render) {
        const tpl = swig.compileFile(appRoot + '/views/mail/' + option.render + '.html');
        info.html = tpl(option.data);
    } else if (option.text) {
        info.text = option.text;
    } else if (option.html) {
        info.html = option.html;
    }

    var that = this;
    var receivers = [];
    var names = [];
    if (single) {
        receivers = info.to.split(",");
    } else {
        receivers = [info.to];
        names = [info.name];
    }

    var tasks = [];
    async.each(receivers, function (user, eachCB) {
        tasks.push(function (callback) {
            info.to = user.split('/')[1];
            let name = user.split('/')[0];
            if (info.html) info.html = info.html.replace(/\{회원아이디\}/g, info.to).replace(/\{이름\}/g, name);
            if (info.text) info.html = info.html.replace(/\{회원아이디\}/g, info.to).replace(/\{이름\}/g, name);
            that.nodemailer.sendMail(info, callback);
        });
        eachCB();
    }, function (error) {
        if (!error) {
            async.parallel(tasks, function (error, results) {
                if (error) {
                    throw error;
                }
                else {
                    if (that.isVerbose) {
                        console.info('Mail sent to respective users!');
                    }
                    return callback(null, results);
                }
            });
        }
        else {
            if (that.isVerbose) {
                console.error(error);
            }
            return callback(error);
        }
    });
};
module.exports = BulkMailer;