const fs = require('fs');

const deleteUploaded = (req, files) => {
    return new Promise(async (resolve, reject) => {
        if(!files || typeof files !== "object" || !files.length) {
            reject({success: false, message: "Parameter error"});
        }
        for(i in files){
            const file = files[i];
            if(!file || !file.path) reject({success: false, message: "file data error"});
            const [ queryRows, queryFields ] = await req.db.query("DELETE FROM attach_file WHERE path = ?",
            [file.path]);
            fs.unlinkSync("public" + file.path);
        }
        resolve({success: true, message: ""})
    });
}
module.exports = deleteUploaded;