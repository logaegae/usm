module.exports = {
    PASSWORD_REGEXP: /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/,
    CODE_REGEXP: /^[0-9+]{6}$/,
    EMAIL_REGEXP: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    ID_REGEXP: /^[A-Za-z0-9+]{4,12}$/,
    PHONE_REGEXP: /^[0-9]*$/,
    NUMBER_REGEXP: /^[0-9]*$/,
    COMPANYNUMBER_REGEXP: /^[0-9\-]*$/,
    YN_REGEXP: /^[YN]$/,
    SYMBOL_REGEXP_REPLACE: /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi,
    COMPANYNAME_REGEXP_MATCH: /[\{\}\[\]\/?.,;:|*~`!^_+<>@\#$%\\\=\'\"]/gi,
}
