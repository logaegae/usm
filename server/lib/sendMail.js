const nodemailer = require('nodemailer');
const appRoot =  require('app-root-path');
const swig = require('swig-templates');
let config = require.main.require('./config');

module.exports = async (option) => {

    let transporter = nodemailer.createTransport(config.mailer);

    const from = option.from ? option.from : '유벤더 < '+config.email+' >';
    const to = option.to;
    const subject = option.subject;

    let mailOptions = {
        from,
        to,
        subject,
    };

    if(option.render) {
        const tpl = swig.compileFile(appRoot + '/views/mail/'+option.render+'.html');
        mailOptions.html = tpl(option.data);
    } else if(option.text) {
        mailOptions.text = option.text;
    } else if(option.html) {
        mailOptions.html = option.html;
    }
    
    // 본문에 html이나 text를 사용할 수 있다.

    const info = await transporter.sendMail(mailOptions);

    console.log("Message sent: %s", info.messageId);

    return info;
}