const models = require.main.require('./models');
const net = require('net');
 
const stringByteLength = (s, b, i, c) => {
    for (b = i = 0; c = s.charCodeAt(i++); b += c >> 11 ? 3 : c >> 7 ? 2 : 1);
    return b
};

const cutByLen = (str, maxByte) => {

    for (b = i = 0; c = str.charCodeAt(i);) {
        b += c >> 7 ? 2 : 1;
        if (b > maxByte) break;
        i++;
    }
    return str.substring(0, i);
}

//option = {name, email, title, content, tel, icodeToken}
const setJson = (option) => {

    const content = option.content;
    const tel = option.tel;
    const icodeToken = option.icodeToken;
    let title = option.title || '';

    //내용이 길 경우 제목
    if (stringByteLength(content) >= 90) title = cutByLen(content, 30);
    //2000바이트 이상 자르기
    if (stringByteLength(content) > 2000) content = cutByLen(content, 2000);

    const icode = `{"key":"${icodeToken}","tel":"${tel}","cb":"025431358","msg": "${content}","title":"${title}", "date":"","charset":"utf-8"}`;

    let length = stringByteLength(icode);
    length = '0000' + length.toString();
    length = length.substr(-4);

    return ['06', length, icode];
}

module.exports = async (text, number) => {
    console.log(text, number)
    let setting = await models.config.findById(1);
    
    let client = await net.connect({ port: 9201, host: '211.172.232.124' });

    client.on('connect', function () {
        console.log('client connected');
        
        let smsOption = {
            tel: number,
            content : text,
            icodeToken: setting.dataValues.icodeToken
        }
        let result = setJson(smsOption);

        client.write(result[0], function () {
            client.write(result[1], function () {
                client.write(result[2]);
                client.end();
            });
        });

        return{ message: 'send' };
    });
    client.setTimeout(500);
    client.on('data', function (data) {
        console.log(data.toString());
        client.end();
    });
    client.on('end', function () {
        client.destroy();
        console.log('client disconnected');
    });
    client.on('error', function (err) {
        console.log(err);
        client.end();
    });
    client.on('timeout', function (err) {
        console.log('client timeout');
        client.end();
    });
}