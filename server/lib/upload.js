const multer = require('multer');
const crypto = require('crypto');
const mime = require('mime');
const fs = require('fs');

/* Setup File upload */
exports.upload = function(req, res, obj) {

    return new Promise((resolve, reject) => {
        const { fieldName, dest, processId, referenceId, temp } = obj;
        const creator = req.decoded.id || req.decoded.email || false;

        if(!creator) reject({success:false, message:"권한이 없습니다"});

        const upload = multer({
            limits: { fileSize: 10 * 1024 * 1024 },
            storage: multer.diskStorage({
                destination(req, file, cb) {
                    
                    folder = `public/uploads/${dest}/`;

                    if(fs.existsSync(folder) === false) {
                        fs.mkdirSync(folder);
                    }
                    
                    cb(null, folder);
                },
                filename(req, file, cb) {
                    crypto.pseudoRandomBytes(16, function(err, raw) {
                        cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
                    });
                }
            })
        }).array(fieldName);

        upload(req, res, async (err) => {
            if (err) reject({success:false, message: err.message});
            else {

                if(!req.files || !req.files.length) resolve({success:false, message:"선택된 파일이 없습니다", data: {list : []}});

                let files = [];
                for(file of req.files) {
                    let path = file.path.replace(/\\/g, '/');
                    path = path.replace('public/', '');
                    files.push({
                        size : file.size,
                        fileName : file.filename,
                        originalName : file.originalname,
                        path : '/' + path,
                        mimeType : file.mimetype
                    });
                }
                //temp 일 경우 DB에 기록을 하지 않는다
                if(!temp) {
                    for(i in files){
                        const file = files[i];
                        const [ queryRows, queryFields ] = await req.db.query('INSERT INTO attach_file (processId, referenceId, path, fileName, originalName, mimeType, size, createdBy) VALUES (?, ?, ?, ?, ?, ?, ?, ?);',
                        [processId, referenceId, file.path, file.fileName, file.originalName, file.mimeType, file.size, creator]);
                                            
                        if(queryRows.affectedRows == 0) {
                            reject({success:false, message:"시스템 에러 [LU-11]"});
                        }
                    }
                }
                resolve({success:true, message:'', data: {list :files}});
            }
        });
    });
};
// 파일을 옮길 경우
exports.moveFiles = async (files, dest) => {
    result = [];
    folder = `public/uploads/${dest}`;

    if(fs.existsSync(folder) === false) {
        fs.mkdirSync(folder);
    }

    for(i in files){
        const newPath = folder + '/' + files[i].fileName;
        const success = await new Promise((resolve, reject) => {
            fs.rename('public' + files[i].path, newPath, (err)=>{
                if(err) reject(false);
                resolve(true);
            });
        });
        //실패 시 원복을 해야 하지만 안해도 될 것 같음
        files[i].path = newPath.replace('public', '');
        result.push(files[i]);
    }
    return result;
}
// 기록만 할 경우
exports.recordFiles = async (req, processId, referenceId, files, creator) => {

    await req.db.query('DELETE FROM attach_file WHERE processId=? AND referenceId=?;',
        [processId, referenceId]);
        
    for(let i in files){
        const file = files[i];
        const [ queryRows, queryFields ] = await req.db.query('INSERT INTO attach_file (`processId`, `referenceId`, `path`, `fileName`, `originalName`, `mimeType`, `size`, `createdBy`, `order`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);',
        [processId, referenceId, file.path, file.fileName, file.originalName, file.mimeType, file.size, creator, parseInt(i)+1]);

        if(queryRows.affectedRows == 0) {
            return ({success:false, message:"시스템 에러 [LU-11]"});
        }
    }
    return ({success: true, message: ""});
}
// 삭제할 경우
exports.deleteUploaded = (req, files) => {
    return new Promise(async (resolve, reject) => {
        if(!files || typeof files !== "object" || !files.length) {
            reject({success: false, message: "Parameter error"});
        }
        for(i in files){
            const file = files[i];
            if(!file || !file.path) reject({success: false, message: "file data error"});
            const [ queryRows, queryFields ] = await req.db.query("DELETE FROM attach_file WHERE path = ?",
            [file.path]);
            fs.unlinkSync("public" + file.path);
        }
        resolve({success: true, message: ""})
    });
}