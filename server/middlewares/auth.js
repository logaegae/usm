const jwt = require('jsonwebtoken');
const cipher = require.main.require('./lib/cipher');
const {EMAIL_REGEXP} = require.main.require('./lib/regEx');

const authMiddleware = (req, res, next) => {
    // read the token from header or url
    const token = req.headers['authorization'];

    // token does not exist
    if(!token) {
        return res.status(403).json({
            success: false,
            message: '로그인을 해주세요.'
        });
    }

    // create a promise that decodes the token
    const p = new Promise(
        (resolve, reject) => {
            jwt.verify(token, req.app.get('jwt-secret'),{algorithm: 'HS512'}, (err, result) => {
                if(err) reject(err);
                let decoded = cipher.decode(result.data);
                
                decoded = JSON.parse(decoded);
                if(!decoded.id && !decoded.email) reject('data error');
                if(!decoded.group) reject('data error');
                if(decoded.email && !EMAIL_REGEXP.test(decoded.email) ) reject('data error');
                resolve(decoded);
            });
        }
    );

    // if it has failed to verify, it will return an error message
    const onError = (error) => {
        res.status(403).json({
            success: false,
            message: typeof error === 'string' ? error : error.message
        });
    };

    // process the promise
    p.then((decoded)=>{
        req.decoded = decoded;
        next();
    }).catch(onError);
}

module.exports = authMiddleware;
