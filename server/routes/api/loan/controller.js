const models = require.main.require('./models');
const Op = require('sequelize').Op;
const calculate = require.main.require('./lib/calculate');

/* code */
const code = require.main.require('./config/code');

/**
 * Loan Application List
 */
//대출신청 리스트 보기
exports.getloanApplicationList = async (req, res) => {
    console.log(req.body)

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    let pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;
    
    let where = {delYn : 'N'};
    let order = [];
    let loanApplicationStatus = [];

    if(req.body.selected){
        where.loanApplicationStatus = req.body.selected.loanApplicationStatus;
    }

    //검색어 입력시
    if(req.body.searchText) {
        where = {
            [Op.or] : [{
                name : { [Op.like]: '%'+req.body.searchText+'%' }
            },
            {
                email : { [Op.like]: '%'+req.body.searchText+'%' }
            }],
            delYn : 'N'
        }
    }

    //정렬옵션 선택시
    if(req.body.orderSelected) {
        if(req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC'){
            order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['loanApplicationIdx', 'DESC']);
        }else{
            console.log(req.body.orderSelected.type)
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    }else{
        order = [
            ['loanApplicationIdx', 'DESC']
        ];
    }

    
    let result = await models.loanApplication.findAll({
        where,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
        order
    });

    let count = await models.loanApplication.count({where : {
        delYn : 'N'
    }});

    for(i=0;i<code.loanApplicationStatus.length;i++){
        loanApplicationStatus[i] = {};
        loanApplicationStatus[i].value = code.loanApplicationStatus[i].value;
        loanApplicationStatus[i].name = code.loanApplicationStatus[i].name;
        loanApplicationStatus[i].count = await models.loanApplication.count({where : {
            delYn : 'N',
            loanApplicationStatus : code.loanApplicationStatus[i].value
        }});
    }

    res.json({
        list : result,
        count,
        loanApplicationStatus,
        selected : req.body.selected
    });
};

//모집완료 된 프로젝트 보기
exports.getProjectList = async (req, res) => {
    
    let where = {
        delYn : 'N',
        [Op.or] : [{
            projectStatus : code.projectStatus[2].value
        },{
            projectStatus : null
        }]
    };
    
    let result = await models.projectList.findAll({
        where,
        order : [['projectIdx','DESC']],
        attributes : ['projectIdx','projectSubtitle','projectName','targetMoney','earningRate','startDate','investPeriod', 'interestIncome', 'platformIncome']
    });

    result.forEach((e,i) => {
        e.projectName = e.projectSubtitle +' '+ e.projectName;
    });

    res.json({
        list : result
    });
};

//상환 관리에 보여질 프로젝트 리스트 보기
exports.getProjectListInRepayment = async (req, res) => {
    
    let where = {
        delYn : 'N'
    };
    
    let result = await models.projectList.findAll({
        where,
        order : [['projectIdx','DESC']],
        attributes : ['projectIdx','projectSubtitle','projectName','targetMoney','earningRate','startDate','investPeriod']
    });

    result.forEach((e,i) => {
        e.projectName = e.projectSubtitle +' '+ e.projectName;
    });

    res.json({
        list : result
    });
};

//가상계좌 있는 회원 보기
exports.getUserList = async (req, res) => {
    
    let where = {
        memberStatus : code.memberStatus[0].value
    };
    
    let result = await models.user.findAll({
        where,
        order : [['email','ASC']],
        attributes : ['userIdx','name','email', 'companyName', 'businessType'],
        include : [{
            model : models.userBankAccount,
            required : true,
            where : {
                virtualBankConfirm : 'Y'
            }
        }]
    });

    result.forEach((e,i) => {
        let name = e.name;
        if(e.businessType !== code.businessType[0].value) name = e.companyName;

        e.name = name + '(' + e.email + ')';
    });

    res.json({
        list : result
    });
};

//대출신청 상세내용 보기
exports.getloanApplicationView = async (req, res) => {
    console.log(req.body);

    let result = await models.loanApplication.findOne({
        where : {
            loanApplicationIdx : req.body.id
        },
        include : [{
            model : models.projectList,
            attributes : ['projectIdx', 'projectSubtitle', 'projectName', 'startDate', 'investPeriod', 'targetMoney', 'earningRate']
        },{
            model : models.user,
            attributes : ['name', 'email', 'userIdx', 'mobile', 'guid', 'companyName', 'totalInvestedMoney'],
            include : [{
                model : models.userBankAccount
            }]
        }]
    })

    res.json(result);
}

//대출상태 수정
exports.setloanApplicationStatus = async (req, res) => {
    console.log(req.body);

    let list = req.body;

    models.sequelize.transaction( async (t) => {

        let promises = [];
        
        for (var i = 0; i < list.length; i++) {

            let obj = {}
            
            if(list[i].loanStatus) obj.loanStatus = list[i].loanStatus;
            if(list[i].totalInterest) obj.totalInterest = list[i].totalInterest;

            newPromise = models.loanApplication.update(obj, {
                where : {
                    loanApplicationIdx : list[i].loanApplicationIdx
                }, 
                transaction: t
            });
            
            if(Object.getOwnPropertyNames(obj).length != 0) promises.push(newPromise);
        }
                
        return Promise.all(promises);

    }).then(function(result) {

        res.json({error : 0, message:'저장되었습니다'});

    }).catch(function(err) {

        res.json({error : 1, message:'에러'});

    });

}
//대출신청 삭제
exports.delLoanApplicationView = async (req, res) => {
    console.log(req.body);

    let item = {
        delYn : 'Y'
    };

    await models.loanApplication.update(item, {
        where : {
            loanApplicationIdx : req.body.loanApplicationIdx
        }
    });

    res.json({message:'삭제되었습니다.'});
}

//대출신청 상세내용 생성 및 수정
exports.setloanApplicationView = async (req, res) => {
    console.log(req.body);

    let item = req.body;

    item.applicationAmount = typeof item.applicationAmount === 'string' ? parseInt(item.applicationAmount.replace(/,/g,'')) : null;
    item.confirmAmount =  typeof item.confirmAmount === 'string' ? parseInt(item.confirmAmount.replace(/,/g,'')) : null;
    item.updatedDate = new Date();
    
    if(item.idx === 'new'){
        delete item.idx;
        item.registedDate = new Date();
        item.delYn = 'N';
        item.loanStatus = code.loanStatus[0].value;
        await models.loanApplication.create(item);
    }else{
    
        await models.loanApplication.update(item,{
            where : {
                loanApplicationIdx : req.body.loanApplicationIdx
            }
        });
    }

    res.json({message:'저장되었습니다.'});
}

//대출기록 시작
exports.loanStart = async (req, res) => {
    console.log(req.body);
    let item = req.body;

    const applicationAmount = typeof item.applicationAmount === 'string' ? parseInt(item.applicationAmount.replace(/,/g,'')) : null;
    const confirmAmount =  typeof item.targetMoney === 'string' ? parseInt(item.confirmAmount.replace(/,/g,'')) : null;
    const projectIdx = item.projectIdx;
    const loanApplicationIdx = item.loanApplicationIdx;

    //상환중인지 확인
    let repayment = await models.loanRepayment.findAll({
        where : {
            loanApplicationIdx : item.loanApplicationIdx
        }
    });

    if(repayment.length && repayment.length > 0) return res.json({error : '해당 대출신청은 이미 대출상환 기록이 있습니다.'})
    
    //프로젝트 상태변경
    await models.projectList.update({
        projectStatus : code.projectStatus[3].value
    },{where : {
        projectIdx
    }});

    //상환기록 계산
    let result = await calculate.startLoanRepayment(applicationAmount, confirmAmount, projectIdx, loanApplicationIdx);

    if(result.status === 'SUCCESS') {
         result = {message : '대출기록을 완료하였습니다.'};
    }else {
        result = {message : result.error};
    }
    return res.json(result);
}

/**
 * Loan Repayment
 */

 //대출 상환 목록 보기
exports.getLoanRepaymentList = async (req, res) => {
    console.log(req.body)

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    let pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;
    
    let where = {
        delYn : 'N',
        [Op.not] : {
            [Op.or] : [
                {
                    loanStatus : code.loanStatus[0].value
                },
                {
                    loanStatus : code.loanStatus[1].value
                }
            ]
        }
    };
    let order = [];
    let where2 = {};

    //정렬옵션 선택시
    if(req.body.orderSelected) {
        if(req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC'){
            order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['loanApplicationIdx', 'DESC']);
        }else{
            console.log(req.body.orderSelected.type)
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    }else{
        order = [
            ['loanApplicationIdx', 'DESC']
        ];
    }

    //검색어 입력시
    if(req.body.searchText) {
        where2 = {
            [Op.or] : [{
                name : { [Op.like]: '%'+req.body.searchText+'%' }
            },
            {
                email : { [Op.like]: '%'+req.body.searchText+'%' }
            }]
        }
    }
    
    let result = await models.loanApplication.findAll({
        where,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
        order,
        include : [{
            model : models.projectList,
            attributes : ['projectSubtitle', 'projectName','targetMoney'],
            required : true,
            // where : where2
        },{
            model : models.user,
            attributes : ['name', 'email', 'mobile', 'userIdx', 'guid', 'companyName', 'totalInvestedMoney'],
            required : true,
            where : where2,
            include : [{
                model : models.userBankAccount,
                required : true
            }]
        }]
    });

    res.json({
        list : result
    });
}

//대출상환 상세내용 보기
exports.getloanRepaymentView = async (req, res) => {
    console.log(req.body)

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    let pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;
    
    let where = {
        delYn : 'N',
        loanApplicationIdx : req.body.id
    };
    let where1 = {};
    let where2 = {};
    let order = [];
    let loanApplicationStatus = [];
    let selected = req.body.selected;

    if(selected){
        let dateRange = false;
        Object.keys(selected).map(function(key, i) {
            if(key === 'projectIdx' && selected[key]) where1.projectIdx = selected[key];
            if(key === 'repaymentStatus' && selected[key]) where.repaymentStatus = selected[key];
            if(key === 'repaymentType' && selected[key]) where.repaymentType = selected[key];
            if(key === 'startDate' && selected[key]){
                dateRange = true;
            }else{
                dateRange = false;
            }
            if(key === 'endDate' && selected[key]){
                dateRange = true;
            }else{
                dateRange = false;
            }
        });
        if(dateRange){
            let startDate = selected['startDate'];
            let endDate = selected['endDate'];

            where.expectedRepaymentDate = {
                [Op.and]: {
                    [Op.gte]: startDate,
                    [Op.lte]: endDate
                }
            }
        }
    }

    //검색어 입력시
    if(req.body.searchText) {
        where2 = {
            [Op.or] : [{
                name : { [Op.like]: '%'+req.body.searchText+'%' }
            },
            {
                email : { [Op.like]: '%'+req.body.searchText+'%' }
            }]
        }
    }
    
    const result = await models.loanRepayment.findAll({
        where,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
        order : [['loanRepaymentIdx', 'DESC']],
        include : [{
            model : models.loanApplication,
            attributes : ['projectIdx'],
            required : true,
            include : [{
                model : models.projectList,
                attributes : ['projectSubtitle', 'projectName','targetMoney'],
                required : true,
                where : where1
            },{
                model : models.user,
                attributes : ['name','email', 'guid', 'mobile', 'userIdx', 'companyName', 'totalInvestedMoney'],
                required : true,
                where : where2,
                include : [{
                    model : models.userBankAccount,
                    required : true
                }]
            }]
        }]
    });

    res.json(result);
}

//대출상환 상세내용 수정
exports.setloanRepaymentView = async (req, res) => {
    console.log(req.body)

    let list = req.body;

    models.sequelize.transaction( async (t) => {

        let promises = [];
        
        for (var i = 0; i < list.length; i++) {

            let obj = {}
            
            if(list[i].repaymentDate) obj.repaymentDate = list[i].repaymentDate;
            if(list[i].repaymentStatus) obj.repaymentStatus = list[i].repaymentStatus;

            newPromise = models.loanRepayment.update(obj, {
                where : {
                    loanRepaymentIdx : list[i].loanRepaymentIdx
                }, 
                transaction: t
            });
            
            if(Object.getOwnPropertyNames(obj).length != 0) promises.push(newPromise);
        }
                
        return Promise.all(promises);

    }).then(function(result) {

        res.json({error : 0, message:'저장되었습니다'});

    }).catch(function(err) {

        res.json({error : 1, message:'에러'});

    });
}