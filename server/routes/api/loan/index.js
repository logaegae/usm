const router = require('express').Router();
const controller = require('./controller');
const authMiddleware = require.main.require('./middlewares/auth');

for(let fn in controller){
    let service = controller[fn];
    if (typeof service === 'function') {
        router.use('/'+fn, authMiddleware);
        router.post('/'+fn, service);
    }
}


module.exports = router;
