const models = require.main.require('./models');
const sendMail = require.main.require('./lib/sendMail');
/**
 * mail config
 */

exports.getMailList = async (req, res) => {
    const mail = await models.mail.findAndCountAll({
        where : {
            delYn : 'N'
        }
    });
    res.json({
        list : mail.rows,
        count : mail.count
    });
};

exports.getMailView = async (req, res) => {
    console.log(req.body);

    const result = await models.mail.findOne({where : {
        mailIdx : req.body.id
    }});

    res.json(result);
}

exports.getMailOptionList = async (req, res) => {
    const result = await models.config.findOne({
        where : {
            configIdx : 1
        }
    });
    
    res.json(result.mailOption);
};

exports.setMailOption = async (req, res) => {
    console.log(req.body);
    let item = {};
    for(i=1;i<6;i++){
        item['option'+i] = req.body['option'+i] || null
    }

    let result = await models.config.findOne({
        where : {
            configIdx : 1
        }
    });

    result.mailOption = item;
    await result.save();

    res.json({
        message: "저장되었습니다."
    });
}

exports.setMailView = async (req, res) => {
    console.log(req.body);
    let item = req.body;
    let message = '';
    const idx = item.idx;
    delete item.idx;

    if(idx === 'new'){
        item.registedDate = new Date();
        item.updatedDate = new Date();
        await models.mail.create(item);
        message = "저장되었습니다.";
    }else{
        item.updatedDate = new Date();
        let result = await models.mail.update(item,{
            where :{
                mailIdx : idx
            }
        });
        message = req.body.delYn === 'Y' ? "삭제되었습니다." : "저장되었습니다.";
    }
    
    res.json({
        message
    });
}

// 메일 삭제
exports.setMailViewDel = async (req, res) => {
    console.log(req.body);
    let item = req.body;
    const mailIdx = req.body.idx;
    delete item.idx;
    item.updatedDate = new Date();

    await models.mail.update(item, {
        where: {
            mailIdx: mailIdx
        }
    });
    let setting = await models.config.findById(1);
    setting.mailOption = setting.dataValues.mailOption
    Object.keys(setting.dataValues.mailOption).forEach((e,i) => {
        if(setting.dataValues.mailOption[e] == mailIdx) setting.mailOption[e] = null
    });

    await setting.save();

    res.json({ message: '저장되었습니다.' });
}

exports.sendMail = async (req, res) => {
    console.log(req.body);
    const item = await models.mail.findOne({
        where : {
            mailIdx : req.body.idx
        }
    });
    if(!item) {
        res.json({
            error : true,
            message: '아이디 에러'
        }); 
    }
    const config = await models.config.findOne({
        where : {
            configIdx : 1
        }
    })

    item.content = item.content.replace(/\{회원아이디\}/g,config.email).replace(/\{이름\}/g,'관리자');
    
    await sendMail({
        to: config.email,
        subject: item.title,
        render: 'mailing_layout',
        data: {
            title : item.title,
            content : item.content
        }
    });

    res.json({
        error : false,
        message: '메일발송을 처리하였습니다.'
    });
}