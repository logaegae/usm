const models = require.main.require('./models');
const mapCodeToName = require.main.require('./lib/mapCodeToName');
const cipher = require.main.require('./lib/cipher');
const excel = require('node-excel-export');
const Op = require('sequelize').Op;
const calculate = require.main.require('./lib/calculate');
const dateToString = require.main.require('./lib/dateToString');
/* code*/
const code = require.main.require('./config/code');

/**
 * Change pwd info
 */
exports.changePwd = async (req, res) => {
    console.log(req.body);

    let result = {};
    let message = null;
    const newPwd = cipher.encode(req.body.password);

    result = await models.adminMember.findOne({
        where: {
            id: req.body.id
        }
    });
    console.log(newPwd);

    if (result && result.pwd === cipher.encode(req.body.oldPwd)) {

        result.pwd = newPwd;
        await result.save();
        message = {
            success: true,
            message: '변경하였습니다'
        };

    } else {

        message = {
            success: false,
            message: '비밀번호를 확인해주세요'
        };

    }

    res.json(message);
}
/**
 * member Info excel download
 */
exports.getMemberListExcel = async (req, res) => {
    let userJson = [];
    let user = await models.user.findAll({
        include: [{
            model: models.userBankAccount,
            attributes: ['bankAccount', 'bankName', 'accountOwnerName', 'virtualAccountBalance']
        }]
    });

    for (let one of user) {
        let obj = {};
        let item = one.dataValues;

        obj['아이디'] = item['email'] || '-';
        obj['이름'] = item['name'] || '-';
        obj['회원분류'] = mapCodeToName(code.memberStatus, item['memberStatus']) || '-';
        obj['투자등급'] = mapCodeToName(code.investorType, item['investorType']) || '-';
        obj['성별'] = item['sex'] == '1' ? '남' : item['sex'] == '0' ? '여' : '-';
        obj['생년월일'] = item['birthday'] || '-';
        obj['주민번호'] = item['socialNum'] ? item['socialNum'].split('-')[0] + '-' + cipher.decode(item['socialNum'].split('-')[1]) : '-';
        obj['연락처'] = item['mobile'] || '-';
        obj['우편번호'] = item['postNo'];
        obj['주소'] = item['basicAddress'] + item['detailAddress'] || '-';
        if (item.userBankAccount && item.userBankAccount.dataValues) {
            obj['은행'] = item.userBankAccount.dataValues.bankAccount || '-';
            obj['계좌번호'] = item.userBankAccount.dataValues.bankAccount || '-';
            obj['계좌주'] = item.userBankAccount.dataValues.accountOwnerName || '-';
            obj['예치금'] = item.userBankAccount.dataValues.virtualAccountBalance +'원' || '-';
        }else{
            obj['은행'] = '-';
            obj['계좌번호'] = '-';
            obj['계좌주'] = '-';
            obj['예치금'] = '-';
        }
        obj['사업자분류'] = mapCodeToName(code.businessType, item['businessType']) || '-';
        obj['사업자승인'] = item['businessTypeConfirmYn'] == 'Y' ? '예' : item['businessTypeConfirmYn'] == 'N' ? '아니오' : '-';
        obj['대부업승인'] = item['loanCompanyConfirm'] == 'Y' ? '예' : item['loanCompanyConfirm'] == 'N' ? '아니오' : '-';
        obj['회사명'] = item['companyName'] || '-';
        obj['법인번호'] = item['corporateNo'] || '-';
        obj['사업자번호'] = item['businessNo'] || '-';
        obj['대표자이름'] = item['presidentName'] || '-';
        obj['누적투자금액'] = item['totalInvestedMoney'] + '원' || '-';
        obj['이메일수신'] = item['agreeEmail'] == 'Y' ? '예' : item['agreeEmail'] == 'N' ? '아니오' : '-';
        obj['SMS수신'] = item['agreeSms'] == 'Y' ? '예' : item['agreeSms'] == 'N' ? '아니오' : '-';
        obj['로그인일시'] = dateToString(item['loginDate']) || '-';
        obj['탈퇴일자'] = dateToString(item['withdrawalDate']) || '-';
        obj['접근차단일자'] = dateToString(item['excludedDate']) || '-';
        obj['가입일시'] = dateToString(item['registedDate']) || '-';
        userJson.push(obj);
    }

    const styles = {
        center: {
            alignment: {
                horizontal: 'center'
            }
        }
    };

    const heading = [
        [{
            value: 'NK FUNDING 회원목록',
            style: styles.center
        }]
    ];

    const merges = [{
        start: {
            row: 1,
            column: 1
        },
        end: {
            row: 1,
            column: 26
        }
    }];

    const specification = (arg) => {
        let obj = {};
        for (let key in arg) {
            obj[key] = {
                displayName: key, // <- Here you specify the column header
                headerStyle: styles.center, // <- Header style
                width: 60
            }
        }
        return obj;
    };

    const report = excel.buildExport(
        [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
            {
                name: '회원 목록', // <- Specify sheet name (optional)
                heading: heading, // <- Raw heading array (optional)
                merges: merges, // <- Merge cell ranges
                specification: specification(userJson[0]),
                data: userJson // <-- Report data
            }
        ]
    );

    return res.send(report);
}

/**
 * Member info
 */
exports.getMemberList = async (req, res) => {
    console.log(req.body);

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    let pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;

    let normalCount = 0,
        excludedCount = 0,
        withdrawalCount = 0;
    let where = {};
    let order = [];

    if (req.body.memberStatus) where.memberStatus = req.body.memberStatus;

    //검색어 입력시
    if (req.body.searchText) {
        where = {
            [Op.or]: [{
                name: { [Op.like]: '%' + req.body.searchText + '%' }
            },
            {
                companyName: { [Op.like]: '%' + req.body.searchText + '%' }
            },
            {
                email: { [Op.like]: '%' + req.body.searchText + '%' }
            }]
        }
    }

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['userIdx', 'DESC']);
        } else {
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['userIdx', 'DESC']
        ];
    }


    let result = await models.user.findAndCountAll({
        where,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
        order,
        attributes: {
            exclude: ['pwd']
        },
        include : [{
            model : models.userBankAccount,
            attributes : ['virtualAccountBalance', 'virtualBankConfirm']
        }]
    });

    normalCount = await models.user.count({
        where: { memberStatus: code.memberStatus[0].value }
    });
    excludedCount = await models.user.count({
        where: { memberStatus: code.memberStatus[1].value }
    });
    withdrawalCount = await models.user.count({
        where: { memberStatus: code.memberStatus[2].value }
    });

    totalCount = normalCount + excludedCount + withdrawalCount;

    res.json({
        list: result.rows,
        count: totalCount,
        normalCount,
        excludedCount,
        withdrawalCount
    });
}

// 회원정보 조회
exports.getMemberInfoView = async (req, res) => {
    console.log(req.body);

    let result = await models.user.findOne({
        where: {
            userIdx: req.body.id
        },
        attributes: {
            exclude: ['pwd']
        },
        include: [{
            model: models.userBankAccount
        }]
    });
    
    if(result.dataValues.socialNum) {
        result.dataValues.socialF = result.dataValues.socialNum.split('-')[0];
        result.dataValues.socialB = cipher.decode(result.dataValues.socialNum.split('-')[1]);
    }
    res.json(result);
}
//회원정보 수정
exports.setMemberInfoView = async (req, res) => {

    console.log(req.body);
    let item = req.body;
    delete item.userBankAccount;

    if(item.socialF && item.socialB) item.socialNum = item.socialF + '-' + cipher.encode(item.socialB);

    if (item.pwd) {
        item.pwd = cipher.encode(item.pwd);
    }
    if(item.excludedYn === 'Y'){
        item.memberStatus = code.memberStatus[1].value;
    }else if(!item.withdrawalDate){
        item.memberStatus = code.memberStatus[0].value;
    }else{
        item.memberStatus = code.memberStatus[2].value;
    }

    let setting = await models.config.findById(1);
    let maxOption = setting.dataValues.maxOption;
    maxOption.forEach((e,i) => {
        if(e.value === item.investorType) {
            item.totalInvestableAmount = e.config.maximum;
            item.projectInvestableAmount = e.config.projectMaximum;
        }
    });
    
    await models.user.update(item, {
        where: {
            userIdx: item.userIdx
        }
    });

    await calculate.IableA(item.userIdx);
        
    res.json({
        message: "저장되었습니다."
    });
}
//회원정보에서 아이디 중복체크
exports.checkDuplication = async (req, res) => {
    console.log(req.body);
    const count = await models.user.count({
        where: {
            email: req.body.email
        }
    });

    res.json(count === 0 ? { isDupli: true } : { isDupli: false });
}

//회원 대출신청 내역 보기
 exports.getLoanRepaymentList = async (req, res) => {
    console.log(req.body)

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    let pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;
    
    let where = {
        userIdx : req.body.userIdx,
        delYn : 'N',
        [Op.not] : {
            [Op.or] : [
                {
                    loanStatus : code.loanStatus[0].value
                },
                {
                    loanStatus : code.loanStatus[1].value
                }
            ]
        }
    };
    let order = [];
    let where2 = {};

    //정렬옵션 선택시
    if(req.body.orderSelected) {
        if(req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC'){
            order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['loanApplicationIdx', 'DESC']);
        }else{
            console.log(req.body.orderSelected.type)
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    }else{
        order = [
            ['loanApplicationIdx', 'DESC']
        ];
    }

    //검색어 입력시
    if(req.body.searchText) {
        where2 = {
            [Op.or] : [{
                name : { [Op.like]: '%'+req.body.searchText+'%' }
            },
            {
                email : { [Op.like]: '%'+req.body.searchText+'%' }
            }]
        }
    }
    
    let result = await models.loanApplication.findAndCountAll({
        where,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
        order,
        include : [{
            model : models.projectList,
            attributes : ['projectSubtitle', 'projectName','targetMoney'],
            required : true,
            // where : where2
        },{
            model : models.user,
            attributes : ['name', 'email', 'mobile', 'userIdx', 'guid'],
            required : true,
            where : where2,
            include : [{
                model : models.userBankAccount,
                required : true
            }]
        }]
    });

    res.json({
        list : result.rows,
        count : result.count
    });
}

//회원 투자내역 보기
exports.getProjectStatusList = async (req, res) => {
    console.log(req.body);

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    let pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;
    let where = {
        userIdx: req.body.userIdx,
        delYn: 'N'
    };
    let order = [];
    let where2 = {};

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            if (req.body.orderSelected['item'] === 'name' || req.body.orderSelected['item'] === 'email') {
                order.unshift([models.user, req.body.orderSelected['item'], req.body.orderSelected['type']]);
            } else {
                order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            }
            order.push(['investmentIdx', 'DESC']);
        } else {
            console.log(req.body.orderSelected.type)
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['investmentIdx', 'DESC']
        ];
    }

    //검색어 입력시
    if (req.body.searchText) {
        where2 = {
            [Op.or]: [{
                name: { [Op.like]: '%' + req.body.searchText + '%' }
            },
            {
                email: { [Op.like]: '%' + req.body.searchText + '%' }
            }]
        }
    }

    let result = await models.investmentList.findAndCountAll({
        where,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
        order,
        include: [{
            model: models.projectList,
            attributes: ['projectSubtitle', 'projectName', 'targetMoney', 'platformRate'],
            required: true,
            // where : where2
        }, {
            model: models.user,
            attributes: ['name', 'email', 'mobile', 'guid', 'userIdx'],
            required: true,
            where: where2,
            include: [{
                model: models.userBankAccount,
                where: {
                    virtualBankConfirm: 'Y',
                },
                attributes: ['bankAccount', 'bankName', 'virtualAccount', 'virtualAccountBankName', 'virtualBankConfirm'],
                required: true,
            }]
        }]
    });

    res.json({
        list: result.rows,
        count: result.count
    });

};