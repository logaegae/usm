const models = require.main.require('./models');
const mapCodeToName = require.main.require('./lib/mapCodeToName');
const cipher = require.main.require('./lib/cipher');
const calculate = require.main.require('./lib/calculate');
const seypert = require.main.require('./lib/seypert');
const excel = require('node-excel-export');
const request = require('request');
const Op = require('sequelize').Op;
const api = require.main.require('./config').seypert.domain;
const sendMail = require.main.require('./lib/sendMail');
const bulkMail = require.main.require('./lib/bulkMail');
const sendSMS = require.main.require('./lib/sendSMS');

/* code */
const code = require.main.require('./config/code');

//투자 확정 함수
const confirmPend = async (idx) => {
    let investment = await models.investmentList.findOne({
        where: {
            investmentIdx: idx,
            delYn: 'N'
        },
        include: [{
            model: models.projectList,
            attributes: ['projectName', 'projectSubtitle', 'targetMoney', 'investPeriod', 'taxRate', 'businessTaxRate', 'platformRate', 'startDate', 'earningRate'],
            required: true
        }, {
            model: models.user,
            attributes: ['name', 'email', 'businessTypeConfirmYn', 'mobile', 'loanCompanyConfirm'],
            required: true
        }]
    });
    if (!investment) return {
        error: 'no result'
    };
    if (!investment.dataValues.tid) {
        investment.investedStatus = code.investedStatus[3].value;
        await investment.save();
        return {
            error: 'no tid error'
        };
    }

    let title = investment.dataValues.projectList.dataValues.projectSubtitle + ' ' + investment.dataValues.projectList.dataValues.projectName + ' - ' + investment.dataValues.user.dataValues.name + '(' + investment.dataValues.user.dataValues.email + ')님의 ' + '투자 확정[관리자승인]';
    let result = await seypert.releasePend(investment.dataValues.tid, title);
    let fReuslt;
    let needRepay = false;

    if (result.result && result.result.status === 'SUCCESS') {
        if (result.result.cdKey === 'SFRT_TRNSFR_PND_RELEASED') {

            investment.investedStatus = code.investedStatus[1].value;
            await investment.save();
            fResult = {
                message: '투자완료(확정)상태로 전환되었습니다.'
            };
            needRepay = true;
        }
    } else {
        let result2 = await seypert.transactionDetail(investment.dataValues.tid);
        if (result2.result.status === 'SUCCESS') {
            if (result2.result.list[0].trnsctnSt === 'SFRT_TRNSFR_PND_CHLD_RELEASED' || result2.result.list[0].trnsctnSt === 'SFRT_TRNSFR_PND_RELEASED') {
                investment.investedStatus = code.investedStatus[1].value;
                await investment.save();
                fResult = {
                    error: '이미 확정 완료된 거래입니다.'
                };
                needRepay = true;
            } else if (result2.result.list[0].trnsctnSt === 'SFRT_TRNSFR_CHLD_CANCELED') {
                investment.investedStatus = code.investedStatus[3].value;
                await investment.save();
                fResult = {
                    error: '이미 취소된 거래입니다.'
                };
            } else {
                fResult = {
                    error: '알 수 없는 상태\n세이퍼트 관리자에서 확인하세요\nTID : ' + investment.dataValues.tid
                };
            }
        } else {
            fResult = {
                error: '알 수 없는 상태\n세이퍼트 관리자에서 확인하세요\nTID : ' + investment.dataValues.tid
            };
        }
    }

    let repayment = await models.repaymentList.findAll({
        where: {
            investmentIdx: idx,
            tid: investment.dataValues.tid
        }
    });

    if ((!repayment || repayment.length === 0) && needRepay) {
        //상환기록 작성

        const investPeriod = investment.dataValues.projectList.dataValues.investPeriod;
        const startDate = investment.dataValues.projectList.dataValues.startDate;
        const targetMoney = investment.dataValues.projectList.dataValues.targetMoney;
        const earningRate = investment.dataValues.projectList.dataValues.earningRate;
        let taxRate = investment.dataValues.taxRate;
        if (taxRate === null) taxRate = investment.dataValues.projectList.dataValues.taxRate;
        //대부업은 세금 없음
        if (investment.dataValues.user.dataValues.loanCompanyConfirm === 'Y') taxRate = 0;
        const platformRate = investment.dataValues.projectList.dataValues.platformRate;
        let rResult = await calculate.startInvestRepayment(idx, investment.dataValues.investmentMoney, investPeriod, startDate, targetMoney, earningRate, taxRate, platformRate, investment.dataValues.tid);

        if (rResult.error) fResult = {
            error: '상환 기록 중에 오류가 발생하였습니다.'
        };
    } else {
        if (!fResult.error) fResult = {
            error: '이미 상환 기록이 있는 거래입니다.'
        };
    }

    const setting = await models.config.findById(1);
    //투자 확정 메일 발송
    if (setting.mailOption.option4 != null) {

        const mail = await models.mail.findOne({
            where: {
                mailIdx: setting.mailOption.option4
            }
        });

        if (mail) {
            mail.content = mail.content
                .replace(/\{회원아이디\}/g, investment.dataValues.user.dataValues.email)
                .replace(/\{이름\}/g, investment.dataValues.user.dataValues.name)
                .replace(/\{투자상품명\}/g, investment.dataValues.projectList.dataValues.projectSubtitle + ' ' + investment.dataValues.projectList.dataValues.projectName);

            await sendMail({
                to: investment.dataValues.user.dataValues.email,
                subject: mail.title,
                render: 'mailing_layout',
                data: {
                    title: mail.title,
                    content: mail.content
                }
            });
        }
    }

    //투자 확정 SMS 발송
    if (setting.smsOption.option5 != null && investment.dataValues.user.dataValues.mobile) {

        let sms = await models.sms.findOne({
            where: {
                smsIdx: setting.smsOption.option5
            }
        });
        if (sms) {
            sms.content = sms.dataValues.content
                .replace(/\{회원아이디\}/g, investment.dataValues.user.dataValues.email)
                .replace(/\{이름\}/g, investment.dataValues.user.dataValues.name)
                .replace(/\{투자상품명\}/g, investment.dataValues.projectList.dataValues.projectSubtitle + ' ' + investment.dataValues.projectList.dataValues.projectName);

            await sendSMS(sms.content, investment.dataValues.user.dataValues.mobile);
        }
    }
    return fResult;
}

/**
 * project List
 */
//투자상품 목록보기
exports.getProjectList = async (req, res) => {
    console.log(req.body)
    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    let pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;

    let where = {
        delYn: 'N'
    };
    let order = [];
    let projectStatus = [];

    if (req.body.selected) {
        where.projectStatus = req.body.selected.projectStatus;
    }

    //검색어 입력시
    if (req.body.searchText) {
        where = {
            [Op.or]: [{
                    projectName: {
                        [Op.like]: '%' + req.body.searchText + '%'
                    }
                },
                {
                    projectSubtitle: {
                        [Op.like]: '%' + req.body.searchText + '%'
                    }
                }
            ],
            delYn: 'N'
        }
    }

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['projectIdx', 'DESC']);
        } else {
            console.log(req.body.orderSelected.type)
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['projectIdx', 'DESC']
        ];
    }


    let result = await models.projectList.findAll({
        where,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
        order
    });

    let count = await models.projectList.count({
        where: {
            delYn: 'N'
        }
    });

    for (i = 0; i < code.projectStatus.length; i++) {
        projectStatus[i] = {};
        projectStatus[i].value = code.projectStatus[i].value;
        projectStatus[i].name = code.projectStatus[i].name;
        projectStatus[i].count = await models.projectList.count({
            where: {
                delYn: 'N',
                projectStatus: code.projectStatus[i].value
            }
        });
    }

    res.json({
        list: result,
        count,
        projectStatus,
        selected: req.body.selected
    });
};

//프로젝트 상세 보기
exports.getProjectView = async (req, res) => {
    console.log(req.body);
    const setting = await models.config.findOne({
        where: {
            configIdx: 1
        }
    });

    let result = await models.projectList.findOne({
        where: {
            projectIdx: req.body.id
        }
    });
    result.dataValues.oriSticker = setting.sticker;
    res.json(result);

}

//스티커 목록 보기
exports.getSticker = async (req, res) => {
    console.log(req.body);

    const setting = await models.config.findOne({
        where: {
            configIdx: 1
        }
    });
    let result = {};
    result.sticker = setting.sticker;
    res.json(result);
}

//프로젝트 목록에서 삭제
exports.deleteProjectView = async (req, res) => {
    console.log(req.body)
    await models.projectList.update({
        delYn: 'Y'
    }, {
        where: {
            projectIdx: req.body.projectIdx
        }
    });

    res.json({
        message: '삭제되었습니다.'
    });
}
//프로젝트 등록 및 수정
exports.setProjectView = async (req, res) => {
    console.log(req.body);
    let item = req.body;
    let idx = req.body.idx;
    let projectIdx = item.projectIdx;
    delete item.idx;

    if (item.minInvest) item.minInvest = parseInt(item.minInvest.toString().replace(/\,/g, '').replace(/\D/g, ''));
    if (item.targetMoney) item.targetMoney = parseInt(item.targetMoney.toString().replace(/\,/g, '').replace(/\D/g, ''));
    if (item.videoUrl) item.videoUrl = item.videoUrl.replace('https://www.youtube.com/watch?v=', '').replace('https://youtu.be/', '');

    //신규작성
    if (idx === 'new') {

        const setting = await models.config.findOne({
            where: {
                configIdx: 1
            }
        });

        item.taxRate = setting.taxRate;
        item.businessTaxRate = setting.businessTaxRate;
        item.platformRate = setting.platformRate;
        item.updatedDate = new Date();

        item.registedDate = new Date();
        item.sticker[0] = setting.sticker[0];
        await models.projectList.create(item).then((result) => {
            projectIdx = result.projectIdx;
        });


        //신규투자 메일 발송
        if (setting.mailOption.option1 != null) {

            const mail = await models.mail.findOne({
                where: {
                    mailIdx: setting.mailOption.option1
                }
            });

            const user = await models.user.findAll({
                where: {
                    agreeEmail: 'Y',
                    memberStatus: code.memberStatus[0].value,
                    dormantYn: 'N'
                },
                attributes: ['email', 'name']
            });

            if (user && user.length > 0 && mail) {

                let to = '';
                let name = '';
                for (var i = 0; i < user.length; i++) {
                    to += user[i].dataValues.name + '/' + user[i].dataValues.email;
                    if (i !== user.length - 1) to += ',';
                }

                const bulkMailer = new bulkMail(setting);
                const mailOptions = {
                    from: '엔케이펀딩 < ' + setting.email + ' >',
                    to,
                    subject: mail.title,
                    render: 'mailing_layout',
                    data: {
                        title: mail.title,
                        content: mail.content
                    }
                }

                bulkMailer.send(mailOptions, true, function (error, result) { // arg1: mailinfo, agr2: parallel mails, arg3: callback
                    if (error) {
                        console.error(error);
                    } else {
                        console.info(result);
                    }
                });
            }
        }

        //신규투자 SMS 발송
        if (setting.smsOption.option1 != null) {

            const sms = await models.sms.findOne({
                where: {
                    smsIdx: setting.smsOption.option1
                }
            });

            const user = await models.user.findAll({
                where: {
                    agreeSms: 'Y',
                    memberStatus: code.memberStatus[0].value,
                    dormantYn: 'N'
                },
                attributes: ['email', 'name', 'mobile']
            });

            if (user && user.length > 0 && sms) {

                for (var i = 0; i < user.length; i++) {
                    if (user[i].dataValues.mobile) {
                        let tel = user[i].dataValues.mobile
                        let text = sms.content.replace(/\{회원아이디\}/g, user[i].dataValues.email).replace(/\{이름\}/g, user[i].dataValues.name);
                        sendSMS(text, tel);
                    }
                }
            }
        }

    } else {
        console.log(item.recruitStartTime)
        await models.projectList.update(item, {
            where: {
                projectIdx: item.projectIdx
            }
        });
    }
    console.log(projectIdx)
    let cResult = await calculate.projectExpectedIncome(projectIdx);
    // console.log(cResult);

    res.json({
        message: '저장되었습니다.'
    });
}

/**
 * project status
 */
//프로젝트 투자자 목록 엑셀 다운
exports.getInvestorListExcel = async (req, res) => {
    console.log(req.body);

    let projectIdx = req.body.projectIdx;
    let investorJson = [];
    let count = 0;

    let result = await models.investmentList.findAll({
        where: {
            projectIdx,
            delYn: 'N',
            investedStatus: {
                [Op.ne]: null
            }
        },
        include: [{
            model: models.projectList,
            required: true,
        }, {
            model: models.user,
            required: true,
            include: [{
                model: models.userBankAccount,
                where: {
                    virtualBankConfirm: 'Y'
                },
                required: true,
            }]
        }]
    });
    if (result.length === 0) res.status(403).json({
        message: '검색결과가 없습니다'
    });
    let projectTitle = result[0].dataValues.projectList.dataValues.projectSubtitle + ' ' + result[0].dataValues.projectList.dataValues.projectName;

    for (let one of result) {
        let obj = {};
        let item = one.dataValues;

        count++;
        obj['No'] = count;
        obj['아이디'] = item.user.dataValues.email || '-';
        obj['이름'] = item.user.dataValues.name || '-';
        obj['회원분류'] = mapCodeToName(code.memberStatus, item.user.dataValues.memberStatus) || '-';
        obj['투자등급'] = mapCodeToName(code.investorType, item.user.dataValues.investorType) || '-';
        obj['주민번호'] = item.user.dataValues.socialNum ? item.user.dataValues.socialNum.split('-')[0] + '-' + cipher.decode(item.user.dataValues.socialNum.split('-')[1]) : '-';
        obj['연락처'] = item.user.dataValues.mobile || '-';
        obj['사업자분류'] = mapCodeToName(code.businessType, item.user.dataValues.businessType) || '-';
        obj['사업자승인'] = item.user.dataValues.businessTypeConfirmYn == 'Y' ? '예' : item.user.dataValues.businessTypeConfirmYn == 'N' ? '아니오' : '-';
        obj['대부업승인'] = item.user.dataValues.loanCompanyConfirm == 'Y' ? '예' : item.user.dataValues.loanCompanyConfirm == 'N' ? '아니오' : '-';
        obj['회사명'] = item.user.dataValues.companyName || '-';
        obj['법인번호'] = item.user.dataValues.corporateNo || '-';
        obj['사업자번호'] = item.user.dataValues.businessNo || '-';
        obj['대표자이름'] = item.user.dataValues.presidentName || '-';
        obj['투자금액(원)'] = (item['investmentMoney'] || '-') + '원';
        obj['투자비율'] = (item['investmentRate'] || '-') + '%';
        obj['투자상태'] = mapCodeToName(code.investedStatus, item['investedStatus']) || '-';
        obj['사업자수익률'] = (item.projectList.dataValues.platformRate || '-') + '%';
        obj['사업자수익금'] = (item['platformIncome'] || '-') + '원';
        obj['이자수익'] = (item['interestIncome'] || '-') + '원';
        obj['이자세금'] = (item['interestTax'] || '-') + '원';
        obj['실이자수익'] = (item['afterTaxIncome'] || '-') + '원';
        if (item.user.dataValues.userBankAccount && item.user.dataValues.userBankAccount.dataValues) {
            obj['은행'] = item.user.dataValues.userBankAccount.dataValues.bankAccount || '-';
            obj['계좌번호'] = item.user.dataValues.userBankAccount.dataValues.bankAccount || '-';
            obj['계좌주'] = item.user.dataValues.userBankAccount.dataValues.accountOwnerName || '-';
            obj['예치금'] = item.user.dataValues.userBankAccount.dataValues.virtualAccountBalance + '원' || '-';
        } else {
            obj['은행'] = '-';
            obj['계좌번호'] = '-';
            obj['계좌주'] = '-';
            obj['예치금'] = '-';
        }
        obj['가상계좌 은행명'] = item.user.dataValues.userBankAccount.dataValues.virtualAccountBankName || '-';
        obj['가상계좌번호'] = item.user.dataValues.userBankAccount.dataValues.virtualAccount || '-';
        obj['GUID'] = item.user.dataValues.guid || '-';
        investorJson.push(obj);
    }

    const styles = {
        center: {
            alignment: {
                horizontal: 'center'
            }
        }
    };

    const heading = [
        [{
            value: '투자상품 - ' + projectTitle,
            style: styles.center
        }]
    ];

    const merges = [{
        start: {
            row: 1,
            column: 1
        },
        end: {
            row: 1,
            column: 16
        }
    }];

    const specification = (arg) => {
        let obj = {};
        for (let key in arg) {
            obj[key] = {
                displayName: key, // <- Here you specify the column header
                headerStyle: styles.center, // <- Header style
                width: 80
            }
        }
        return obj;
    };

    const report = excel.buildExport(
        [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
            {
                name: '투자자 목록', // <- Specify sheet name (optional)
                heading: heading, // <- Raw heading array (optional)
                merges: merges, // <- Merge cell ranges
                specification: specification(investorJson[0]),
                data: investorJson // <-- Report data
            }
        ]
    );

    return res.send(report);

}
// 투자자목록에서 투자상품 상세보기
exports.getProjectInfo = async (req, res) => {

    console.log(req.body);

    await calculate.TI(req.body.idx);

    let project = await models.projectList.findOne({
        where: {
            projectIdx: req.body.idx
        },
        attributes: ['projectSubtitle', 'projectName', 'targetMoney', 'totalInvest', 'totalInvestCount', 'recruitStartDate', 'recruitEndDate', 'startDate', 'endDate', 'projectStatus']
    });

    if (!project) return res.json({
        error: 'no result'
    });

    return res.json(project);
}

//세이퍼트 예약이체 취소 실행
exports.requestCancellation = async (req, res) => {
    console.log(req.body);

    let investment = await models.investmentList.findOne({
        where: {
            investmentIdx: req.body.investmentIdx,
            delYn: 'N'
        },
        include: [{
            model: models.projectList,
            required: true
        }]
    });
    if (!investment) return res.json({
        error: 'no result'
    });
    if (!investment.dataValues.tid) return res.json({
        error: 'no tid'
    });

    let result = await seypert.cancelPend(investment.dataValues.tid, investment.dataValues.projectList.dataValues.projectSubtitle + ' ' + investment.dataValues.projectList.dataValues.projectName + '의 투자 취소 [관리자 승인]');
    await calculate.TI(investment.dataValues.projectIdx);
    console.log(result)
    if (result.result && result.result.status === 'SUCCESS') {
        if (result.result.cdKey === 'SFRT_TRNSFR_PND_CANCELED') {
            investment.investedStatus = code.investedStatus[3].value;
            await investment.save();
            return res.json({
                message: '투자 취소를 실행하였습니다.'
            });
        } else {
            investment.investedStatus = code.investedStatus[2].value;
            return res.json({
                error: '취소 신청을 완료하지 못했습니다.\n취소대기 상태로 전환합니다.'
            });
        }

    } else {
        if (result.error.cdKey === 'STATUS_HAS_CANCELED') {
            investment.investedStatus = code.investedStatus[3].value;
            await investment.save();
        }
        return res.json({
            error: result.error.cdDesc
        });
    }
}
//투자 확인
exports.checkInvestment = async (req, res) => {
    console.log(req.body);

    //검증
    let investment = await models.investmentList.findOne({
        where: {
            investedStatus: code.investedStatus[4].value,
            delYn: 'N'
        },
        include: [{
            model: models.user,
            required: true,
            attributes: ['name', 'email', 'userIdx']
        }, {
            model: models.projectList,
            required: true,
            attributes: ['projectSubtitle', 'projectName', 'projectIdx']
        }]
    });

    let seyResult = await seypert.transactionDetail(investment.dataValues.tid);
    console.log(seyResult)
    if (seyResult.result.status === 'SUCCESS') {
        //입금 승인
        if (seyResult.result.list[0].trnsctnSt === 'SFRT_TRNSFR_PND_AGRREED') {

            let user = await models.user.findOne({
                where: {
                    userIdx: investment.dataValues.userIdx
                }
            })

            let investJson = {
                mobile: user.dataValues.mobile,
                investedDate: new Date(),
                investmentMoney: seyResult.result.list[0].payAmt,
                investedStatus: code.investedStatus[0].value
            }

            //신청기록 업데이트
            await models.investmentList.update(investJson, {
                where: {
                    investmentIdx: investment.dataValues.investmentIdx
                }
            });

            //투자가능금액조정 
            await calculate.IableA(investment.dataValues.userIdx);
            //프로젝트 기록 계산
            await calculate.TI(investment.dataValues.projectIdx);
            //투자수익 계산
            await calculate.expectedIncome(investment.dataValues.investmentIdx, seyResult.result.list[0].payAmt);

            //알람
            await models.alarm.create({
                title: '투자신청',
                content: investment.dataValues.email + '님이 투자신청을 하였습니다.',
                registedDate: new Date(),
                type: 'info'
            });

            //투자 신청 확인 메일 발송
            const setting = await models.config.findById(1);
            if (setting.mailOption.option3 != null) {

                const mail = await models.mail.findOne({
                    where: {
                        mailIdx: setting.mailOption.option3,
                        delYn: 'N'
                    }
                });

                if (mail) {
                    mail.content = mail.content
                        .replace(/\{회원아이디\}/g, investment.dataValues.user.dataValues.email)
                        .replace(/\{이름\}/g, investment.dataValues.user.dataValues.name)
                        .replace(/\{투자상품명\}/g, einvestment.dataValues.projectList.dataValues.projectSubtitle + ' ' + investment.dataValues.projectList.dataValues.projectName);

                    sendMail({
                        to: investment.dataValues.user.dataValues.email,
                        subject: mail.title,
                        render: 'mailing_layout',
                        data: {
                            title: mail.title,
                            content: mail.content
                        }
                    });
                }
            }

            //투자 신청 확정 SMS 발송
            if (setting.smsOption.option4 != null && investment.dataValues.user.dataValues.mobile) {

                let sms = await models.sms.findOne({
                    where: {
                        smsIdx: setting.smsOption.option4
                    }
                });
                if (sms) {
                    sms.content = sms.dataValues.content
                        .replace(/\{회원아이디\}/g, investment.dataValues.user.dataValues.email)
                        .replace(/\{이름\}/g, investment.dataValues.user.dataValues.name)
                        .replace(/\{투자상품명\}/g, investment.dataValues.projectList.dataValues.projectSubtitle + ' ' + investment.dataValues.projectList.dataValues.projectName);

                    await sendSMS(sms.content, investment.dataValues.user.dataValues.mobile);
                }
            }
            console.log("검증 완료");
            return res.json({
                message: '입금이 완료되어 투자신청 처리되었습니다.'
            });
        } else if (seyResult.result.list[0].trnsctnSt === 'REQUEST_HAS_TIME_OUT') {
            return res.json({
                error: '투자 신청자의 인증문자 유효시간이 초과되어 신청이 취소되었습니다.'
            });
        } else if (seyResult.result.list[0].trnsctnSt === 'SFRT_TRNSFR_PND_TRYING') {
            return res.json({
                error: '투자 신청자가 인증문자를 회신하지 않았습니다.'
            });
        }
    } else {
        return res.json({
            error: '상태코드 : ' + seyResult.result.list[0].trnsctnSt
        });
    }
}

//투자 철회
exports.investmentCancellation = async (req, res) => {
    console.log(req.body);

    let repayment = await models.repaymentList.findAll({
        where: {
            investmentIdx: req.body.investmentIdx,
            delYn: 'N'
        },
        attributes: ['repaymentIdx', 'investmentIdx', 'delYn']
    });

    models.sequelize.transaction(async (t) => {

        let promises = [];

        for (var i = 0; i < repayment.length; i++) {

            let newPromise;

            newPromise = models.repaymentList.update({
                delYn: 'Y'
            }, {
                where: {
                    investmentIdx: req.body.investmentIdx
                },
                transaction: t
            })

            promises.push(newPromise);
        }

        return Promise.all(promises);

    }).then(async (result) => {

        let investment = await models.investmentList.findOne({
            where: {
                investmentIdx: req.body.investmentIdx,
                delYn: 'N'
            },
            attributes: ['investmentIdx', 'delYn']
        });
        if (!investment) return res.json({
            error: 'no result'
        });

        investment.delYn = 'Y';
        await investment.save();
        return res.json({
            message: '투자기록을 변경하였습니다.'
        });

    }).catch(function (err) {

        return {
            error: 'transaction Error'
        };

    });
}

//취소 확인
exports.requestCancellationConfirm = async (req, res) => {
    console.log(req.body);

    let investment = await models.investmentList.findOne({
        where: {
            investmentIdx: req.body.investmentIdx,
            delYn: 'N'
        }
    });
    if (!investment) return res.json({
        error: 'no result'
    });
    if (!investment.dataValues.tid) return res.json({
        error: 'no tid'
    });

    let projectIdx = investment.dataValues.projectIdx;
    if (!projectIdx) return res.json({
        error: 'no projcet'
    });

    let project = await models.projectList.findOne({
        where: {
            projectIdx
        },
        attributes: ['projectSubtitle', 'projectName']
    })

    let result = seypert.transactionDetail(investment.dataValues.tid);
    await calculate.TI(investment.dataValues.projectIdx);

    if (result.status === 'SUCCESS') {
        if (result.list[0].trnsctnSt === 'SFRT_TRNSFR_CHLD_CANCELED') {

            investment.investedStatus = code.investedStatus[3].value;
            await investment.save();
            return res.json({
                message: '취소 승인이 완료되었습니다.'
            });
        } else {
            return res.json({
                warning: '취소 승인이 되지 않았습니다.'
            });
        }
    } else {
        return res.json({
            error: 'request error'
        });
    }

}

//투자 취소 철회
exports.calcelProjectCalcellation = async (req, res) => {
    console.log(req.body);

    let investment = await models.investmentList.findOne({
        where: {
            investmentIdx: req.body.investmentIdx,
            delYn: 'N'
        }
    });
    if (!investment) return res.json({
        error: 'no result'
    });
    if (!investment.dataValues.tid) {
        investment.investedStatus = code.investedStatus[3].value;
        await investment.save();
        return res.json({
            error: 'no tid error'
        });
    }


    let result = await seypert.transactionDetail(investment.dataValues.tid);

    if (result.result.status === 'SUCCESS') {
        if (result.result.list[0].trnsctnSt !== 'SFRT_TRNSFR_CHLD_CANCELED') {

            investment.investedStatus = code.investedStatus[0].value;
            await investment.save();
            return res.json({
                message: '투자 대기상태로 전환되었습니다.'
            });

        } else {
            investment.investedStatus = code.investedStatus[3].value;
            await investment.save();
            return res.json({
                error: '이미 취소 완료된 거래입니다.'
            });
        }
    }
}

//투자 확정
exports.confirmPend = async (req, res) => {
    console.log(req.body);

    const fResult = await confirmPend(req.body.investmentIdx);

    return res.json(fResult);
}

//일괄 투자 확정
exports.confirmPends = async (req, res) => {
    console.log(req.body);

    let fResult;

    let investment = await models.investmentList.findAll({
        where: {
            projectIdx: req.body.projectIdx,
            investedStatus: code.investedStatus[0].value,
            delYn: 'N'
        },
        attributes: ['investmentIdx']
    });

    if (investment.length === 0) fResult = {
        error: "확정할 투자신청이 없습니다."
    }

    let promises = [];

    for (i = 0; i < investment.length; i++) {

        promises.push(confirmPend(investment[i].dataValues.investmentIdx));
    }

    let results = Promise.all(promises);

    results.then(data => {
        console.log(data)
        let yn = data.some((e) => {
            return !!e.error
        });
        yn ? fResult = {
            error: "일괄 확정 중 에러가 있습니다. \n확인해주세요."
        } : fResult = {
            message: "일괄 확정 성공"
        };

        return res.json(fResult);

    });
}

//투자 취소
exports.calcelWait = async (req, res) => {
    console.log(req.body);

    let investment = await models.investmentList.findOne({
        where: {
            investmentIdx: req.body.investmentIdx,
            delYn: 'N'
        }
    });
    if (!investment) return res.json({
        error: 'no result'
    });

    investment.investedStatus = code.investedStatus[2].value;
    await investment.save();

    return res.json({
        message: '취소대기 상태로 전환되었습니다.'
    });
}


exports.setProjectStatusList = async (req, res) => {
    console.log(req.body);

    let list = [];
    req.body.forEach((e, i) => {
        let obj = {}
        obj.investmentIdx = e.investmentIdx;
        obj.investedStatus = e.investedStatus;
        list.push(obj);
    });

    models.sequelize.transaction(async (t) => {

        let promises = [];

        for (var i = 0; i < list.length; i++) {

            let obj2 = {}

            if (list[i].investedStatus) obj2.investedStatus = list[i].investedStatus;

            newPromise = models.investmentList.update(obj2, {
                where: {
                    investmentIdx: list[i].investmentIdx,
                    delYn: 'N'
                },
                transaction: t
            });
        }

        return Promise.all(promises);

    }).then(function (result) {

        res.json({
            error: 0,
            message: '저장되었습니다'
        });

    }).catch(function (err) {

        res.json({
            error: 1,
            message: '에러'
        });

    });
}
//투자상환상세 수정
exports.setProjectRepaymentView = async (req, res) => {
    console.log(req.body);

    let list = req.body.list;

    models.sequelize.transaction(async (t) => {

        let promises = [];

        for (var i = 0; i < list.length; i++) {

            newPromise = models.repaymentList.update(list[i], {
                where: {
                    repaymentIdx: list[i].repaymentIdx
                },
                transaction: t
            });
        }

        return Promise.all(promises);

    }).then(function (result) {

        res.json({
            error: 0,
            message: '저장되었습니다'
        });

    }).catch(function (err) {

        res.json({
            error: 1,
            message: '에러'
        });

    });
}

//투자상환목록 엑셀 다운
exports.getInvestorListExcel2 = async (req, res) => {
    console.log(req.body);

    let selected = {};
    let where = {
        delYn: 'N',
        [Op.not]: {
            investedStatus: code.investedStatus[0].value
        }
    };
    let order = [];
    let where2 = {};
    let where3 = {};

    if (req.body.selected && Object.keys(req.body.selected).length > 0) {
        if (req.body.selected.projectIdx) where.projectIdx = req.body.selected.projectIdx;
        if (req.body.selected.investmentIdx) where.investmentIdx = req.body.selected.investmentIdx
        if (req.body.selected.repaymentCount) where3.repaymentCount = req.body.selected.repaymentCount;
        if (req.body.selected.repaymentStatus) where3.repaymentStatus = req.body.selected.repaymentStatus;
        if (req.body.selected.startDate && req.body.selected.endDate) {
            where3.expectedRepaymentDate = {
                [Op.and]: {
                    [Op.gte]: req.body.selected.startDate,
                    [Op.lte]: req.body.selected.endDate
                }
            }
        }
        selected = req.body.selected;
    }

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            if (req.body.orderSelected === 'projectIdx')
                order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['investmentIdx', 'DESC']);
        } else {
            console.log(req.body.orderSelected.type)
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['investmentIdx', 'DESC']
        ];
    }

    //검색어 입력시
    if (req.body.selected && req.body.selected.searchText) {
        where2 = {
            [Op.or]: [{
                    name: {
                        [Op.like]: '%' + req.body.selected.searchText + '%'
                    }
                },
                {
                    email: {
                        [Op.like]: '%' + req.body.selected.searchText + '%'
                    }
                }
            ]
        }
    }

    let result = await models.repaymentList.findAll({
        where: where3,
        order,
        include: [{
            model: models.investmentList,
            required: true,
            where: where,
            include: [{
                model: models.projectList,
                attributes: ['projectSubtitle', 'projectName', 'projectIdx'],
                required: true,
                // where : where2
            }, {
                model: models.user,
                required: true,
                where: where2
            }]
        }]
    });

    if (result.length === 0) res.status(403).json({
        message: '검색결과가 없습니다'
    });
    let projectTitle = result[0].dataValues.investmentList.dataValues.projectList.dataValues.projectSubtitle + ' ' + result[0].dataValues.investmentList.dataValues.projectList.dataValues.projectName;
    let count = 0;
    let investorJson = [];

    for (let one of result) {
        let obj = {};
        let item = one.dataValues;
        let user = item.investmentList.dataValues.user
        count++;
        obj['No'] = count;
        obj['투자자'] = item.investmentList.dataValues.user.dataValues.name || '-';
        obj['이메일'] = item.investmentList.dataValues.user.dataValues.email || '-';
        obj['회원분류'] = mapCodeToName(code.memberStatus, user.dataValues.memberStatus) || '-';
        obj['투자등급'] = mapCodeToName(code.investorType, user.dataValues.investorType) || '-';
        obj['주민번호'] = user.dataValues.socialNum ? user.dataValues.socialNum.split('-')[0] + '-' + cipher.decode(user.dataValues.socialNum.split('-')[1]) : '-';
        obj['연락처'] = user.dataValues.mobile || '-';
        obj['사업자분류'] = mapCodeToName(code.businessType, user.dataValues.businessType) || '-';
        obj['사업자승인'] = user.dataValues.businessTypeConfirmYn == 'Y' ? '예' : user.dataValues.businessTypeConfirmYn == 'N' ? '아니오' : '-';
        obj['대부업승인'] = user.dataValues.loanCompanyConfirm == 'Y' ? '예' : user.dataValues.loanCompanyConfirm == 'N' ? '아니오' : '-';
        obj['회사명'] = user.dataValues.companyName || '-';
        obj['법인번호'] = user.dataValues.corporateNo || '-';
        obj['사업자번호'] = user.dataValues.businessNo || '-';
        obj['대표자이름'] = user.dataValues.presidentName || '-';
        obj['회차'] = item.repaymentCount || '-';
        obj['상환예정일자'] = item.expectedRepaymentDate || '-';
        obj['상환일자'] = item.repaymentDate || '-';
        obj['이자'] = (item['interestIncome'] || '-') + '원';
        obj['플랫폼수수료'] = (item['platformIncome'] || '-') + '원';
        obj['세금'] = (item['interestTax'] || '-') + '원';
        obj['세후이자'] = (item['afterTaxIncome'] || '-') + '원';
        obj['원금'] = (item['investedMoney'] || '-') + '원';
        obj['상태'] = mapCodeToName(code.repaymentStatus, item['repaymentStatus']) || '-';
        investorJson.push(obj);
    }

    const styles = {
        center: {
            alignment: {
                horizontal: 'center'
            }
        }
    };

    const heading = [
        [{
            value: '투자상품 (' + projectTitle + ')',
            style: styles.center
        }]
    ];

    const merges = [{
        start: {
            row: 1,
            column: 1
        },
        end: {
            row: 1,
            column: 16
        }
    }];

    const specification = (arg) => {
        let obj = {};
        for (let key in arg) {
            obj[key] = {
                displayName: key, // <- Here you specify the column header
                headerStyle: styles.center, // <- Header style
                width: 80
            }
        }
        return obj;
    };

    const report = excel.buildExport(
        [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
            {
                name: '투자자 목록', // <- Specify sheet name (optional)
                heading: heading, // <- Raw heading array (optional)
                merges: merges, // <- Merge cell ranges
                specification: specification(investorJson[0]),
                data: investorJson // <-- Report data
            }
        ]
    );

    return res.send(report);

}
//가상계좌 인증 회원 목록보기
exports.getUserList = async (req, res) => {

    let result = await models.user.findAll({
        where: {
            memberStatus: code.memberStatus[0].value
        },
        attributes: ['email', 'name', 'companyName', 'businessType', 'userIdx'],
        order: [
            ['email', 'ASC']
        ],
        include: [{
            model: models.userBankAccount,
            required: true,
            where: {
                virtualBankConfirm: 'Y'
            }
        }]
    })

    result.forEach((e, i) => {
        let name = e.name;
        if (e.businessType !== code.businessType[0].value) name = e.companyName;

        e.name = name + '(' + e.email + ')';
    });

    res.json(result);
}

//투자 상환 목록 보기
exports.getProjectRepaymentList = async (req, res) => {
    console.log(req.body)

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    let pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;

    let where = {
        delYn: 'N',
        [Op.not]: {
            [Op.or]: [{
                    projectStatus: code.projectStatus[0].value
                },
                {
                    projectStatus: code.projectStatus[1].value
                },
                {
                    projectStatus: code.projectStatus[2].value
                }
            ]
        }
    };
    let order = [];

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['projectIdx', 'DESC']);
        } else {
            console.log(req.body.orderSelected.type)
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['projectIdx', 'DESC']
        ];
    }

    //검색어 입력시
    if (req.body.searchText) {
        where = {
            [Op.or]: [{
                    projectSubtitle: {
                        [Op.like]: '%' + req.body.searchText + '%'
                    }
                },
                {
                    projectName: {
                        [Op.like]: '%' + req.body.searchText + '%'
                    }
                }
            ]
        }
    }

    let result = await models.projectList.findAndCountAll({
        where,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
        order,
        attributes: ['projectIdx', 'projectSubtitle', 'projectName', 'totalInvest', 'interestIncome', 'projectStatus', 'totalInvestCount', 'repaymentWay']
    });

    res.json({
        list: result.rows,
        count: result.count
    });
}


//투자상환 상세내용 수정
exports.setProjectRepaymentList = async (req, res) => {
    console.log(req.body)

    let list = req.body;

    models.sequelize.transaction(async (t) => {

        let promises = [];

        for (var i = 0; i < list.length; i++) {

            let obj = {}

            if (list[i].interestIncome) obj.interestIncome = list[i].interestIncome;
            if (list[i].projectStatus) obj.projectStatus = list[i].projectStatus;

            newPromise = models.projectList.update(obj, {
                where: {
                    projectIdx: list[i].projectIdx
                },
                transaction: t
            });

            if (Object.getOwnPropertyNames(obj).length != 0) promises.push(newPromise);
        }

        return Promise.all(promises);

    }).then(function (result) {

        res.json({
            error: 0,
            message: '저장되었습니다'
        });

    }).catch(function (err) {

        res.json({
            error: 1,
            message: '에러'
        });

    });
}
//투자 상환 상세보기
exports.getProjectRepaymentView = async (req, res) => {
    console.log(req.body)
    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    let pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;
    let isSelected = false;
    let selected = {};
    let where = {
        delYn: 'N',
        [Op.not]: {
            investedStatus: code.investedStatus[0].value
        }
    };
    let order = [];
    let where2 = {};
    let where3 = {};

    if (req.body.selected && Object.keys(req.body.selected).length > 0) {
        if (req.body.selected.projectIdx) where.projectIdx = req.body.selected.projectIdx;
        if (req.body.selected.investmentIdx) where.investmentIdx = req.body.selected.investmentIdx
        if (req.body.selected.repaymentCount) where3.repaymentCount = req.body.selected.repaymentCount;
        if (req.body.selected.repaymentStatus) where3.repaymentStatus = req.body.selected.repaymentStatus;
        if (req.body.selected.startDate && req.body.selected.endDate) {
            where3.expectedRepaymentDate = {
                [Op.and]: {
                    [Op.gte]: req.body.selected.startDate,
                    [Op.lte]: req.body.selected.endDate
                }
            }
        }
        isSelected = true;
        selected = req.body.selected;
    }

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            if (req.body.orderSelected === 'projectIdx')
                order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['expectedRepaymentDate', 'DESC']);
            order.push(['repaymentIdx', 'DESC']);
        } else {
            console.log(req.body.orderSelected.type)
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['expectedRepaymentDate', 'DESC'],
            ['repaymentIdx', 'DESC']
        ];
    }

    //검색어 입력시
    if (req.body.selected && req.body.selected.searchText) {
        where2 = {
            [Op.or]: [{
                    name: {
                        [Op.like]: '%' + req.body.selected.searchText + '%'
                    }
                },
                {
                    email: {
                        [Op.like]: '%' + req.body.selected.searchText + '%'
                    }
                }
            ]
        }
    }

    let result = await models.repaymentList.findAndCountAll({
        where: where3,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
        order,
        include: [{
            model: models.investmentList,
            required: true,
            where: where,
            include: [{
                model: models.projectList,
                attributes: ['projectSubtitle', 'projectName', 'projectIdx'],
                required: true,
                // where : where2
            }, {
                model: models.user,
                attributes: ['name', 'email', 'mobile', 'guid', 'userIdx', 'companyName', 'totalInvestedMoney'],
                required: true,
                where: where2,
                include: [{
                    model: models.userBankAccount,
                    required: true
                }]
            }]
        }]
    });

    let json = {
        list: result.rows,
        count: result.count
    };
    if (isSelected) json.selected = selected;
    res.json(json);
};

//투자취소현황 보기
exports.getProjectCancle = async (req, res) => {
    console.log(req.body);

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    var pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;

    let key = req.body.selected ? Object.keys(req.body.selected)[0] : null;

    let result = {};
    let order = [];
    let where = {};
    if (key === 'investedStatus') {
        where = {
            investedStatus: req.body.selected[key],
            delYn: 'N'
        }
    } else {
        where = {
            [Op.or]: [{
                investedStatus: code.investedStatus[2].value
            }, {
                investedStatus: code.investedStatus[3].value
            }],
            delYn: 'N'
        };
    }
    let where2 = {};
    let countWait = 0,
        countComplete = 0;

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            if (req.body.orderSelected['item'] === 'name' || req.body.orderSelected['item'] === 'email') {
                order.unshift([models.user, req.body.orderSelected['item'], req.body.orderSelected['type']]);
            } else {
                order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            }
            order.push(['investmentIdx', 'DESC']);
        } else {
            console.log(req.body.orderSelected.type)
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['investmentIdx', 'DESC']
        ];
    }
    //검색어 입력시
    if (req.body.searchText) {
        where2.projectName = {
            [Op.like]: '%' + req.body.searchText + '%'
        };
    }


    let list = await models.investmentList
        .findAndCountAll({
            where,
            offset: (currentPage - 1) * pageSize,
            limit: pageSize,
            order,
            include: [{
                model: models.projectList,
                attributes: ['projectSubtitle', 'projectName'],
                required: true,
                where: where2
            }, {
                model: models.user,
                attributes: ['name', 'email', 'mobile', 'userIdx', 'guid', 'totalInvestedMoney', 'companyName'],
                required: true,
                include: [{
                    model: models.userBankAccount,
                    required: true
                }]
            }]
        });

    countWait = await models.investmentList.count({
        where: {
            investedStatus: code.investedStatus[2].value,
            delYn: 'N'
        }
    });

    countComplete = await models.investmentList.count({
        where: {
            investedStatus: code.investedStatus[3].value,
            delYn: 'N'
        }
    });

    result = {
        list: list.rows,
        count: list.count,
        investedStatus: [countWait, countComplete]
    };

    key ? result.selected = req.body.selected : '';

    res.json(result);

};
//투자상품 투자자 목록 보기
exports.getProjectStatusList = async (req, res) => {
    console.log(req.body);

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    let pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;
    let where;
    if (req.body.id) {
        where = {
            projectIdx: req.body.id,
            delYn: 'N',
            [Op.or]: [{
                investedStatus: code.investedStatus[0].value
            }, {
                investedStatus: code.investedStatus[1].value
            }, {
                investedStatus: code.investedStatus[4].value
            }]
        };
    } else {
        where = {
            delYn: 'N',
            [Op.or]: [{
                investedStatus: code.investedStatus[0].value
            }, {
                investedStatus: code.investedStatus[1].value
            }, {
                investedStatus: code.investedStatus[4].value
            }]
        };
    }
    let order = [];
    let where2 = {};

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            if (req.body.orderSelected['item'] === 'name' || req.body.orderSelected['item'] === 'email') {
                order.unshift([models.user, req.body.orderSelected['item'], req.body.orderSelected['type']]);
            } else {
                order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            }
            order.push(['investmentIdx', 'DESC']);
        } else {
            console.log(req.body.orderSelected.type)
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['investmentIdx', 'DESC']
        ];
    }

    //검색어 입력시
    if (req.body.searchText) {
        where2 = {
            [Op.or]: [{
                    name: {
                        [Op.like]: '%' + req.body.searchText + '%'
                    }
                },
                {
                    email: {
                        [Op.like]: '%' + req.body.searchText + '%'
                    }
                }
            ]
        }
    }

    let result = await models.investmentList.findAndCountAll({
        where,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
        order,
        include: [{
            model: models.projectList,
            attributes: ['projectSubtitle', 'projectName', 'targetMoney', 'platformRate'],
            required: true,
            // where : where2
        }, {
            model: models.user,
            attributes: ['name', 'email', 'mobile', 'guid', 'userIdx', 'companyName', 'totalInvestedMoney'],
            required: true,
            where: where2,
            include: [{
                model: models.userBankAccount,
                where: {
                    virtualBankConfirm: 'Y',
                },
                attributes: ['bankAccount', 'bankName', 'virtualAccount', 'virtualAccountBankName', 'virtualBankConfirm', 'virtualAccountBalance'],
                required: true,
            }]
        }]
    });

    res.json({
        list: result.rows,
        count: result.count
    });

};

exports.getProjectNames = async (req, res) => {

    let where = {
        delYn: 'N',
        [Op.not]: {
            [Op.or]: [{
                projectStatus: code.projectStatus[0].value
            }, {
                projectStatus: null
            }]
        }
    };

    let result = await models.projectList.findAll({
        where,
        order: [
            ['projectIdx', 'DESC']
        ],
        attributes: ['projectIdx', 'projectSubtitle', 'projectName', 'targetMoney', 'earningRate', 'startDate', 'investPeriod']
    });

    result.forEach((e, i) => {
        e.projectName = e.projectSubtitle + ' ' + e.projectName;
    });

    res.json({
        list: result
    });
};