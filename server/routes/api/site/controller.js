const models = require.main.require('./models');
const Op = require('sequelize').Op;
const bulkMail = require.main.require('./lib/bulkMail');
const sendSMS = require.main.require('./lib/sendSMS');
/* code*/
const code = require.main.require('./config/code');

/**
 * banner
 */
//베너 리스트 보기
exports.getBannerList = async (req, res) => {
    console.log(req.body);

    let result = {};
    let order = [
        ['sortingNum', 'ASC'],
        ['bannerIdx', 'ASC']
    ];
    let where = {
        delYn: 'N'
    };

    let list = await models.banner
        .findAndCountAll({
            where,
            order
        });

    result = {
        list: list.rows,
        count: list.count
    };

    res.json(result);
};

//베너 순위 저장
exports.setBannerList = async (req, res) => {
    console.log(req.body);

    //수정

    let data = req.body.list;

    await data.forEach(async (e, i) => {

        let idx = e.bannerIdx;
        delete e.bannerIdx;
        e.updatedDate = new Date();
        e.register = req.body.register;

        await models.banner.update(e, {
            where: {
                bannerIdx: idx
            }
        });
    });

    res.json({
        message: "저장되었습니다."
    });

}
//베너 상세보기
exports.getBannerView = async (req, res) => {
    console.log(req.body);

    if (req.body.bannerIdx) {

        let result = await models.banner.findOne({
            where: {
                bannerIdx: req.body.bannerIdx
            }
        });

        if (!result) {
            throw Error('search error');
        }

        res.json(result);
    } else {
        throw Error('error');
    }
}
//베너 상세내용 생성 및 수정
exports.setBannerView = async (req, res) => {
    console.log(req.body);

    //신규작성
    if (req.body.bannerIdx === 'new') {

        let data = {
            adminIdx: req.body.register,
            registedDate: new Date(),
            updatedDate: new Date(),
            files: req.body.files,
            title: req.body.title,
            text: req.body.text,
            sortingNum: req.body.sortingNum,
            delYn: 'N',
            showYn: 'Y'
        };

        await models.banner.create(data);

        res.json({
            message: "저장되었습니다."
        });

        //수정
    } else if (req.body.bannerIdx) {
        let data = req.body;
        const idx = req.body.bannerIdx;

        delete data.bannerIdx;
        data.updatedDate = new Date();

        await models.banner.update(data, {
            where: {
                bannerIdx: idx
            }
        });

        res.json({
            message: "저장되었습니다."
        });

    } else {
        throw Error('error');
    }
}

/**
 * notice
 */
//공지사항 리스트 보기
exports.getNoticeList = async (req, res) => {
    console.log(req.body);

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    var pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;

    let result = {};
    let order = [];
    let where = {
        delYn: 'N'
    };

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['noticeIdx', 'DESC']);
        } else {
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['updatedDate', 'DESC'],
            ['noticeIdx', 'DESC']
        ];
    }
    //검색어 입력시
    if (req.body.searchText) {
        where.title = { [Op.like]: '%' + req.body.searchText + '%' };
    }

    let list = await models.notice
        .findAndCountAll({
            where,
            offset: (currentPage - 1) * pageSize,
            limit: pageSize,
            order
        });

    result = {
        list: list.rows,
        count: list.count
    };

    res.json(result);
}
//공지사항 상세내용 보기
exports.getNoticeView = async (req, res) => {
    console.log(req.body);

    if (req.body.noticeIdx) {

        let result = await models.notice.findOne({
            where: {
                noticeIdx: req.body.noticeIdx
            }
        });

        if (!result) {
            throw Error('search error');
        }

        res.json(result);
    } else {
        throw Error('error');
    }
}
//공지사항 생성 및 수정
exports.setNoticeView = async (req, res) => {
    console.log(req.body);
    let title = req.body.title;
    let content = req.body.content;

    if(req.body.noticeType === code.noticeType[1].value){
        title = req.body.title.substring(0,100);
        content= req.body.content.substring(0,150) + '...';
    }
    //신규작성
    if (req.body.noticeIdx === 'new') {

        let data = {
            title,
            noticeType: req.body.noticeType,
            content,
            adminIdx: req.body.register,
            registedDate: new Date(),
            updatedDate: new Date(),
            delYn: 'N',
            showYn: 'Y',
            files : req.body.files
        };

        if(req.body.noticeType === code.noticeType[1].value){
            data.url = req.body.url
        }

        await models.notice.create(data);

        //공지사항 메일 발송
        const setting = await models.config.findById(1);

        if (setting.mailOption.option2 != null) {

            const mail = await models.mail.findOne({
                where: {
                    mailIdx: setting.mailOption.option2
                }
            });

            const user = await models.user.findAll({
                where: {
                    agreeEmail: 'Y',
                    memberStatus : code.memberStatus[0].value,
                    dormantYn : 'N'
                },
                attributes : ['email', 'name']
            });

            if (user && user.length > 0 && mail) {

                let to = '';
                let name = '';
                for (var i = 0; i < user.length; i++) {
                    to += user[i].dataValues.name+'/'+user[i].dataValues.email;
                    if(i !== user.length - 1) to += ',';
                }
            
                const bulkMailer = new bulkMail(setting);
                const mailOptions = {
                    from : '엔케이펀딩 < ' + setting.email + ' >',
                    to,
                    subject: mail.title,
                    render: 'mailing_layout',
                    data: {
                        title : mail.title,
                        content : mail.content
                    }
                }

                bulkMailer.send(mailOptions, true, function (error, result) { // arg1: mailinfo, agr2: parallel mails, arg3: callback
                    if (error) {
                        console.error(error);
                    } else {
                        console.info(result);
                    }
                });
            }
        }

        //공지사항 SMS 발송
        if (setting.smsOption.option2 != null) {

            const sms = await models.sms.findOne({
                where: {
                    smsIdx: setting.smsOption.option2
                }
            });

            const user = await models.user.findAll({
                where: {
                    agreeSms: 'Y',
                    memberStatus : code.memberStatus[0].value,
                    dormantYn : 'N'
                },
                attributes : ['email', 'name', 'mobile']
            });

            if (user && user.length > 0 && sms) {

                for (var i = 0; i < user.length; i++) {
                    if(user[i].dataValues.mobile){
                        let tel = user[i].dataValues.mobile
                        let text = sms.content.replace(/\{회원아이디\}/g, user[i].dataValues.email).replace(/\{이름\}/g, user[i].dataValues.name);
                        sendSMS(text, tel);
                    }
                }
            }
        }

        res.json({
            message: "저장되었습니다."
        });

        //수정
    } else if (req.body.noticeIdx) {
        let data = req.body;
        const idx = data.noticeIdx;
        data.content = content;
        data.title = title;
        data.updatedDate = new Date();

        console.log(data)
        await models.notice.update(data, {
            where: {
                noticeIdx: idx
            }
        });

        res.json({
            message: "저장되었습니다."
        });

    } else {
        throw Error('error');
    }
}

/**
 * FAQ
 */
//FAQ 목록 보기
exports.getFaqList = async (req, res) => {
    console.log(req.body);

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    var pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;

    let result = {};
    let order = [];
    let where = {
        delYn: 'N'
    };

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['faqIdx', 'DESC']);
        } else {
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['updatedDate', 'DESC'],
            ['faqIdx', 'DESC']
        ];
    }
    //검색어 입력시
    if (req.body.searchText) {
        where.title = { [Op.like]: '%' + req.body.searchText + '%' };
    }

    let list = await models.faq
        .findAndCountAll({
            where,
            offset: (currentPage - 1) * pageSize,
            limit: pageSize,
            order
        });

    result = {
        list: list.rows,
        count: list.count
    };

    res.json(result);
}
//FAQ 상세내용 보기
exports.getFaqView = async (req, res) => {
    console.log(req.body);

    let result = await models.faq.findOne({
        where: {
            faqIdx: req.body.faqIdx
        }
    });

    if (!result) {
        throw Error('search error');
    }

    res.json(result);
}
//FAQ 상세내용 생성 및 수정
exports.setFaqView = async (req, res) => {
    console.log(req.body);

    //신규작성
    if (req.body.faqIdx === 'new') {

        let data = {
            title: req.body.title,
            faqType: req.body.faqType,
            content: req.body.content,
            adminIdx: req.body.register,
            registedDate: new Date(),
            updatedDate: new Date(),
            delYn: 'N',
            showYn: 'Y'
        };

        await models.faq.create(data);

        res.json({
            message: "저장되었습니다."
        });

        //수정
    } else if (req.body.faqIdx) {
        let data = req.body;
        const idx = req.body.faqIdx;

        delete data.faqIdx;
        data.updatedDate = new Date();

        await models.faq.update(data, {
            where: {
                faqIdx: idx
            }
        });

        res.json({
            message: "저장되었습니다."
        });

    } else {
        throw Error('error');
    }
}

/**
 * popup
 */
//팝업 리스트 보기
exports.getPopupList = async (req, res) => {
    console.log(req.body);

    let currentPage = req.body.currentPage ? parseInt(req.body.currentPage) : 1;
    var pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;

    let result = {};
    let order = [];
    let where = {
        delYn: 'N'
    };

    //정렬옵션 선택시
    if (req.body.orderSelected) {
        if (req.body.orderSelected.type === 'ASC' || req.body.orderSelected.type === 'DESC') {
            order.unshift([req.body.orderSelected['item'], req.body.orderSelected['type']]);
            order.push(['popupIdx', 'DESC']);
        } else {
            where[req.body.orderSelected.item] = req.body.orderSelected.type
        }
    } else {
        order = [
            ['updatedDate', 'DESC'],
            ['popupIdx', 'DESC']
        ];
    }
    //검색어 입력시
    if (req.body.searchText) {
        where.title = { [Op.like]: '%' + req.body.searchText + '%' };
    }

    let list = await models.popup
        .findAndCountAll({
            where,
            offset: (currentPage - 1) * pageSize,
            limit: pageSize,
            order
        });

    result = {
        list: list.rows,
        count: list.count
    };

    res.json(result);
}
//팝업 상세내용 보기
exports.getPopupView = async (req, res) => {
    console.log(req.body);

    let result = await models.popup.findOne({
        where: {
            popupIdx: req.body.popupIdx
        }
    });

    if (!result) {
        throw Error('search error');
    }

    res.json(result);
}
//팝업 상세내용 생성 및 수정
exports.setPopupView = async (req, res) => {
    console.log(req.body);

    //신규작성
    if (req.body.popupIdx === 'new') {

        let data = {
            title: req.body.title,
            startDate: req.body.startDate,
            endDate: req.body.endDate,
            adminIdx: req.body.register,
            files: req.body.files,
            registedDate: new Date(),
            updatedDate: new Date(),
            delYn: 'N',
            showYn: req.body.showYn,
            unableHour: req.body.unableHour,
            
        };

        if(req.body.linkIdx) data.linkIdx = parseInt(req.body.linkIdx)

        await models.popup.create(data);

        res.json({
            message: "저장되었습니다."
        });

        //수정
    } else if (req.body.popupIdx) {
        let data = req.body;
        const idx = req.body.popupIdx;

        delete data.popupIdx;
        data.updatedDate = new Date();

        await models.popup.update(data, {
            where: {
                popupIdx: idx
            }
        });

        res.json({
            message: "저장되었습니다."
        });

    } else {
        throw Error('error');
    }
}

/**
 * terms
 */
//약관 리스트 보기
exports.getTermsList = async (req, res) => {
    console.log(req.body);
    let type1 = [];
    let type2 = [];
    let type3 = [];
    let type4 = [];

    let order = [
        ['startDate', "DESC"]
    ];
    let where = {
        delYn: 'N'
    };

    let list = await models.terms
        .findAndCountAll({
            where,
            order
        });

    result = {
        list: list.rows,
        count: list.count
    };

    res.json(result);

}
//약관 상세내용 보기
exports.getTermsView = async (req, res) => {
    console.log(req.body);

    let result = await models.terms.findOne({
        where: {
            termsIdx: req.body.termsIdx
        }
    });

    if (!result) {
        throw Error('search error');
    }

    res.json(result);
}
//약관 상세내용 생성 및 수정
exports.setTermsView = async (req, res) => {
    console.log(req.body);

    //신규작성
    if (req.body.termsIdx === 'new') {

        let data = {
            termsType: req.body.termsType,
            version: req.body.version,
            content: req.body.content,
            adminIdx: req.body.register,
            startDate: req.body.startDate,
            reDate: req.body.reDate,
            registedDate: new Date(),
            updatedDate: new Date(),
            delYn: 'N',
            showYn: 'Y'
        };

        console.log(data)

        await models.terms.create(data);

        res.json({
            message: "저장되었습니다."
        });

        //수정
    } else if (req.body.termsIdx) {
        let data = req.body;
        const idx = req.body.termsIdx;

        delete data.termsIdx;
        data.updatedDate = new Date();

        await models.terms.update(data, {
            where: {
                termsIdx: idx
            }
        });

        res.json({
            message: "저장되었습니다."
        });

    } else {
        throw Error('error');
    }
}

/* 설정 */
//설정 내용 보기
exports.getConfig = async (req, res) => {

    console.log(req.body);

    const result = await models.config.findOne({
        where: {
            configIdx: 1
        },
        attributes: {
            exclude: ['bankCode', 'sticker', 'mailOption', 'smsOption', 'configIdx']
        }
    });

    res.json(result);
}

//설정 저장
exports.setConfig = async (req, res) => {

    console.log(req.body);

    let item = req.body;
    delete item.maxOption;

    item.minMoney = typeof item.minMoney === 'string' ? parseInt(item.minMoney.replace(/,/g, '')) : null;
    item.accumulatedLoan = typeof item.accumulatedLoan === 'string' ? parseInt(item.accumulatedLoan.replace(/,/g, '')) : null;
    item.loanBalance = typeof item.loanBalance === 'string' ? parseInt(item.loanBalance.replace(/,/g, '')) : null;

    await models.config.update(item, {
        where: {
            configIdx: 1
        }
    });

    res.json({ message: '수정되었습니다.' });
}

//한도 설정 저장
exports.setConfigLimit = async (req, res) => {

    console.log(req.body);
    let item = {};
    item.maxOption = req.body;
    item.maxOption[0].config.maximum = item.maxOption[0].config.maximum ? parseInt(item.maxOption[0].config.maximum.replace(/\D/g, '')) : null;
    item.maxOption[1].config.maximum = item.maxOption[1].config.maximum ? parseInt(item.maxOption[1].config.maximum.replace(/\D/g, '')) : null;
    item.maxOption[2].config.maximum = item.maxOption[2].config.maximum ? parseInt(item.maxOption[2].config.maximum.replace(/\D/g, '')) : null;
    item.maxOption[0].config.projectMaximum = item.maxOption[0].config.projectMaximum ? parseInt(item.maxOption[0].config.projectMaximum.replace(/\D/g, '')) : null;
    item.maxOption[1].config.projectMaximum = item.maxOption[1].config.projectMaximum ? parseInt(item.maxOption[1].config.projectMaximum.replace(/\D/g, '')) : null;
    item.maxOption[2].config.projectMaximum = item.maxOption[2].config.projectMaximum ? parseInt(item.maxOption[2].config.projectMaximum.replace(/\D/g, '')) : null;

    await models.sequelize.transaction(async (t) => {

        await models.config.update(item, {
            where: {
                configIdx: 1
            },
            transaction: t
        });

        await models.user.update({
            totalInvestableAmount: item.maxOption[0].config.maximum,
            projectInvestableAmount: item.maxOption[0].config.projectMaximum
        }, {
                where: {
                    investorType: item.maxOption[0].value
                }
            });

        await models.user.update({
            totalInvestableAmount: item.maxOption[1].config.maximum,
            projectInvestableAmount: item.maxOption[1].config.projectMaximum
        }, {
                where: {
                    investorType: item.maxOption[1].value
                }
            });

        await models.user.update({
            totalInvestableAmount: item.maxOption[2].config.maximum,
            projectInvestableAmount: item.maxOption[2].config.projectMaximum
        }, {
                where: {
                    investorType: item.maxOption[2].value
                }
            });

    }).then(function (result) {

        res.json({ message: '수정되었습니다' });

    }).catch(async (err) => {

        res.json({ error: 'write error' });

    });


}

//알람 리스트 보기
exports.getAlarmList = async (req, res) => {

    let limit = req.body.length || 15;
    let list = await models.alarm.findAll({
        where: {
            delYn: 'N'
        },
        order: [['registedDate', 'DESC']],
        limit
    });

    let totalCount = await models.alarm.count({
        where: {
            delYn: 'N'
        }
    });

    return res.json({ list: list, count: totalCount });
}

//알람 삭제
exports.setAlarmList = async (req, res) => {

    let alarmIdx = req.body.idx;
    let list = await models.alarm.update({
        delYn: 'Y'
    }, {
            where: {
                alarmIdx
            }
        });

    return res.json({ result: 'SUCCESS' });
}