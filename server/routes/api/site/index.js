const router = require('express').Router();
const controller = require('./controller');
const authMiddleware = require.main.require('./middlewares/auth');
const upload = require('./upload');

for(let fn in controller){
    let service = controller[fn];
    if (typeof service === 'function') {
        router.use('/'+fn, authMiddleware);
        router.post('/'+fn, service);
    }
}

router.use(require('./upload'));

module.exports = router;
