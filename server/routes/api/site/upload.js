const express = require('express');
const router = express.Router();
const multer = require('multer');
const crypto = require('crypto');
const mime = require('mime');
const moment = require('moment');
const fs = require('fs');
const mainPath = require('path').dirname(require.main.filename);
const authMiddleware = require.main.require('./middlewares/auth');
let existUploadFolder = '';

const storage = multer.diskStorage({
    destination: function(req, file, cb) {

        if( fs.existsSync('public/uploads') === false ) {
            fs.mkdirSync('public/uploads');
        }

        let folder = 'public/uploads/'+moment().format('YYYYMM');
        if(existUploadFolder != folder && fs.existsSync(folder) === false) {
            fs.mkdirSync(folder);
            existUploadFolder = folder;
        }
        cb(null, folder);
    },
    filename: function(req, file, cb) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
        });
    }
});
const upload = multer({storage : storage});

router.use('/upload', authMiddleware);
router.post('/upload', upload.array('fileupload'), async (req, res, next) => {
    let files = [];
    for(file of req.files) {
        let path = file.path.replace(/\\/g, '/');
        path = path.replace('public/', '');

        files.push({
            size : file.size,
            name : file.filename,
            originalName : file.originalname,
            path : '/' + path,
            mimeType : file.mimetype
        });
    }
    res.send({
        files : files
    });
});

module.exports = router;
