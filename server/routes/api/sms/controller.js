const models = require.main.require('./models');
const sendSMS = require.main.require('./lib/sendSMS');
const request = require('request');

/**
 * sms config
 */

//발송
exports.sendSMS = async (req, res) => {

    console.log(req.body);

    let result = await sendSMS(req.body.text, req.body.tel);
    res.json(result);

};

exports.getCoin = async (req, res) => {

    let result = '';
    let setting = await models.config.findById(1);

    await request.post({
        url: 'http://www.icodekorea.com/res/coin2.php',
        form: {
            sms_id: setting.dataValues.icodeId,
            sms_pw: setting.dataValues.icodePwd
        }
    }, function (err, httpResponse, body) {
        
        result = body;
        res.json(result);
    });
}

// SMS 상세보기
exports.getSMSView = async (req, res) => {
    console.log(req.body);
    const smsIdx = req.body.idx;

    let result = await models.sms.findOne({
        where: {
            smsIdx: smsIdx,
            delYn: 'N'
        }
    });

    res.json(result);
}

// SMS 저장, 수정
exports.setSMSView = async (req, res) => {
    console.log(req.body);
    let item = req.body;
    const smsIdx = req.body.idx;
    delete item.idx;
    item.updatedDate = new Date();

    if (smsIdx === 'new') {
        item.registedDate = new Date();
        await models.sms.create(item);

    } else {

        await models.sms.update(item, {
            where: {
                smsIdx: item.smsIdx
            }
        });

    }

    res.json({ message: '저장되었습니다.' });
}

// SMS 삭제
exports.setSMSViewDel = async (req, res) => {
    console.log(req.body);
    let item = req.body;
    const smsIdx = req.body.idx;
    delete item.idx;
    item.updatedDate = new Date();

    await models.sms.update(item, {
        where: {
            smsIdx: smsIdx
        }
    });
    let setting = await models.config.findById(1);
    setting.smsOption = setting.dataValues.smsOption
    Object.keys(setting.dataValues.smsOption).forEach((e,i) => {
        if(setting.dataValues.smsOption[e] == smsIdx) setting.smsOption[e] = null
    });

    await setting.save();

    res.json({ message: '저장되었습니다.' });
}

// SMS 목록 보기
exports.getSMSList = async (req, res) => {
    console.log(req.body);

    let result = await models.sms.findAndCountAll({
        where: {
            delYn: 'N'
        }
    });

    res.json({
        list: result.rows,
        count: result.count
    });
}

// SMS 설정 보기
exports.getOptionList = async (req, res) => {
    console.log(req.body);

    let result = await models.config.findById(1);

    res.json(result.smsOption);
}
// SMS 설정 저장
exports.setSMSOption = async (req, res) => {
    console.log(req.body);

    let item = {};
    for (i = 1; i < 7; i++) {
        item['option' + i] = req.body['option' + i] || null
    }

    let result = await models.config.findOne({
        where: {
            configIdx: 1
        }
    });

    result.smsOption = item;
    await result.save();

    res.json({
        message: "저장되었습니다."
    });
}