const models = require.main.require('./models');
const Op = require('sequelize').Op;
const calculate = require.main.require('./lib/calculate');
/* code*/
const code = require.main.require('./config/code');
const seypertCode = require.main.require('./config/seypertCode');
const guid = require.main.require('./config').seypert.guid;
const seypert = require.main.require('./lib/seypert');

/**
 * sms config
 */

//회원리스트
exports.getUserList = async (req, res) => {
    
    let result = await models.user.findAll({
        where : {
            memberStatus : code.memberStatus[0].value,
            guid :{
                [Op.ne] : null
            }
        },
        attributes : ['email', 'name', 'companyName', 'businessType', 'userIdx'],
        order : [['name','ASC'],['email','ASC']],
        include : [{
            model : models.userBankAccount,
            required : true,
            where : {
                virtualBankConfirm : 'Y'
            }
        }]
    })
    
    result.forEach((e,i) => {
        let name = e.name;
        if(e.businessType !== code.businessType[0].value) {
            name = e.companyName;
            if(!e.companyName) {
                name = e.name;
            }
        }
        
        e.name = name + '(' + e.email + ')';
    });

    res.json(result);
}

//내역조회
exports.getStatement = async (req, res) => {
    console.log(req.body);

    let currentPage = req.body.currentPage;
    let pageSize = req.body.pageSize;
    let startDate = null;
    let endDate = null;
    let trnsctnSt = req.body.searchText;
    
    if(req.body.startDate) startDate = req.body.startDate.replace(/\D/g,'');
    if(req.body.endDate) endDate = req.body.endDate.replace(/\D/g,'');

    let result = await seypert.getSeyfertList(guid, currentPage, pageSize, startDate, endDate, trnsctnSt);
    if(result.result) {

        for(i=0;i<result.result.list;i++){
            result.result.list[i].trnsctnSt = seypertCode[result.result.list[i].trnsctnSt];
        }
        res.json({
            list : result.result.list,
            count : result.result.totalCount
        });
    }
}

//관리자 이체
exports.adminPendTransfer = async (req, res) => {
    console.log(req.body);
    
    const userIdx = req.body.userIdx;
    const amount = parseInt(req.body.amount.replace(/\D/g,''));
    const reason = req.body.reason;
    let title = req.body.title;
    const user = await models.user.findOne({
        where : {
            userIdx
        }
    });

    title = user.dataValues.name + '님('+user.dataValues.email+')에게 '+amount+'원 이체 - ' + title + ' [관리자승인]';

    if(!user.dataValues.guid) return res.json({error : 'no guid member'});
    
    // get dstMemType
    let dstMemType = await seypert.getMemType(user.dataValues.investorType, user.dataValues.businessTypeConfirmYn, user.dataValues.loanCompanyConfirm);
    //세이퍼트 관리자 이체
    let result = await seypert.adminPendTransfer(user.dataValues.guid, amount, title, dstMemType, reason);
    console.log(result);
    if(result.status === 'SUCCESS') {
        if(result.cdKey === 'SFRT_TRNSFR_PND_AGRREED'){

            //이체 확정
            let result2 = await seypert.releasePend(result.tid, title+'[관리자승인]');
            console.log(result2)
            if(result2.result && result2.result.status === 'SUCCESS') {
                if( result2.result.cdKey === 'SFRT_TRNSFR_PND_RELEASED'){
                    //알람
                    models.alarm.create({
                        title : '이체를 완료 하였습니다.',
                        content : user.dataValues.name+'('+user.dataValues.email+')님에게 '+ amount + '원을 이체하였습니다.',
                        registedDate : new Date(),
                        type : 'success'
                    });
                    return res.json({status : 'SUCCESS', message : '예약 이체를 승인하였습니다.'})
                }
                return res.json({status : 'SUCCESS'});
            }else{
                if(result2.error.cdKey === 'STATUS_HAS_FINISHED') return res.json({error : '이체가 이미 처리되었습니다.'})
                return res.json({error : '이체 확정 오류. 입출금 내역에서 다시 확인해주세요.'});
            }

            return res.json({status : 'SUCCESS'});
        }else{
            return res.json({error : 'unknown error'});
        }

    }else{
        return res.json(result);
    }
}

//전송 승인
exports.transferConfirm = async (req, res) => {
    console.log(req.body);
    
    const tid = req.body.tid;
    
    let result = await seypert.releasePend(tid, '관리자 이체 [관리자 승인]');
    console.log(result)
    if(result.result && result.result.status === 'SUCCESS') {
        if( result.result.cdKey === 'SFRT_TRNSFR_PND_RELEASED') return res.json({status : 'SUCCESS', message : '예약 이체를 승인하였습니다.'})
        return res.json({status : 'SUCCESS'});
    }else{
        if(result.error.cdKey === 'STATUS_HAS_FINISHED') return res.json({error : '이체가 이미 처리되었습니다.'})
        return res.json({error : result.error.cdDesc});
    }
}

//잔액조회
exports.getBalance = async (req, res) => {
    console.log(req.body);
        
    let result = await seypert.checkVA();
    if(result.status === 'SUCCESS') {
        
        return res.json({amount : result.amount});

    }else{
        return res.json(result);
    }
}

//회원 잔액조회
exports.getMemberBalance = async (req, res) => {
    console.log(req.body);
    const memberGuid = req.body.guid;
        
    let result = await seypert.checkVA(memberGuid);
    if(result.status === 'SUCCESS') {
        let account = await models.userBankAccount.findOne({
            where : {
                userIdx : req.body.userIdx
            },
            attributes : ['virtualAccountBalance', 'userBankAccountIdx']
        });
        account.virtualAccountBalance = result.amount;
        await account.save();
        return res.json({amount : result.amount});
    }else{
        return res.json(result);
    }
}

//회원 누적투자금액조회
exports.getMemberInvest = async (req, res) => {
    console.log(req.body);
        
    let result = await calculate.IableA(req.body.userIdx);
    if(result.status === 'SUCCESS') {
        let user = await models.user.findOne({
            where : {
                userIdx : req.body.userIdx
            },
            attributes : ['totalInvestedMoney']
        });
        return res.json({amount : user.dataValues.totalInvestedMoney});
    }else{
        return res.json(result);
    }
}