const cipher = require.main.require('./lib/cipher');
const urlencode = require('urlencode');
const router = require('express').Router();
const sendMail = require.main.require('./lib/sendMail');
const jwt = require('jsonwebtoken');
const config = require.main.require('./config');
const {PASSWORD_REGEXP, EMAIL_REGEXP, ID_REGEXP, CODE_REGEXP, COMPANYNAME_REGEXP_MATCH} = require.main.require('./lib/regEx');

// 토큰 인증
// 셀러, 방문자인 경우 email, group
// 관리자, 운영자인 경우 id, group 를 property로 가진다
router.get('/tokenVerification', async (req, res, next) => {

    const token = req.headers['authorization'];

    try
    {
        let bool;
        let result = jwt.verify(token, req.app.get('jwt-secret'),{
            algorithm: 'HS512'
        });
        result = cipher.decode(result.data);
        result = JSON.parse(result);
        
        if(result && (result.email || result.id) && result.group) bool = true
        else bool = false;

        if(!bool){
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        res.json({
            success : true,
            message : '',
            data : {
                auth : bool,
                info : result
            }
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TAI-11]",
            data : {
                auth : false
            }
        });
    }
});
// 비밀번호 재설정
router.post('/resetPassword', async (req, res, next) => {
    console.log(req.body);
    try
    {
        if(!req.body.email || !EMAIL_REGEXP.test(req.body.email) ) throw "잘못된 메일 형식입니다";
        if(!req.body.code || !req.body.password) throw "입력 데이터 오류 [TAI-21]";
        if(!PASSWORD_REGEXP.test(req.body.password) ) throw "비밀번호는 문자, 숫자, 특수문자(!@#$%^&+=) 포함 8~15자 이내 입니다";

        const password = cipher.sha256(req.body.password);
        let [ queryRows, queryFields ] = await req.db.query("CALL USP_UDATE_FORGOT_USER_PASSWORD(?, ?, ?)", 
            [req.body.email, password, req.body.code]);
        
        const query = queryRows[0][0];

        res.json({
            success : query.result != 0 ? true : false,
            message : query.message
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TAI-21]"
        });
    }
});
// 비밀번호 재설정 메일 발송
router.post('/sendPwResetMail', async (req, res, next) => {
    console.log(req.body);
    try
    {
        if(!req.body.email || !EMAIL_REGEXP.test(req.body.email) ) throw "잘못된 메일 형식입니다";

        //이메일 중복 체크
        let [ queryRows, queryFields ] = await req.db.query('SELECT COUNT(*) AS COUNT FROM seller_user WHERE email = ? LIMIT 1;',
            [req.body.email]);
        if(queryRows[0].COUNT == 0) throw "이메일을 다시 확인해주세요";

        let message;
        let success = true;
        const encodedCode = cipher.sha256(req.body.email + new Date().getTime());
        const href = `${config.ng.protocol}://${config.ng.domain}${config.ng.port != 80 ? ':'+config.ng.port : ''}/pages/auth/reset-password?email=${urlencode(req.body.email)}&code=${encodedCode}`;

        const result = await sendMail({
            to: req.body.email,
            subject: "[USM] 비밀번호 재설정 메일",
            render: 'mailLayout',
            data: {
                title : "USM 비밀번호 재설정",
                content : `아래 링크를 통해 비밀번호를 다시 설정할 수 있습니다. <br />
                    <div style='text-align:center; padding:20px;'>
                        <a href="${href}" target="_blank"><b>${href}</b></a>
                    </div>`
            }
        });
        
        if(result.response.indexOf("250") == -1 && result.response.indexOf("OK") == -1) throw "이메일 발송 에러. 잠시 후 다시 시도해주세요";
        else message = "인증메일이 발송되었습니다\n메일을 확인해주세요";

        //유효기간 하루
        [ queryRows, queryFields ] = await req.db.query('CALL USP_INSERT_MAIL_LOG(?, ?, ?, ?);',
            [req.body.email, encodedCode, null, 60 * 24]);

        if(queryRows[0].result) {
            success = false;
            message = "시스템 에러 [TAI-31]";
        }

        res.json({
            success : success,
            message,
            data : {}
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TAI-32]",
        })
    }
});
// 인증메일 발송
router.post('/sendAuthMail', async (req, res, next) => {
    console.log(req.body);
    try
    {
        if(!req.body.email || !EMAIL_REGEXP.test(req.body.email) ) throw "잘못된 메일 형식입니다";

        //이메일 중복 체크
        let [ queryRows, queryFields ] = await req.db.query('SELECT COUNT(*) AS COUNT FROM seller_user WHERE email = ?;',
            [req.body.email]);
        if(queryRows[0].COUNT > 0) throw "이미 가입된 이메일이 있습니다";

        //이메일 발송
        const code = Math.floor(Math.random() * (900000)) + 100000;
        const result = await sendMail({
            to: req.body.email,
            subject: "[USM] 보안 인증번호를 안내해드립니다",
            render: 'mailLayout',
            data: {
                title : "보안 인증번호를 안내해드립니다",
                content : `안녕하세요. <b>유벤더</b> 입니다. <br>
                요청하신 보안 인증번호를 안내해드립니다. <br>
                아래 번호를 인증번호 입력란에 입력하시면 보안인증이 완료 됩니다. <br>
                    <div style='text-align:center; padding:20px;'>
                        <h2><b>${code}</b></h2>
                    </div>
                <br>
                인증번호는 5분동안 유효하며, 5분 경과 시 인증번호를 재발급 받으셔야 합니다.<br>
                감사합니다.
                <br>
                <hr>
                <br>
                본 메일은 발신 전용으로 회신되지 않습니다. 추가 문의는 <a>[고객센터]</a>를 이용해주시기 바랍니다.
                <br>
                <small>Copyright(C) 유벤더 All right reserved.<small>
                `
            }
        });

        let message;
        let success = true;
        if(result.response.indexOf("250 ok") == -1) throw "이메일 발송 에러. 잠시 후 다시 시도해주세요";
        else message = "인증메일이 발송되었습니다\n메일을 확인해주세요";

        //유효기간 5분후
        [ queryRows, queryFields ] = await req.db.query('CALL USP_INSERT_MAIL_LOG(?, ?, ?, ?);',
            [req.body.email, code, null, 5]);

        if(queryRows[0].result) {
            success = false;
            message = "시스템 에러 [TAI-41]";
        }

        res.json({
            success : success,
            message : message,
            data : { id : queryRows.insertId }
        });
    }
    catch(err)
    {
        
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TAI-42]",
        });
    }
});

// 이메일 인증 번호 확인
router.post('/checkMailCode', async (req, res, next) => {
    console.log(req.body);
    try
    {
        if(!req.body.email || !EMAIL_REGEXP.test(req.body.email) ) throw "잘못된 메일 형식입니다";
        if(!req.body.code) throw "필수 항목은 모두 입력해주세요";
        if(!CODE_REGEXP.test(req.body.code) ) throw "잘못된 코드 형식입니다";
        
        //회원 정보 저장
        let [ queryRows, queryFields ] = await req.db.query('CALL USP_SELECT_CHECK_MAIL_CODE(?, ?);',
            [req.body.email, req.body.code]);
        
        const query = queryRows[0][0];
        res.json({
            success : query.result != 0 ? true : false,
            message : query.message,
            data : {
                authCallback : query.result
            }
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TAI-51]",
        });
    }
});

//일반 셀러 회원 가입
router.post('/memberSignUp', async (req, res, next) => {
    console.log(req.body);
    try
    {
        //회원 정보 저장
        if(!req.body.email || !EMAIL_REGEXP.test(req.body.email) ) throw "잘못된 메일 형식입니다";
        if(!req.body.companyName || !req.body.password || !req.body.code) throw "필수 입력정보를 모두 입력해주세요";
        if(req.body.companyName.match(COMPANYNAME_REGEXP_MATCH) != null) throw "회사이름에 ()-를 제외한 기호를 사용할 수 없습니다";
        if(!CODE_REGEXP.test(req.body.code) ) throw "잘못된 코드 형식입니다";
        if(!PASSWORD_REGEXP.test(req.body.password) ) throw "비밀번호는 문자, 숫자, 특수문자(!@#$%^&+=) 포함 8~15자 이내 입니다";
        if(!req.body.authCallback) throw "이메일 인증을 완료해주세요";

        const password = cipher.sha256(req.body.password);
        let [ queryRows, queryFields ] = await req.db.query('CALL USP_INSERT_SELLER_USER(?, ?, ?, ?, ?, ?);',
            [req.body.companyName, req.body.email, password, req.body.code, req.body.authCallback, req.body.from || null]);

        if(!queryRows[0][0].result) throw queryRows[0][0].message;

        res.json({
            success : queryRows[0][0].result != 0 ? true : false,
            message : queryRows[0][0].message
        })
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TAI-61]",
        });
    }
});

//일반 셀러 로그인
router.post('/memberSignIn', async (req, res, next) => {
    console.log(req.body);
    try
    {
        if(!req.body.email || !EMAIL_REGEXP.test(req.body.email) ) throw "잘못된 메일 형식입니다";
        if(!req.body.password) throw "필수 입력정보를 모두 입력해주세요";
        if(!PASSWORD_REGEXP.test(req.body.password) ) throw "비밀번호를 확인해주세요";
        //회원 정보 저장

        const password = cipher.sha256(req.body.password);
        const ip = req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
        const userAgent = req.headers['user-agent'];

        let [ queryRows, queryFields ] = await req.db.query('CALL USP_SEARCH_SELLER_SIGNIN(?, ?, ?, ?);',
            [req.body.email, password, ip, userAgent]);

        if(!queryRows[0][0].result) throw queryRows[0][0].message;

        let group = queryRows[0][0].userGroup;
        let companyName = queryRows[0][0].companyName;
        let isSellerCertified = queryRows[0][0].isSellerCertified;
        let isAccountCertified = queryRows[0][0].isAccountCertified;
        let token;
        let payload = cipher.encode(JSON.stringify({ email: req.body.email, group, companyName, isSellerCertified, isAccountCertified}));

        if(queryRows[0][0].result == 1){
            token = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
                data: payload
              }, req.app.get('jwt-secret'),
              {
                algorithm: 'HS512'
              });
        }

        res.json({
            success : queryRows[0][0].result != 0 ? true : false,
            message : queryRows[0][0].message,
            data : {
                token
            }
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TAI-71]",
        });
    }
});
//관리자 로그인
router.post('/adminSignIn', async (req, res, next) => {
    console.log(req.body);
    try
    {
        
        if(!req.body.id || !ID_REGEXP.test(req.body.id) ) throw "잘못된 아이디형식입니다";
        if(!req.body.id || !req.body.password) throw "필수 입력정보를 모두 입력해주세요";
        if(!PASSWORD_REGEXP.test(req.body.password) ) throw "비밀번호를 확인해주세요";
        //회원 정보 저장

        const password = cipher.sha256(req.body.password);
        const ip = req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
        const userAgent = req.headers['user-agent'];

        let [ queryRows, queryFields ] = await req.db.query('CALL USP_SEARCH_ADMIN_SIGNIN(?, ?, ?, ?);',
            [req.body.id, password, ip, userAgent]);

        if(!queryRows[0][0].result) throw queryRows[0][0].message;
        let token;
        let payload = cipher.encode(JSON.stringify({ id: req.body.id, group: queryRows[0][0].group }));

        if(queryRows[0][0].result == 1){
            token = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
                data: payload
              }, req.app.get('jwt-secret'),
              {
                algorithm: 'HS512'
              });
        }

        res.json({
            success : queryRows[0][0].result != 0 ? true : false,
            message : queryRows[0][0].message,
            data : {
                token
            }
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TAI-81]",
        });
    }
});


module.exports = router;
