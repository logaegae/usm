const router = require('express').Router();

router.use('/users', require('./users'));
router.use('/product', require('./product'));
router.use('/profile', require('./profile'));
router.use('/dashboard', require('./dashboard'));
router.use('/auth', require('./auth'));
router.use('/upload', require('./upload'));

module.exports = router;
