const router = require('express').Router();
const authMiddleware = require.main.require('./middlewares/auth');
const {upload, deleteUploaded} = require.main.require('./lib/upload');

const cipher = require.main.require('./lib/cipher');
const config = require.main.require('./config');
const {PASSWORD_REGEXP, EMAIL_REGEXP, PHONE_REGEXP, COMPANYNAME_REGEXP_MATCH, SYMBOL_REGEXP_REPLACE, COMPANYNUMBER_REGEXP} = require.main.require('./lib/regEx');

//관리자 회원 계정 정보
router.get('/aboutAdmin', authMiddleware, async (req, res, next) => {
    try
    {
        // Only for Admin group
        
        if(req.decoded.group !== 'admin' && req.decoded.group !== 'operator') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        let [ queryRows, queryFields ] = await req.db.query('CALL USP_SELECT_ADMIN_USER_ABOUT(?);',
            [req.decoded.id]);
        const query = queryRows[0][0];
        let data = {
            about : {
                userId : query.userId,
                cDate : query.cDate,
                groupName : query.groupName,
                email : query.email
            }
        }

        res.json({
            success : !!query.result,
            message : query.message,
            data
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TPfI-11]"
        });
    }
});
//셀러 회원 계정 정보
router.get('/aboutSeller', authMiddleware, async (req, res, next) => {
    try
    {
        // Only for Seller group
        console.log(req.decoded)
        if(req.decoded.group !== 'seller' && req.decoded.group !== 'visitor') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        let [ queryRows, queryFields ] = await req.db.query('CALL USP_SELECT_SELLER_USER_ABOUT(?, ?);',
            [req.decoded.email, req.decoded.isSellerCertified]);
        const query = queryRows[0][0];
        
        let obj = {
            success : !!query.result,
            message : query.message
        }
        delete query.success;
        delete query.message;
        obj.data = {about : query }

        res.json(obj);
    }
    catch(err)
    {
        console.log(err)
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TPfI-21]"
        });
    }
});
//유저 회원 비밀번호 변경
router.post('/changePwInfo', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        if(!req.body.oldPassword || !req.body.password) throw "비밀번호를 입력해주세요";
        if(!PASSWORD_REGEXP.test(req.body.oldPassword) ) throw "잘못된 비밀번호 형식입니다";
        if(!PASSWORD_REGEXP.test(req.body.password) ) throw "잘못된 비밀번호 형식입니다";

        const oldPassword = cipher.sha256(req.body.oldPassword);
        const password = cipher.sha256(req.body.password);
        let [ queryRows, queryFields ] = await req.db.query('CALL USP_UDATE_USER_PASSWORD(?, ?, ?, ?);',
            [req.decoded.id || req.decoded.email, oldPassword, password, req.decoded.group]);

        const query = queryRows[0][0];

        res.json({
            success : query.result != 0 ? true : false,
            message : query.message
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TPfI-31]",
        });
    }
});
//관리자 회원 계정 정보 변경
router.post('/changeAdminInfo', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin group
        if(req.decoded.group !== 'admin') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        if(!req.body.email || !EMAIL_REGEXP.test(req.body.email) ) throw "잘못된 메일 형식입니다";

        const [ queryRows, queryFields ] = await req.db.query('UPDATE admin_user SET email = ? WHERE userId = ?;',
            [req.body.email, req.decoded.id]);

        if(queryRows.affectedRows == 0) throw "시스템 에러 [TPfI-42]";

        res.json({
            success : true,
            message : '변경되었습니다',
            data : {
                email : req.body.email
            }
        });
    }
    catch (err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TPfI-41]",
        });
    }
});
//셀러 판매 담당자 연락처 수정
router.post('/changeSellerPhone', authMiddleware, async (req, res, next) => {
    console.log(req.body)
    try
    {
        // Only for Seller group
        if(req.decoded.group !== 'seller' && req.decoded.group !== 'visitor') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }
        if(!req.body.sellerPhone) throw "판매 담당자 연락처를 입력해주세요";

        if(!PHONE_REGEXP.test(req.body.sellerPhone) ) throw "잘못된 전화번호 형식입니다";
        if(req.body.sellerAlarmEmail && !EMAIL_REGEXP.test(req.body.sellerAlarmEmail) ) throw "잘못된 메일 형식입니다";

        const [ queryRows, queryFields ] = await req.db.query('CALL USP_UPDATE_SELLER_PHONE(?, ?, ?);',
            [req.decoded.email, req.body.sellerPhone, req.body.sellerAlarmEmail]);

        if(!queryRows[0][0].result) throw "시스템 에러 [TPfI-52]";

        res.json({
            success : true,
            message : '변경되었습니다',
            data : {
            }
        });
    }
    catch (err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TPfI-51]",
        });
    }
});
//셀러 회원 사업자 인증
router.post('/changeSellerInfo', authMiddleware, async (req, res, next) => {
    try
    {
        // Only for Admin group
        if(req.decoded.group !== 'seller' && req.decoded.group !== 'visitor') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        // 파일 업로드 처리

        let [ queryRows, queryFields ] = await req.db.query('SELECT COUNT(*) AS COUNT, id FROM seller_user WHERE email = ?;',
            [req.decoded.email]);

        if(queryRows[0].COUNT == 0) throw "데이터 에러 [TPfI-62]";
        
        const result = await upload(req, res, {
            fieldName : "uploadFile", 
            dest : "seller-profile/" + req.decoded.email, 
            processId : "sellerProfileAttachment", 
            referenceId : queryRows[0].id
        });

        // 여기까지 업로드 완료

        console.log(req.body)
        queryRows = null;
        queryFields = null;

        if(!result.success) throw { message : result.message, files : result.data.list};
        
        if(!req.body.companyName || !req.body.representation || !req.body.companyPhone || !req.body.sector || !req.body.business || !req.body.companyNumber || !req.body.corporateNumber) {
            throw { message: "필수 입력정보를 모두 입력해주세요", files: result.data.list};
        }
        if(req.body.companyName.match(COMPANYNAME_REGEXP_MATCH) != null) throw { message: "사업자(체)명에 ()-&를 제외한 기호를 사용할 수 없습니다", files: result.data.list };
        if(req.body.representation.match(SYMBOL_REGEXP_REPLACE) != null) throw { message: "대표자명에 기호를 사용할 수 없습니다", files: result.data.list };
        if(req.body.sector.match(SYMBOL_REGEXP_REPLACE) != null) throw { message: "업종에 기호를 사용할 수 없습니다", files: result.data.list };
        if(req.body.business.match(SYMBOL_REGEXP_REPLACE) != null) throw { message: "업태에 기호를 사용할 수 없습니다", files: result.data.list };
        if(req.body.companyNumber.match(COMPANYNUMBER_REGEXP) === null) throw { message: "사업자 등록번호 형식을 확인해 주세요", files: result.data.list };
        if(req.body.corporateNumber.match(COMPANYNUMBER_REGEXP) === null) throw { message: "법인 등록번호 형식을 확인해 주세요", files: result.data.list };
        if(!PHONE_REGEXP.test(req.body.companyPhone) ) throw { message: "잘못된 전화번호 형식입니다", files: result.data.list };

        [ queryRows, queryFields ] = await req.db.query('CALL USP_UPDATE_SELLER_PROFILE(?, ?, ?, ?, ?, ?, ?, ?);',
            [req.decoded.email, req.body.companyName, req.body.representation, req.body.companyPhone, req.body.sector, req.body.business, req.body.companyNumber, req.body.corporateNumber]);

        if(!queryRows[0][0].result) throw {message: "시스템 에러 [TPfI-63]", files: result.data.list};

        res.json({
            success : true,
            message : '신청이 완료되었습니다',
            data : {
            }
        });
    }
    catch (err)
    {
        //에러 발생시 업로드한 파일, 기록 삭제
        if(typeof err === "object" && err.files) deleteUploaded(req, err.files)
        
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TPfI-61]",
        });
    }
});

//셀러 판매 담당자 계정 정보 인증
router.post('/changeSellerAccount', authMiddleware, async (req, res, next) => {
    console.log(req.body)
    try
    {
        // Only for Seller group
        if(req.decoded.group !== 'seller' && req.decoded.group !== 'visitor') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }
        if(!req.body.bankName || !req.body.ownerName || !req.body.bankAccount) throw "필수 입력정보를 모두 입력해주세요";
        if(req.body.bankName.match(SYMBOL_REGEXP_REPLACE) != null) throw "은행명에 기호를 사용할 수 없습니다";
        if(req.body.ownerName.match(SYMBOL_REGEXP_REPLACE) != null) throw "예금주명에 기호를 사용할 수 없습니다";
        if(!NUMBER_REGEXP.test(req.body.bankAccount) ) throw "계좌번호는 숫자만 입력해주세요";

        const [ queryRows, queryFields ] = await req.db.query('CALL USP_MERGE_SELLER_ACCOUNT(?, ?, ? ,?);',
            [req.decoded.email, req.body.bankName, req.body.ownerName, req.body.bankAccount]);
        console.log(queryRows)
        if(!queryRows[0][0].result) throw "시스템 에러 [TPfI-72]";

        res.json({
            success : true,
            message : '신청이 완료되었습니다',
            data : {
            }
        });
    }
    catch (err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TPfI-71]",
        });
    }
});
module.exports = router;
