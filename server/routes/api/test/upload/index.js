const router = require('express').Router();
const authMiddleware = require.main.require('./middlewares/auth');
const {upload, deleteUploaded} = require.main.require('./lib/upload');

const cipher = require.main.require('./lib/cipher');
const config = require.main.require('./config');

//관리자의 회원정보 업데이트 혹은 생성
router.post('/tempProductImage', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin , Operator group
        if(req.decoded.group !== 'seller' && req.decoded.group !== 'admin' && req.decoded.group !== 'operator') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        // 파일 업로드 처리

        const result = await upload(req, res, {
            fieldName : "uploadFile", 
            dest : "temp", 
            processId : null, 
            referenceId : null,
            temp : true
        });

        res.json({
            success : result.success,
            data: result.data
        });
    }
    catch (err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TPI-71]",
        });
    }
});

//관리자의 회원정보 업데이트 혹은 생성
router.post('/deleteUploadedImage', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin , Operator group
        if(req.decoded.group !== 'seller' && req.decoded.group !== 'admin' && req.decoded.group !== 'operator') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        // temp 파일 업로드 삭제
        if(!req.body.files) throw "POST 데이터 에러";
        const result = await deleteUploaded(req, req.body.files);
        
        res.json({
            success : result.success,
            message : result.message
        });
    }
    catch (err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TPI-71]",
        });
    }
});

module.exports = router;