const router = require('express').Router();
const fs = require('fs');
const authMiddleware = require.main.require('./middlewares/auth');
const cipher = require.main.require('./lib/cipher');
const config = require.main.require('./config');
const sendMail = require.main.require('./lib/sendMail');
const {upload, deleteUploaded, moveFiles} = require.main.require('./lib/upload');
const {PASSWORD_REGEXP, EMAIL_REGEXP, ID_REGEXP, SYMBOL_REGEXP_REPLACE, PHONE_REGEXP, COMPANYNAME_REGEXP_MATCH, NUMBER_REGEXP, YN_REGEXP, COMPANYNUMBER_REGEXP} = require.main.require('./lib/regEx');

//관리자 리스트 조회
router.post('/adminList', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin group
        if(req.decoded.group !== 'admin') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }
        const pageSize = parseInt(req.body.pageSize) || 10;
        const pageIndex = parseInt(req.body.pageIndex) || 0;
        let searchText = req.body.searchText || false;
        //보안을 위해 특수문자 제거
        searchText = searchText ? searchText.replace(SYMBOL_REGEXP_REPLACE, "") : false;

        let query = `SELECT tb1.id, tb1.userId, tb1.email, tb1.group FROM admin_user AS tb1 ${searchText? `WHERE tb1.id LIKE '%${searchText}%' OR tb1.userId LIKE '%${searchText}%' OR tb1.email LIKE '%${searchText}%' OR tb1.group LIKE '%${searchText}%' `:''}ORDER BY id DESC LIMIT ${pageSize} OFFSET ${pageIndex * pageSize};`
        const [ queryRows, queryFields ] = await req.db.query(query);
        const list = queryRows;
        query = `SELECT COUNT(*) AS count FROM admin_user${searchText? ` AS tb1 WHERE tb1.id LIKE '%${searchText}%' OR tb1.userId LIKE '%${searchText}%' OR tb1.email LIKE '%${searchText}%' OR tb1.group LIKE '%${searchText}%'`:''};`
        
        const [ queryRows2, queryFields2 ] = await req.db.query(query);
        
        const length = queryRows2 ? queryRows2[0].count : 0;

        res.json({
            success : true,
            message : '',
            data : {
                list,
                length
            }
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TUI-11]",
        });
    }
});

//관리자 상세 내용 조회
router.post('/adminDetail', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin group
        if(req.decoded.group !== 'admin') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }
        const id = parseInt(req.body.id);
        let detail;
        const [ queryRows, queryFields ] = await req.db.query('SELECT tb1.id, tb1.userId, tb1.email, tb1.group, tb1.createdBy, tb1.cDate, tb1.updatedBy, tb1.uDate, tb1.memo FROM admin_user AS tb1 WHERE tb1.id = ?',
            [id]);
        
        if(queryRows[0])
        {
            detail = queryRows[0];
        }
        else 
        {
            return res.status(403).json({
                success: false,
                message: "ERROR [TUI-22]"
            });
        }

        res.json({
            success : true,
            message : '',
            data : {
                detail
            }
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TUI-21]",
        });
    }
});

//관리자 회원 생성 및 업데이트
router.post('/mergeAdminMember', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin group
        if(req.decoded.group !== 'admin') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }
        if(!req.body.id || !req.body.email || !req.body.group) throw "필수 입력정보를 모두 입력해주세요";
        if(!ID_REGEXP.test(req.body.id) ) throw "잘못된 아이디형식입니다";
        if(!EMAIL_REGEXP.test(req.body.email) ) throw "잘못된 메일 형식입니다";

        const password = req.body.password ? cipher.sha256(req.body.password) : null;
        if(password && !PASSWORD_REGEXP.test(req.body.password) ) throw "비밀번호는 문자, 숫자, 특수문자(!@#$%^&+=) 포함 8~15자 이내 입니다";

        let mode = req.body.mode === 'new' ? 'INSERT' : 'UPDATE';

        let [ queryRows, queryFields ] = await req.db.query('CALL USP_MERGE_ADMIN_USER(?, ?, ?, ?, ?, ?, ?);',
            [mode, req.body.id, req.body.email, password || '', req.body.group, req.body.memo, req.decoded.id]);
        if(!queryRows[0][0].result) throw queryRows[0][0].message;

        res.json({
            success : queryRows[0][0].result != 0 ? true : false,
            message : queryRows[0][0].message
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TUI-31]",
        });
    }
});

//관리자 회원 삭제
router.post('/deleteAdminMember', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin group
        if(req.decoded.group !== 'admin') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        if(req.body.id === 'superAdmin') 
        {
            return res.json({
                success: false,
                message: '최고 관리자는 삭제할 수 없습니다'
            });
        }

        if(!req.body.id) throw "필수 입력정보가 없습니다";

        let [ queryRows, queryFields ] = await req.db.query('DELETE FROM admin_user WHERE userId = ?',
            [req.body.id]);

        if(queryRows.affectedRows == 0) throw "시스템 에러 [TPI-32]";

        res.json({
            success : true,
            message : '삭제되었습니다'
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TUI-41]",
        });
    }
});

//셀러 리스트 조회
router.post('/sellerList', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin , Operator group
        if(req.decoded.group !== 'admin' && req.decoded.group !== 'operator') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        const pageSize = parseInt(req.body.pageSize) || 10;
        const pageIndex = parseInt(req.body.pageIndex) || 0;
        let sort1 = req.body.sort1;
        let sort2 = req.body.sort2;
        let searchText = req.body.searchText || false;

        //특수문자 제거
        searchText = searchText ? searchText.replace(SYMBOL_REGEXP_REPLACE, "") : false;
        if(sort2) {
            sort1 = sort1 ? 'tb1.' + sort1.replace(SYMBOL_REGEXP_REPLACE, "") : 'tb1.id';
            sort2 = sort2 == 'asc' ? 'asc' : 'desc';
        }else{
            sort1 = 'tb1.id';
            sort2 = 'desc';
        }

        let query = `SELECT tb1.id, tb1.companyName, tb1.email, tb1.cDate, tb2.isAccountCertified, tb2.isSentAccount, tb2.isSentDocument, tb2.isSellerCertified FROM seller_user AS tb1 LEFT JOIN seller_auth AS tb2 ON tb1.id = tb2.referenceId ${searchText? `WHERE tb1.id LIKE '%${searchText}%' OR tb1.companyName LIKE '%${searchText}%' OR tb1.email LIKE '%${searchText}%'`:''}ORDER BY ${sort1} ${sort2} LIMIT ${pageSize} OFFSET ${pageIndex * pageSize};`
        const [ queryRows, queryFields ] = await req.db.query(query);
        const list = queryRows;
        query = `SELECT COUNT(*) AS count FROM seller_user${searchText? ` AS tb1 WHERE tb1.id LIKE '%${searchText}%' OR tb1.companyName LIKE '%${searchText}%' OR tb1.email LIKE '%${searchText}%'`:''};`
        
        const [ queryRows2, queryFields2 ] = await req.db.query(query);
        
        const length = queryRows2 ? queryRows2[0].count : 0;

        res.json({
            success : true,
            message : '',
            data : {
                list,
                length
            }
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TUI-51]",
        });
    }
});

//셀러 사용자 상세 내용 조회
router.post('/sellerDetail', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin , Operator group
        if(req.decoded.group !== 'admin' && req.decoded.group !== 'operator') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }
        const id = parseInt(req.body.id);
        let detail;
        const [ queryRows, queryFields ] = await req.db.query('CALL USP_SELECT_SELLER_USER_DETAIL(?);',
            [id]);
        
        if(queryRows[0][0].result)
        {
            detail = queryRows[0][0];
            delete detail['result'];
            delete detail['message'];
        }
        else 
        {
            throw queryRows[0][0].message;
        }

        const [ queryRows2, queryFields2 ] = await req.db.query("SELECT id, path, originalName, mimeType, size, cDate FROM attach_file WHERE processId = 'sellerProfileAttachment' AND referenceId = ? ORDER BY cDate desc;",
            [id]);

        res.json({
            success : true,
            message : '',
            data : {
                detail,
                attachments: queryRows2
            }
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TUI-61]",
        });
    }
});

//관리자의 회원정보 업데이트 혹은 생성
router.post('/mergeSellerMember', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin , Operator group
        if(req.decoded.group !== 'admin' && req.decoded.group !== 'operator') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        // 파일 업로드 처리

        const result = await upload(req, res, {
            fieldName : "uploadFile", 
            dest : "temp", 
            processId : null, 
            referenceId : null,
            temp : true
        });

        // 여기까지 업로드 완료

        let body = req.body;

        if(!body.id) throw {message:"필수 입력정보를 모두 입력해주세요", files: result.data.list };

        if(body.companyName && body.companyName.match(COMPANYNAME_REGEXP_MATCH) != null) throw {message:"회사이름에 ()-&를 제외한 기호를 사용할 수 없습니다", files: result.data.list };
        if(body.sellerPhone && !PHONE_REGEXP.test(body.sellerPhone)) throw {message:"잘못된 전화번호 형식입니다", files: result.data.list };
        if(body.companyPhone && !PHONE_REGEXP.test(body.companyPhone)) throw {message:"잘못된 전화번호 형식입니다", files: result.data.list };
        if(body.isPhoneCertified && !YN_REGEXP.test(body.isPhoneCertified)) throw {message:"잘못된 데이터 형식입니다", files: result.data.list };
        if(body.isSellerCertified && !YN_REGEXP.test(body.isSellerCertified)) throw {message:"잘못된 데이터 형식입니다", files: result.data.list };
        if(body.isAccountCertified && !YN_REGEXP.test(body.isAccountCertified)) throw {message:"잘못된 데이터 형식입니다", files: result.data.list };
        if(body.isSentDocument && !YN_REGEXP.test(body.isSentDocument)) throw {message:"잘못된 데이터 형식입니다", files: result.data.list };
        if(body.isSentAccount && !YN_REGEXP.test(body.isSentAccount)) throw {message:"잘못된 데이터 형식입니다", files: result.data.list };
        if(body.representation && body.representation.match(SYMBOL_REGEXP_REPLACE) != null) throw {message:"대표자명에 기호를 사용할 수 없습니다", files: result.data.list };
        if(body.sector && body.sector.match(SYMBOL_REGEXP_REPLACE) != null) throw {message:"업종에 기호를 사용할 수 없습니다", files: result.data.list };
        if(body.business && body.business.match(SYMBOL_REGEXP_REPLACE) != null) throw {message:"업태에 기호를 사용할 수 없습니다", files: result.data.list };
        if(body.companyNumber && body.companyNumber.match(COMPANYNUMBER_REGEXP) === null) throw {message:"사업자 등록번호 형식을 확인해 주세요", files: result.data.list };
        if(body.corporateNumber && body.corporateNumber.match(COMPANYNUMBER_REGEXP) === null) throw {message:"법인 등록번호 형식을 확인해 주세요", files: result.data.list };
        if(body.ownerName && body.ownerName.match(SYMBOL_REGEXP_REPLACE) != null) throw {message:"예금주명에 기호를 사용할 수 없습니다", files: result.data.list };
        if(body.bankAccount && !NUMBER_REGEXP.test(body.bankAccount) ) throw {message:"계좌번호는 숫자만 입력해주세요", files: result.data.list };
        if(body.bankName && body.bankName.match(SYMBOL_REGEXP_REPLACE) != null) throw {message:"은행명에 기호를 사용할 수 없습니다", files: result.data.list };
        if(body.password && !PASSWORD_REGEXP.test(body.password) ) throw {message:"비밀번호는 문자, 숫자, 특수문자(!@#$%^&+=) 포함 8~15자 이내 입니다", files: result.data.list };
        if(body.email && !EMAIL_REGEXP.test(body.email) ) throw {message:"잘못된 메일 형식입니다", files: result.data.list };

        const password = body.password ? cipher.sha256(body.password) : null;

        let mode = body.mode === 'new' ? 'INSERT' : 'UPDATE';

        [ queryRows, queryFields ] = await req.db.query('CALL USP_MERGE_SELLER_USER(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);',
            [mode, body.companyName, body.email, password || '', body.companyPhone, body.sellerPhone, 
                body.isPhoneCertified, body.isSellerCertified, body.isAccountCertified, body.isSentAccount, body.isSentDocument, req.decoded.id, 
                body.ownerName, body.bankAccount, body.bankName, body.companyNumber, body.corporateNumber, 
                body.representation, body.sector, body.business]);
        
        if(!queryRows[0][0].result) throw {message:queryRows[0][0].message, files: result.data.list };
        const referenceId = queryRows[0][0].id;

        const files = await moveFiles(result.data.list,`seller-profile/${body.email}`);

        for(i in files){
            const file = files[i];
            [ queryRows, queryFields ] = await req.db.query('INSERT INTO attach_file (processId, referenceId, path, fileName, originalName, mimeType, size, createdBy) VALUES (?, ?, ?, ?, ?, ?, ?, ?);',
            ['sellerProfileAttachment', referenceId, file.path, file.fileName, file.originalName, file.mimeType, file.size, req.decoded.id]);
            
            if(queryRows.affectedRows == 0) throw {message:"데이터 쓰기 에러 [TPI-71]", files: files };
        }
        
        res.json({
            success : true,
            message : '저장되었습니다'
        });
    }
    catch(err)
    {
        //에러 발생시 업로드한 파일, 기록 삭제
        if(typeof err === "object" && err.files) deleteUploaded(req, err.files)

        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :typeof err === "string" ? err : "ERROR [TUI-71]",
        });
    }
});
//관리자의 회원 삭제
router.post('/deleteSellerMember', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin , Operator group
        if(req.decoded.group !== 'admin' && req.decoded.group !== 'operator') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        if(!req.body.email) throw "필수 입력정보가 없습니다";
        if(!req.body.email || !EMAIL_REGEXP.test(req.body.email) ) throw "잘못된 데이터 형식입니다";

        let [ queryRows, queryFields ] = await req.db.query('DELETE FROM seller_user WHERE email = ?',
            [req.body.email]);

        if(queryRows.affectedRows == 0) throw "시스템 에러 [TPI-82]";

        res.json({
            success : true,
            message : '삭제되었습니다'
        });
    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TUI-81]",
        });
    }
});
//관리자의 사용자 첨부파일 삭제
router.post('/deleteSellerAttachment', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin , Operator group
        if(req.decoded.group !== 'admin' && req.decoded.group !== 'operator') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }

        if(!req.body.attachId || !req.body.id) throw "필수 입력정보가 없습니다";
        if(!NUMBER_REGEXP.test(req.body.attachId) ) throw "잘못된 데이터 형식입니다";
        if(!NUMBER_REGEXP.test(req.body.id) ) throw "잘못된 데이터 형식입니다";

        let [ queryRows, queryFields ] = await req.db.query("SELECT path FROM attach_file WHERE processId='sellerProfileAttachment' AND referenceId=? AND id=?",
            [req.body.id, req.body.attachId]);
            
        if(queryRows.length == 0) throw "데이터 에러 [TPI-93]";
        const path = 'public' + queryRows[0].path;

        [ queryRows, queryFields ] = await req.db.query("DELETE FROM attach_file WHERE processId='sellerProfileAttachment' AND referenceId=? AND id=?",
            [req.body.id, req.body.attachId]);

        if(queryRows.affectedRows == 0) throw "시스템 에러 [TPI-92]";

        //파일 삭제
        try {
            fs.unlinkSync(path);
        }
        catch(err){
            //파일이 경로에 없는 경우
            throw "파일 시스템 에러 [TPI-94]";
        }

        [ queryRows, queryFields ] = await req.db.query("SELECT id, path, originalName, mimeType, size, cDate FROM attach_file WHERE processId = 'sellerProfileAttachment' AND referenceId = ? ORDER BY cDate desc;",
            [req.body.id]);

        res.json({
            success : true,
            message : '삭제되었습니다',
            data: {
                attachments : queryRows
            }
        });
    }
    catch(err)
    {
        let queryRows, queryFields;
        if(req.body.id && NUMBER_REGEXP.test(req.body.id))
        {
            [ queryRows, queryFields ] = await req.db.query("SELECT id, path, originalName, mimeType, size, cDate FROM attach_file WHERE processId = 'sellerProfileAttachment' AND referenceId = ? ORDER BY cDate desc;",
            [req.body.id]);
        }else{
            queryRows = [];
        }

        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TUI-91]",
            data: {
                attachments: queryRows
            }
        });
    }
});
//인증 완료 메일 발송
router.post('/sendmailtocustomer', authMiddleware, async (req, res, next) => {
    console.log(req.body);
    try
    {
        // Only for Admin , Operator group
        if(req.decoded.group !== 'admin' && req.decoded.group !== 'operator') 
        {
            return res.status(403).json({
                success: false,
                message: '권한이 없습니다'
            });
        }
        if(!req.body.email) throw "필수 입력 정보가 없습니다";
        if(req.body.email && !EMAIL_REGEXP.test(req.body.email) ) throw "잘못된 메일 형식입니다";
        if(req.body.isSellerCertified && !YN_REGEXP.test(req.body.isSellerCertified)) throw "잘못된 데이터 형식입니다";
        if(req.body.isAccountCertified && !YN_REGEXP.test(req.body.isAccountCertified)) throw "잘못된 데이터 형식입니다";

        let text = "";
        //이메일 발송
        if(req.body.isSellerCertified === 'Y' && req.body.isAccountCertified === 'N') text = "사업자 인증이 완료되었습니다";
        else if(req.body.isSellerCertified === 'N' && req.body.isAccountCertified === 'Y') text = "은행 계좌 인증이 완료되었습니다";
        else if(req.body.isSellerCertified === 'Y' && req.body.isAccountCertified === 'Y') text = "은행 계좌 인증 및 사업자 인증이 완료되었습니다";

        else throw "잘못된 데이터 형식입니다";
        
        let url = config.mode != 'production' ? config.ng.protocol + "://"+ config.ng.domain + ":" + config.ng.port : config.domain;
        url += "/pages/seller-profile";

        const result = await sendMail({
            to: req.body.email,
            subject: `[USM] ${text}`,
            render: 'mailLayout',
            data: {
                title : `${text}`,
                content : `안녕하세요. <b>유벤더</b> 입니다. <br>
                요청하신 ${text}. <br>
                아래 링크로 접속하셔서 서비스 사용 가능 여부를 확인해보세요. <br>
                    <div style='text-align:center; padding:20px;margin-top:12px;'>
                        <a style="border-radius:3px;
                            background-color: #039be5;
                            color:white;
                            padding:12px;
                            font-size:18px;
                            text-decoration: none;
                        " href="${url}" target="_blank"> 바로가기 </a>
                    </div>
                <br>
                <hr>
                <br>
                본 메일은 발신 전용으로 회신되지 않습니다. 추가 문의는 <a>[고객센터]</a>를 이용해주시기 바랍니다.
                <br>
                <small>Copyright(C) 유벤더 All right reserved.<small>
                `
            }
        });

        console.log(result)

        if(result.response.indexOf("250") == -1 && result.response.indexOf("OK") == -1) throw "이메일 발송 에러. 잠시 후 다시 시도해주세요";

        res.json({
            success : true,
            message : '메일을 발송하였습니다'
        });

    }
    catch(err)
    {
        res.json({
            success : false,
            message : config.mode === 'dev' ? typeof err === "object" ? err.message : err : typeof err === "string" ? err :"ERROR [TUI-81]",
        });
    }
});

module.exports = router;