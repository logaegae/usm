const router = require('express').Router();

router.all('/test', async (req, res, next) => {
    res.render('test')
});
module.exports = router;