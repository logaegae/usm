const schedule = require('node-schedule');
const request = require('request');
const models = require.main.require('./models');
const code = require.main.require('./config/code');
const Op = require('sequelize').Op;

//초(0-59) 분(0-59) 시(0-23) 일(1-31) 월(1-12) 요일(0-7)
const ststistics = schedule.scheduleJob('25 0 * * *', async () => {
     
    console.log('** every AM12 chceck schedule **');

    //오늘날짜 구하기
    const date = new Date();
    const year = date.getFullYear();
    const month = ("0"+(date.getMonth()+1)).substr(-2);
    const day = ("0"+date.getDate()).substr(-2);

    const result = year + '-' + month + '-' + day;

    console.log(result);
    console.log(new Date(result));

    // 투자 상환 알람
    let iR = await models.repaymentList.findAll({
        where : {
            delYn : 'N',
            expectedRepaymentDate : result
        }
    });
    
    if(iR && iR.length > 0){

        models.sequelize.transaction( async (t) => {

            let promises = [];
            
            for (var i = 0; i < iR.length; i++) {

                let investment = await models.investmentList.findOne({
                    where : {
                        investmentIdx : iR[i].dataValues.investmentIdx
                    },
                    include : [{
                        model : models.projectList,
                        required : true,
                        attributes : ['projectName', 'projectSubtitle']
                    },{
                        model : models.user,
                        required : true,
                        attributes : ['email', 'name']
                    }],
                    transaction : t
                });
                console.log(investment)
    
                //알람
                let item = {
                    title: investment.dataValues.projectList.dataValues.projectSubtitle + ' ' + investment.dataValues.projectList.dataValues.projectName + ' 의 투자상환 예정 알림',
                    content: '오늘 '+investment.dataValues.user.dataValues.name+'('+investment.dataValues.user.dataValues.email+')님의 투자 상환일입니다.',
                    registedDate: new Date(),
                    type: 'danger'
                };

                console.log(item)
                    
                newPromise = await models.alarm.create(item, {
                    transaction: t
                });
                
                promises.push(newPromise);
            }
            console.log(promises[0])
            return Promise.all(promises);
    
        }).then(function(result) {
    
            console.log('투자상환 알림')
    
        }).catch(function(err) {
    
            throw new Error('투자상환 알림 error');
    
        });        
    }

    // 대출 상환 알람
    let lR = await models.loanRepayment.findAll({
        where : {
            delYn : 'N',
            expectedRepaymentDate : result
        }
    });
    
    if(lR && lR.length > 0){

        models.sequelize.transaction( async (t) => {

            let promises = [];
            
            for (var i = 0; i < lR.length; i++) {

                let loanApplication = await models.loanApplication.findOne({
                    where : {
                        loanApplicationIdx : lR[i].dataValues.loanApplicationIdx
                    },
                    include : [{
                        model : models.projectList,
                        required : true,
                        attributes : ['projectName', 'projectSubtitle']
                    },{
                        model : models.user,
                        required : true,
                        attributes : ['email', 'name']
                    }],
                    transaction : t
                });
    
                //알람
                let item = {
                    title: loanApplication.dataValues.projectList.dataValues.projectSubtitle + ' ' + loanApplication.dataValues.projectList.dataValues.projectName + ' 의 대출상환 예정 알림',
                    content: '오늘 '+loanApplication.dataValues.user.dataValues.name+'('+loanApplication.dataValues.user.dataValues.email+')님의 대출 상환일입니다.',
                    registedDate: new Date(),
                    type: 'danger'
                };
                    
                newPromise = await models.alarm.create(item, {
                    transaction: t
                });
                
                promises.push(newPromise);
            }
            return Promise.all(promises);
    
        }).then(function(result) {
    
            console.log('대출상환 알림')
    
        }).catch(function(err) {
    
            throw new Error('대출상환 알림 error');
    
        });        
    }

});

module.exports = ststistics;