const schedule = require('node-schedule');
const request = require('request');
const models = require.main.require('./models');
const code = require.main.require('./config/code');
const calculate = require.main.require('./lib/calculate');
const seypert = require.main.require('./lib/seypert');
const Op = require('sequelize').Op;
const extend = require('util')._extend;

//초(0-59) 분(0-59) 시(0-23) 일(1-31) 월(1-12) 요일(0-7)
const checkInvestment = schedule.scheduleJob('*/30 * * * *', async () => {
    
    //이상 거래 있는지 없는지 검증
    console.log('** every 1h schedule **');

    //인증시간초과 검증 > 삭제
    let investment = await models.investmentList.findAll({
        where : {
            investedStatus : code.investedStatus[4].value,
            delYn : 'N',
            investedDate : {
                [Op.lt] : new Date(new Date() - 30 * 60 * 1000)
            }
        },include : [{
            model : models.user,
            required : true,
            attributes : ['name', 'email', 'userIdx']
        },{
            model : models.projectList,
            required : true,
            attributes : ['projectSubtitle', 'projectName', 'projectIdx']
        }]
    });
    console.log(investment)
    let promises = [];

    investment.forEach((e,i) => {
        let newPromise = new Promise( async (resolve, reject) => {
            let seyResult = await seypert.transactionDetail(e.dataValues.tid);
            console.log(seyResult)
            if (seyResult.result.status === 'SUCCESS') {
                //입금 승인
                if (seyResult.result.list[0].trnsctnSt === 'SFRT_TRNSFR_PND_AGRREED') {

                    let user = await models.user.findOne({
                        where : {
                            userIdx : e.dataValues.userIdx
                        }
                    })
        
                    let investJson = {
                        mobile: user.dataValues.mobile,
                        investedDate: new Date(),
                        investmentMoney: seyResult.result.list[0].payAmt,
                        investedStatus: code.investedStatus[0].value
                    }
        
                    //신청기록 업데이트
                    await models.investmentList.update(investJson, {
                        where: {
                            investmentIdx: e.dataValues.investmentIdx
                        }
                    });
    
                    //투자가능금액조정 
                    await calculate.IableA(e.dataValues.userIdx);
                    //프로젝트 기록 계산
                    await calculate.TI(e.dataValues.projectIdx);
                    //투자수익 계산
                    await calculate.expectedIncome(e.dataValues.investmentIdx, seyResult.result.list[0].payAmt);
    
                    //알람
                    await models.alarm.create({
                        title: '투자신청',
                        content: e.dataValues.email + '님이 투자신청을 하였습니다.',
                        registedDate: new Date(),
                        type: 'info'
                    });
    
                    //투자 신청 확인 메일 발송
                    const setting = await models.config.findById(1);
                    if (setting.mailOption.option3 != null) {
    
                        const mail = await models.mail.findOne({
                            where: {
                                mailIdx: setting.mailOption.option3,
                                delYn: 'N'
                            }
                        });
    
                        if (mail) {
                            mail.content = mail.content
                                .replace(/\{회원아이디\}/g, e.dataValues.user.dataValues.email)
                                .replace(/\{이름\}/g, e.dataValues.user.dataValues.name)
                                .replace(/\{투자상품명\}/g, e.dataValues.projectList.dataValues.projectSubtitle + ' ' + e.dataValues.projectList.dataValues.projectName);
    
                            sendMail({
                                to: e.dataValues.user.dataValues.email,
                                subject: mail.title,
                                render: 'mailing_layout',
                                data: {
                                    title: mail.title,
                                    content: mail.content
                                }
                            });
                        }
                    }
        
                    //투자 신청 확정 SMS 발송
                    if (setting.smsOption.option4 != null && e.dataValues.user.dataValues.mobile) {
    
                        let sms = await models.sms.findOne({
                            where: {
                                smsIdx: setting.smsOption.option4
                            }
                        });
                        if (sms) {
                            sms.content = sms.dataValues.content
                                .replace(/\{회원아이디\}/g, e.dataValues.user.dataValues.email)
                                .replace(/\{이름\}/g, e.dataValues.user.dataValues.name)
                                .replace(/\{투자상품명\}/g, e.dataValues.projectList.dataValues.projectSubtitle + ' ' + e.dataValues.projectList.dataValues.projectName);
    
                            await sendSMS(sms.content, e.dataValues.user.dataValues.mobile);
                        }
                    }
                    console.log("검증 완료");
                    resolve();
                }
            }
        });
        promises.push(newPromise);
    });

    Promise.all(promises).then( async ()=>{

        let investment2 = await models.investmentList.findAll({
            where : {
                investedStatus : code.investedStatus[4].value,
                delYn : 'N',
                investedDate : {
                    [Op.lt] : new Date(new Date() - 30 * 60 * 1000)
                }
            }
        });

        if( investment2 && investment2.length > 0) {
            models.sequelize.transaction( async (t) => {
    
                let promises2 = [];
    
                investment2.forEach((e,i) => {
                    
                    let newPromise2 = models.investmentList.update({
                        delYn : 'Y'
                    },{
                        where : {
                            investmentIdx : e.dataValues.investmentIdx
                        },
                        transaction: t
                    })
    
                    promises2.push(newPromise2);
                });
    
                return Promise.all(promises2);
    
            }).then(function(result) {
    
                console.log('investment list checked 1');
    
            }).catch(function(err) {
                console.log('investment list check error');
    
            });
        }
    });
});

module.exports = checkInvestment;