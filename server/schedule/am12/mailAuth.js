const schedule = require('node-schedule');
const models = require.main.require('./models');
const Op = require('sequelize').Op;
const fs = require('fs');

//초(0-59) 분(0-59) 시(0-23) 일(1-31) 월(1-12) 요일(0-7)
const am12 = schedule.scheduleJob('5 0 * * *', async () => {
     
     console.log('** every AM2 mailAuth clear schedule **');

    //clear mailAuth List
    models.mailAuth.destroy({
        where: {
            expireDate : {
                [Op.lt] : new Date()
            }
        }
    });
    
});

module.exports = am12;