const schedule = require('node-schedule');
const request = require('request');
const models = require.main.require('./models');
const code = require.main.require('./config/code');
const Op = require('sequelize').Op;

//초(0-59) 분(0-59) 시(0-23) 일(1-31) 월(1-12) 요일(0-7)
const projectListCheck = schedule.scheduleJob('0 0 * * *', async () => {
     
    console.log('** every AM12 project list chceck schedule **');
    //통계

    //모집후 3일간 new 표시
    let targetDate = new Date();
    let b = targetDate.getDate();
    targetDate.setDate(b-3);
    let projectList = await models.projectList.findAll({
        where : {
            delYn : 'N',
            showYn : 'Y',
            recruitStartDate : {
                [Op.lt] : targetDate
            }
        }
    });

    if( projectList && projectList.length > 0) {
        models.sequelize.transaction( async (t) => {

            let promises = [];

            projectList.forEach((e,i) => {

                let sticker = e.dataValues.sticker;
                sticker[0] = null;

                newPromise = models.projectList.update({
                    sticker
                },{
                    where : {
                        projectIdx : e.dataValues.projectIdx
                    },
                    transaction: t
                })

                promises.push(newPromise);
            });

            return Promise.all(promises);

        }).then(function(result) {

            console.log('project list checked for sticker');

        }).catch(function(err) {

            console.log('project list check error');

        });
    }

    //모집 실패시
    let projectList2 = await models.projectList.findAll({
        where : {
            delYn : 'N',
            showYn : 'Y',
            recruitEndDate : {
                [Op.lt] : new Date()
            },
            projectStatus : code.projectStatus[1].value
        }
    });

    if( projectList2 && projectList2.length > 0) {
        models.sequelize.transaction( async (t) => {

            let promises = [];

            projectList2.forEach((e,i) => {

                newPromise = models.projectList.update({
                    projectStatus : code.projectStatus[6].value
                },{
                    where : {
                        projectIdx : e.dataValues.projectIdx
                    },
                    transaction: t
                });

                promises.push(newPromise);

                newPromise = models.alarm.create({
                    title : '투자상품 상태 변경',
                    content : e.dataValues.projectSubtitle +' '+ e.dataValues.projectName + ' 의 투자모집이 실패하였습니다.',
                    registedDate : new Date()
                });

                promises.push(newPromise);
            });

            return Promise.all(promises);

        }).then(function(result) {

            console.log('project list checked for recruit failure');

        }).catch(function(err) {

            console.log('project list check error');

        });
    }
    
    //상환중인 프로젝트 기간 경과시 상환완료 간주
    // let projectList5 = await models.projectList.findAll({
    //     where : {
    //         delYn : 'N',
    //         showYn : 'Y',
    //         endDate : {
    //             [Op.lt] : new Date()
    //         },
    //         projectStatus : code.projectStatus[3].value
    //     }
    // });

    // if( projectList5 && projectList5.length > 0) {
    //     models.sequelize.transaction( async (t) => {

    //         let promises = [];

    //         projectList5.forEach((e,i) => {

    //             newPromise = models.projectList.update({
    //                 projectStatus : code.projectStatus[4].value
    //             },{
    //                 where : {
    //                     projectIdx : e.dataValues.projectIdx
    //                 },
    //                 transaction: t
    //             })

    //             promises.push(newPromise);

    //             newPromise =  models.alarm.create({
    //                 title : '투자상품 상태 변경',
    //                 content : e.dataValues.projectSubtitle +' '+ e.dataValues.projectName + ' 의 투자기간이 경과하였습니다.',
    //                 registedDate : new Date()
    //             });

    //             promises.push(newPromise);
    //         });

    //         return Promise.all(promises);

    //     }).then(function(result) {

    //         console.log('project list checked for complete');

    //     }).catch(function(err) {

    //         console.log('project list check error');

    //     });
    // }

    //모집중인 프로젝트 목표금액 모집시 모집마감 전환
    let projectList6 = await models.projectList.findAll({
        where : {
            delYn : 'N',
            showYn : 'Y',
            recruitEndDate : {
                [Op.gte] : new Date()
            },
            projectStatus : code.projectStatus[1].value
        }
    });

    if( projectList6 && projectList6.length > 0) {

        models.sequelize.transaction( async (t) => {

            let promises = [];

            projectList6.forEach((e,i) => {
                
                if(e.dataValues.targetMoney <= e.dataValues.totalInvest || e.dataValues.targetMoney - e.dataValues.totalInvest < e.dataValues.minInvest){

                    newPromise = models.projectList.update({
                        projectStatus : code.projectStatus[2].value
                    },{
                        where : {
                            projectIdx : e.dataValues.projectIdx
                        },
                        transaction: t
                    });

                    promises.push(newPromise);

                    newPromise = models.alarm.create({
                        title : '투자상품 상태 변경',
                        content : e.dataValues.projectSubtitle +' '+ e.dataValues.projectName + ' 의 모집이 마감되었습니다.',
                        registedDate : new Date()
                    });
    
                    promises.push(newPromise);
                }
            });

            return Promise.all(promises);

        }).then(function(result) {

            console.log('project list checked for application complete');

        }).catch(function(err) {

            console.log('project list check error');

        });
    }

    //한도 계산

});

module.exports = projectListCheck;