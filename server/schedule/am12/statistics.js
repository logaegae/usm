const schedule = require('node-schedule');
const request = require('request');
const models = require.main.require('./models');
const code = require.main.require('./config/code');
const Op = require('sequelize').Op;

//초(0-59) 분(0-59) 시(0-23) 일(1-31) 월(1-12) 요일(0-7)
const ststistics = schedule.scheduleJob('10 0 * * *', async () => {
     
    console.log('** every AM12 ststistics chceck schedule **');
    //통계

    let setting = await models.config.findOne({
        where: {
            configIdx : 1
        }
    });

    let projectList = await models.projectList.findAndCountAll({
        where : {
            [Op.or] : [{
                projectStatus : code.projectStatus[3].value
            },{
                projectStatus : code.projectStatus[4].value
            }]
        },
        attributes : ['targetMoney', 'earningRate']
    });

    let projectList2 = await models.projectList.findAndCountAll({
        where : {
            projectStatus : code.projectStatus[3].value
        },
        attributes : ['targetMoney']
    });
    //누적 대출금
    
    if(setting.dataValues.ALUpdateYn === 'Y'){
        let amount = 0;

        projectList.rows.forEach((e,i) => {
            amount += e.dataValues.targetMoney;
        });
        setting.accumulatedLoan = amount;
        setting.save();
    }
    //잔여 대출금
    if(setting.dataValues.LBUpdateYn === 'Y'){
        let amount = 0;
        
        projectList2.rows.forEach((e,i) => {
            amount += e.dataValues.targetMoney;
        });

        setting.loanBalance = amount;
        setting.save();
    }
    //평균 수익률
    if(setting.dataValues.AIRUpdateYn === 'Y'){
        let amount = 0;

        projectList.rows.forEach((e,i) => {
            amount += e.dataValues.earningRate;
        });
        setting.averageIncomeRate = parseInt(amount / projectList.count);
        setting.save();
    }
});

module.exports = ststistics;