const schedule = require('node-schedule');
const request = require('request');
const models = require.main.require('./models');
const code = require.main.require('./config/code');
const Op = require('sequelize').Op;
const extend = require('util')._extend;

//초(0-59) 분(0-59) 시(0-23) 일(1-31) 월(1-12) 요일(0-7)
const user = schedule.scheduleJob('20 0 * * *', async () => {
     
    console.log('** every AM1 user check schedule **');

    //휴면계정 전환
    const user = await models.user.findAll({
        where : {
            loginDate : {
                [Op.lt] : new Date(new Date() - 365 * 12 * 60 * 60 * 1000)
            },
            dormantYn : 'N'
        }
    });

    if(user.length != 0){

        models.sequelize.transaction( (t) => {

            let promises = [];

            for (var i = 0; i < user.length; i++) {

                let newPromise;
                const obj = extend({}, user[i].dataValues);
                const userIdx = obj.userIdx;
                const email = obj.email;
                const pwd = obj.pwd;
                delete obj.dormantYn;
                obj.dormantedDate = new Date();

                newPromise = models.dormantUser.create(obj, {transaction: t});
                promises.push(newPromise);

                newPromise = models.user.update({
                    dormantYn : 'Y',
                    dormantedDate : obj.dormantedDate,
                    name : null,
                    mobile : null,
                    idConfirm : null,
                    di : null,
                    nationalInfo : null,
                    postNo : null,
                    basicAddress : null,
                    detailAddress : null,
                    agreeEmail : null,
                    agreeSms : null,
                    companyName : null,
                    presidentName : null,
                    businessNo : null,
                    corporateNo : null,
                    businessTypeConfirmYn : null,
                    loanCompanyYn : null,
                    loanCompanyConfirm : null,
                    businessType : null,
                    investorType : null,
                    totalInvestableAmount : null,
                    projectInvestableAmount :null,
                    remainInvestableAmount : null,
                    accessIp : null,
                    registedDate :null,
                    birthday :null,
                    sex : null,
                },
                {
                    where : {
                        userIdx
                    },
                    transaction: t
                });

                promises.push(newPromise);

            }
            return Promise.all(promises);

        }).then(function (result) {

            console.log("dormant user check completed");

        }).catch(function (err) {
            console.log(err)
            console.log("dormant user check error");
        });
    }
    
    //휴면계정 삭제 ?

    //탈퇴회원 자료 이전

    //비밀번호 6개월 변경
     
});

module.exports = user;