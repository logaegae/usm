const schedule = require('node-schedule');
const models = require.main.require('./models');
const Op = require('sequelize').Op;

//초(0-59) 분(0-59) 시(0-23) 일(1-31) 월(1-12) 요일(0-7)
const am12 = schedule.scheduleJob('58 * * * *', async () => {
     
     console.log('** every 1h schedule **'); 

     //clear NICE result : 유효기간 1시간
    models.niceResult.destroy({
        where: {
            datetime : {
                [Op.lt] : new Date(new Date() - 10 * 60 * 1000)
            }
        }
    });
});

module.exports = am12;