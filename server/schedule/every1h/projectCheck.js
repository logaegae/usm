const schedule = require('node-schedule');
const request = require('request');
const models = require.main.require('./models');
const code = require.main.require('./config/code');
const Op = require('sequelize').Op;

//초(0-59) 분(0-59) 시(0-23) 일(1-31) 월(1-12) 요일(0-7)
const projectListCheck = schedule.scheduleJob('0 * * * *', async () => {
     
    console.log('** every 1 hour project list chceck schedule **');
    const current = new Date();
    //투자대기중인 프로젝트 투자모집 전환
    let projectList3 = await models.projectList.findAll({
        where : {
            delYn : 'N',
            showYn : 'Y',
            recruitStartDate : {
                [Op.lte] : current
            },
            recruitStartTime : {
                [Op.lte] : current.getHours()
            },
            projectStatus : code.projectStatus[0].value
        }
    });

    if( projectList3 && projectList3.length > 0) {
        models.sequelize.transaction( async (t) => {

            let promises = [];

            projectList3.forEach((e,i) => {

                newPromise = models.projectList.update({
                    projectStatus : code.projectStatus[1].value
                },{
                    where : {
                        projectIdx : e.dataValues.projectIdx
                    },
                    transaction: t
                })

                promises.push(newPromise);

                newPromise = models.alarm.create({
                    title : '투자상품 상태 변경',
                    content : e.dataValues.projectSubtitle +' '+ e.dataValues.projectName + ' 의 투자모집을 시작합니다.',
                    registedDate : new Date()
                });

                promises.push(newPromise);
            });

            return Promise.all(promises);

        }).then(function(result) {

            console.log('project list checked for recruit');

        }).catch(function(err) {

            console.log('project list check error');

        });
    }

});

module.exports = projectListCheck;