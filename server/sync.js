var db = require.main.require('./config').db;
var SequelizeAuto = require('sequelize-auto');

var auto = new SequelizeAuto(db.database, db.username, db.password, {
  host: db.host,
  dialect: 'mysql',
  directory: './models/scheme'
});

auto.run(function (err) {
  if (err) throw err;
  console.log(auto.tables); // table list
  console.log(auto.foreignKeys); // foreign key list
});