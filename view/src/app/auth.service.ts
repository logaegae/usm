import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

import { config } from 'app/server-config';

@Injectable()
export class AuthService implements Resolve<any>
{
    routeParams: any;
    info: any;
    TOKEN_NAME: string = "USMJWT";
    TOKEN_INFO_NAME: string = "USMJWTI";
    infoUpdated: EventEmitter<any> = new EventEmitter();

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        
    }

    authToken(token: string): Observable<any>
    {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': token
            })
        }
        return this._httpClient.get<any>(config.api.domain + 'auth/tokenVerification', httpOptions);
    }

    getToken(): string
    {
        return localStorage.getItem(this.TOKEN_NAME);
    }

    setToken(token: string): void
    {
        localStorage.setItem(this.TOKEN_NAME, token);
    }

    removeToken(): void
    {
        localStorage.removeItem(this.TOKEN_NAME);
        this.info = {};
        this.infoUpdated.emit(this.info);
    }

    setInfo(info: any): void
    {
        this.info = info;
        this.infoUpdated.emit(this.info);
    }
    getInfo(): any
    {
        return this.info;
    }
    
}