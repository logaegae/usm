import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { Product } from 'app/main/apps/product/detail/detail.model';
import { ProductDetailService } from 'app/main/apps/product/detail/detail.service';
import { config } from 'app/server-config';

@Component({
    selector     : 'product-detail',
    templateUrl  : './detail.component.html',
    styleUrls    : ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProductDetailComponent implements OnInit, OnDestroy
{
    staticDomain: string = config.static.domain;
    product: Product;
    pageType: string;
    productForm: FormGroup;
    needAttach: boolean = true;
    needAttach2: boolean = true;
    needInfo: boolean = true;
    uploadingDetailImage: boolean = false;
    uploadingImage: boolean = false;
    loadingNotice: boolean = false;
    deletingImage: boolean = false;
    noticeList: any;
    noticeInfo: any;
    
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProductDetailService} _ProductDetailService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     */
    constructor(
        private _ProductDetailService: ProductDetailService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _matSnackBar: MatSnackBar
    )
    {
        // Set the default
        this.product = new Product();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Subscribe to update product on changes
        this._ProductDetailService.onProductChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(product => {

                if ( product )
                {
                    this.product = new Product(product);
                    this.noticeInfo = this.product.noticeInfo;
                    this.pageType = 'edit';
                }
                else
                {
                    this.pageType = 'new';
                    this.product = new Product();
                }
                console.log(this.product)
                this.productForm = this.createProductForm();
            });
        this._ProductDetailService.getNoticeGroup()
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(response => {
                if(response.success) this.noticeList = response.data;
                else alert(response.message)
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create product form
     *
     * @returns {FormGroup}
     */
    createProductForm(): FormGroup
    {
        return this._formBuilder.group({
            id                  : [this.product.id],
            title               : [this.product.title],
            info                : [this.product.info],
            comment             : [this.product.comment],
            images              : [this.product.images],
            detailImages        : [this.product.detailImages],
            price               : [this.product.price],
            modelName           : [this.product.modelName],
            madein              : [this.product.madein],
            gifts               : [this.product.gifts],
            stock               : [this.product.stock],
            maximumAllow        : [this.product.maximumAllow],
            sellerDiscount      : [this.product.sellerDiscount],
            optionList          : [this.product.optionList],
            needCertifiedProduct: [this.product.needCertifiedProduct],
            medicalHealthProduct: [this.product.medicalHealthProduct],
            safetyCertiInfo     : [this.product.safetyCertiInfo],
            noticeId            : [this.product.noticeId]
        });
    }

    onClickAttachBtn(attachInput: any): void
    {
        attachInput.click();
    }
    fileChange(event: any, type: string): void
    {
        let fileList: FileList = event.target.files;
        if(fileList.length > 0) {
            
            let formData:FormData = new FormData();
            let sizeAlert = false;
            let limit = 6;
            
            // Maximun upload file = 6
            if(type === 'detail'){
                limit = 6 - this.product.detailImages.length;
                this.uploadingDetailImage = true;
            }else{
                limit = 6 - this.product.images.length;
                this.uploadingImage = true;
            }

            limit = limit < fileList.length ? limit : fileList.length;
            for(let i=0; i<limit; i++){
                //10Mb이하만 업로드
                if(fileList[i].size < 10 * 1024 * 1024) formData.append('uploadFile', fileList[i], fileList[i].name);
                else sizeAlert = true;
            }
            
            if(type === 'detail'){
                this.needAttach2 = false;
            }else{
                this.needAttach = false;
            }

            this._ProductDetailService.tempProductImage(formData)
                .pipe(
                    take(1)
                )
                .subscribe((response) => {

                    if(type === 'detail'){
                        this.uploadingDetailImage = false;
                    }else{
                        this.uploadingImage = false;
                    }
                    
                    if(response.success) {
                        if(type === 'detail') this.product.detailImages = this.product.detailImages.concat(response.data.list);
                        else this.product.images = this.product.images.concat(response.data.list);
                    }

                    if(sizeAlert) alert("10Mb 이상의 파일은 업로드 할 수 없습니다");
                },
                error => {
                    this.uploadingImage = false;
                    console.log("Error");
                    console.log(error);
                    alert(error);
                });
        }        
        
    }

    /**
     * Save product
     */
    saveProduct(): void
    {
        const data = this.productForm.getRawValue();
        data.images = this.product.images;
        data.detailImages = this.product.detailImages;
        data.noticeInfo = this.noticeInfo;

        this._ProductDetailService.saveProduct(data)
            .then(() => {

                // Trigger the subscription with new data
                this._ProductDetailService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Product saved', 'OK', {
                    verticalPosition: 'top',
                    duration        : 2000
                });
                // Change the location with new one
                this._location.go('apps/product/management');
            });
    }

    /**
     * Add product
     */
    addProduct(): void
    {
        const data = this.productForm.getRawValue();
        data.images = this.product.images;
        data.detailImages = this.product.detailImages;
        data.noticeInfo = this.noticeInfo;

        console.log(data)

        this._ProductDetailService.addProduct(data)
            .then(() => {

                // Trigger the subscription with new data
                //this._ProductDetailService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Product added', 'OK', {
                    verticalPosition: 'top',
                    duration        : 2000
                });
                // Change the location with new one
                this._location.go('apps/product/management');
            });
    }
    // Btns
    onClickDeleteBtn(index: number, type: string): void
    {
        let conDel = false;
        if(this.pageType == 'edit') conDel = confirm("삭제시 해당 이미지는 복구되지 않습니다.\n삭제를 진행하시겠습니까?");
        if(!conDel) return;

        let target;
        if(type === 'detail') target = this.product.detailImages;
        else target = this.product.images;

        if(target[index] && this.deletingImage == false){
            this.deletingImage = true;
            this._ProductDetailService.deleteUploadedImage({files :[target[index]]})
                .pipe(
                    take(1)
                )
                .subscribe((response) => {

                    this.deletingImage = false;
                    if(response.success) target.splice(index,1);
                },
                error => {
                    this.deletingImage = false;
                    console.log("Error");
                    console.log(error);
                    alert(error);
                });
        }
    }
    onClickNextBtn(index: number, type: string): void
    {
        if(index <= this.product.images.length - 2){
            let target;
            if(type === 'detail') target = this.product.detailImages;
            else target = this.product.images;

            let tmp = target[index];
            target[index] = target[index+1];
            target[index+1] = tmp;
        }
    }
    onClickPrevBtn(index: number, type: string): void
    {
        if(index > 0){
            let target;
            if(type === 'detail') target = this.product.detailImages;
            else target = this.product.images;

            let tmp = target[index];
            target[index] = target[index-1];
            target[index-1] = tmp;
        }
    }
    optionChange(e: any): void
    {
        if(e.checked) this.productForm.get('stock').setValue(null);
        this.product.optionList = [{
            option1: null,
            option2: null,
            stock: null
        }];
    }
    maximumOptionChange(e: any): void
    {
        if(!e.checked) this.productForm.get('maximumAllow').setValue(null);
    }
    sellerDiscountChange(e: any): void
    {
        if(!e.checked) {
            this.productForm.value.sellerDiscount.checked = null;
            this.productForm.value.sellerDiscount.type = null;
            this.productForm.value.sellerDiscount.value = null;
        }else{
            this.productForm.value.sellerDiscount.checked = true;
        }
    }
    modelNameChange(e: any): void
    {
        if(!e.checked) this.productForm.get('modelName').setValue('');
    }
    optionAdd(index: number): void
    {
        this.product.optionList.splice(index + 1, 0, {
            option1: null,
            option2: null,
            stock: null
        });
    }
    optionDelete(index: number): void
    {
        this.product.optionList.splice(index,1);
    }
    checkSellerDiscount(): void
    {
        if(!this.productForm.value.price) {
            this.productForm.value.sellerDiscount.value = null;
            alert("판매가격을 먼저 입력해주세요");
        }
        else if(this.productForm.value.sellerDiscount.type == 1 && this.productForm.value.sellerDiscount.value > 70){
            this.productForm.value.sellerDiscount.value = 70;
            alert("70% 이상 할인할 수 없습니다");
        }
        else if(this.productForm.value.sellerDiscount.type == 2){
            if(this.productForm.value.sellerDiscount.type > 10) this.productForm.value.sellerDiscount.value = Math.floor(this.productForm.value.sellerDiscount.value/10) * 10;
            if(this.productForm.value.price * 0.7 < this.productForm.value.sellerDiscount.value) {
                this.productForm.value.sellerDiscount.value = Math.floor(Math.round(this.productForm.value.price * 0.7)/10) * 10;
                alert("판매가격의 70% 이상을 할인할 수 없습니다");
            }
        }
    }
    checkSellerDiscount2(): void
    {
        this.productForm.value.sellerDiscount.value = Math.floor(this.productForm.value.sellerDiscount.value/10) * 10;
        if(this.productForm.value.sellerDiscount.type == 2){
            if(this.productForm.value.sellerDiscount.value < 100 ){
                this.productForm.value.sellerDiscount.value = null;
                alert("최소 100원 이상 입력해주세요");
            }
        }
    }
    changeSellerDiscountType(): void
    {
        this.productForm.value.sellerDiscount.value = null;
    }
    selectNotice(value: number): void
    {
        this.loadingNotice = true;
        this._ProductDetailService.getNoticeInfo({noticeItemGroupNo: value})
            .pipe(
                take(1)
            )
            .subscribe((response) => {
                this.loadingNotice = false;
                if(response.success) this.noticeInfo = JSON.parse(response.data);
            },
            error => {
                this.loadingNotice = false;
                console.log("Error");
                console.log(error);
                alert(error);
            });
    }
    checkInfoValid(): void
    {
        let needed = true;
        for(let i=0; i<this.noticeInfo.length; i++){
            if (this.noticeInfo[i].IsEssencial && !this.noticeInfo[i].value) {
                needed = false;
                break;
            }
        }
        this.needInfo = needed;
    }
}
