import { MatChipInputEvent } from '@angular/material';

import { FuseUtils } from '@fuse/utils';

export class Product
{
    id: number;
    title: string;
    info: string;
    comment: string;
    memo: string;
    images: {
        id: number,
        path: string,
        mimeType: string,
        size: number,
        originalName: string,
        fileName: string;
    }[];
    detailImages: {
        id: number,
        path: string,
        mimeType: string,
        size: number,
        originalName: string,
        fileName: string;
    }[];
    price: number;
    optionList: {
        option1: string,
        option2: string,
        stock: number
    }[];
    modelName : string;
    madein: string;
    gifts: string;
    stock: number;
    maximumAllow: number;
    sellerDiscount:{
        checked: boolean,
        type: number,
        value: number
    };
    needCertifiedProduct: boolean;
    medicalHealthProduct: boolean;
    safetyCertiInfo: boolean;
    noticeId: number;
    noticeInfo: any;

    /**
     * Constructor
     *
     * @param product
     */
    constructor(product?)
    {
        product = product || {};
        this.id = product.id || null;
        this.title = product.title || '';
        this.info = product.info || '';
        this.comment = product.comment || '';
        this.images = product.images || [];
        this.detailImages = product.detailImages || [];
        this.price = product.price || null;
        this.optionList = product.optionList || [];
        this.modelName = product.modelName || '';
        this.madein = product.madein || '';
        this.gifts = product.gifts || '';
        this.stock = product.stock || null;
        this.maximumAllow = product.maximumAllow || null;
        this.sellerDiscount = product.sellerDiscount || {
            checked: null,
            type: null,
            value: null
        },
        this.needCertifiedProduct = product.needCertifiedProduct || false;
        this.medicalHealthProduct = product.medicalHealthProduct || false;
        this.safetyCertiInfo = product.safetyCertiInfo || false;
        this.noticeId = product.noticeId || null;
        this.noticeInfo = product.noticeInfo || [];
    }
}