import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { config } from '../../../../server-config'

@Injectable()
export class ProductDetailService implements Resolve<any>
{
    routeParams: any;
    product: any;
    onProductChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
        // Set the defaults
        this.onProductChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProduct()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get product
     *
     * @returns {Promise<any>}
     */
    getProduct(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onProductChanged.next(false);
                resolve(false);
            }
            else
            {
                this._httpClient.post(config.api.domain + 'product/detail/' + this.routeParams.id, {})
                    .subscribe((response: any) => {
                        this.product = response.data;
                        this.product.medicalHealthProduct = !!this.product.medicalHealthProduct;
                        this.product.needCertifiedProduct = !!this.product.needCertifiedProduct;
                        this.product.safetyCertiInfo = !!this.product.safetyCertiInfo;
                        this.product.noticeInfo = JSON.parse(this.product.noticeInfo);
                        this.product.sellerDiscount = JSON.parse(this.product.sellerDiscount);
                        this.product.optionList = JSON.parse(this.product.optionList);
                        this.onProductChanged.next(this.product);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save product
     *
     * @param product
     * @returns {Promise<any>}
     */
    saveProduct(product): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._httpClient.post(config.api.domain + 'product/saveProduct', product)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add product
     *
     * @param product
     * @returns {Promise<any>}
     */
    addProduct(product): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._httpClient.post(config.api.domain + 'product/addProduct', product)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
    tempProductImage(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'upload/tempProductImage', params);
    }
    deleteUploadedImage(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'upload/deleteUploadedImage', params);
    }
    getNoticeGroup(): Observable<any>
    {
        return this._httpClient.get<any>(config.api.domain + 'product/getNoticeItemList');
    }
    getNoticeInfo(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'product/getNoticeInfo', params);
    }
}
