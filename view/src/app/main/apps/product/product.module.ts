import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatChipsModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSnackBarModule,
    MatSortModule,
    MatTableModule, MatTabsModule, MatProgressSpinnerModule, MatDividerModule, MatCheckboxModule, MatRadioModule
} from '@angular/material';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';

import { ProductManagementComponent } from 'app/main/apps/product/management/management.component';
import { ProductManagementService } from 'app/main/apps/product/management/management.service';
import { ProductDetailComponent } from 'app/main/apps/product/detail/detail.component';
import { ProductDetailService } from 'app/main/apps/product/detail/detail.service';
import { NewOrdersComponent } from 'app/main/apps/product/new-orders/new-orders.component';
import { NewOrdersService } from 'app/main/apps/product/new-orders/new-orders.service';
import { PackingComponent } from 'app/main/apps/product/packing/packing.component';
import { PackingService } from 'app/main/apps/product/packing/packing.service';
import { DeliveryComponent } from 'app/main/apps/product/delivery/delivery.component';
import { DeliveryService } from 'app/main/apps/product/delivery/delivery.service';
import { CompleteComponent } from 'app/main/apps/product/complete/complete.component';
import { CompleteService } from 'app/main/apps/product/complete/complete.service';

const routes: Routes = [
    {
        path     : 'management',
        component: ProductManagementComponent,
        resolve  : {
            data: ProductManagementService
        }
    },
    {
        path     : 'detail/:id',
        component: ProductDetailComponent,
        resolve  : {
            data: ProductDetailService
        }
    },
    {
        path     : 'detail/:id/:handle',
        component: ProductDetailComponent,
        resolve  : {
            data: ProductDetailService
        }
    },
    {
        path     : 'new-order',
        component: NewOrdersComponent,
        resolve  : {
            data: NewOrdersService
        }
    },
    {
        path     : 'packing',
        component: PackingComponent,
        resolve  : {
            data: PackingService
        }
    },
    {
        path     : 'delivery',
        component: DeliveryComponent,
        resolve  : {
            data: DeliveryService
        }
    },
    {
        path     : 'complete',
        component: CompleteComponent,
        resolve  : {
            data: CompleteService
        }
    }
];

@NgModule({
    declarations: [
        ProductManagementComponent,
        ProductDetailComponent,
        NewOrdersComponent,
        PackingComponent,
        DeliveryComponent,
        CompleteComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatProgressSpinnerModule,
        MatDividerModule,
        MatCheckboxModule,
        MatRadioModule,

        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule
    ],
    providers   : [
        ProductManagementService,
        ProductDetailService,
        NewOrdersService,
        PackingService,
        DeliveryService,
        CompleteService
    ]
})
export class ProductModule
{
}
