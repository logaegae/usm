import {Component, OnInit, ViewChild} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {MatPaginator, MatTableDataSource, MatSort, PageEvent} from '@angular/material';
import { AdminMembersService } from './admin-members.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const path = "pages/admin-members";

export interface AdminUserElement {
    id: number;
    userId: string;
    email: string;
    group: string;
}

@Component({
    selector: 'admin-members',
    styleUrls: ['admin-members.component.scss'],
    templateUrl: 'admin-members.component.html'
})
export class AdminMembersComponent {

    displayedColumns: string[] = ['userId', 'email', 'group'];
    dataSource: AdminUserElement[];
    // MatPaginator Inputs
    length: number;
    pageSize: number;
    pageSizeOptions: number[];
    pageIndex: number;
    // MatPaginator Output
    pageEvent: PageEvent;
    searchText: string;
    reloading: boolean;
    listLoading: boolean;

    private _unsubscribeAll: Subject<any>;


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private _adminMembersService: AdminMembersService
    ) {

        // Set the private defaults
        this._unsubscribeAll = new Subject();
        
        this.pageIndex = 0;
        this.length = 0;
        this.pageSize = 10;
        this.pageSizeOptions = [10, 25, 100];
        this.reloading = false;
        this.listLoading = true;
    }

    ngOnInit() {
        this.route
            .queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => {
                this.pageSize = params.size || 10;
                this.pageIndex = params.index;
                this.searchText = params.search;
                this.getList();
        });
    }

    getList() {
        this.reloading = true;
        this._adminMembersService.getAdminList({
            pageIndex: this.pageIndex, 
            pageSize: this.pageSize, 
            searchText: this.searchText
        })
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((response) => {
                if(response.success) 
                {
                    this.dataSource = response.data.list;
                    this.length = response.data.length;
                }
                else
                {
                    this.dataSource = [];
                    this.length = 0;
                }
                this.reloading = false;
                this.listLoading = false;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onPageEvent(e: any): void
    {
        this.router.navigate([path], { queryParams: {size: e.pageSize, index: e.pageIndex, search: this.searchText}});
    }

    getRecord(row: AdminUserElement)
    {
        this.router.navigate([path+"/detail/" + row.id], { queryParams: {size: this.pageSize, index: this.pageIndex, search: this.searchText}});
    }

    onKeyAtSearch(event: any) 
    {
        const SYMBOL_REGEX = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;

        if(event.keyCode == 13)
        {
            if(event.target.value.length > 2) 
            {
                if(!SYMBOL_REGEX.test(event.target.value))
                {
                    this.searchText = event.target.value;
                    this.router.navigate([path], { queryParams: {size: this.pageSize, index: 0, search: this.searchText}});
                }
                else
                {
                    alert("특수문자를 입력할 수 없습니다");
                }
            }
            else
            {
                alert("검색을 위해 두 글자 이상 입력해주세요");
            }
        }
    }

    onClickSearch(): void 
    {
        this.router.navigate([path], { queryParams: {size: this.pageSize, index: 0, search: this.searchText}});
    }

    add(): void 
    {
        this.router.navigate([path+"/detail/new"], { queryParams: {size: this.pageSize, index: this.pageIndex, search: this.searchText}});
    }
}
