import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatTableModule, MatProgressSpinnerModule, MatPaginatorModule, MatIconModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatButtonModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { AdminMembersService } from 'app/main/pages/admin-members/admin-members.service';
import { AdminMembersComponent } from 'app/main/pages/admin-members/admin-members.component';
import { DetailComponent } from 'app/main/pages/admin-members/detail/detail.component';
import { AdminAuthGuard } from 'app/admin-auth.guard';

const routes = [
    {
        path     : 'admin-members',
        component: AdminMembersComponent,
        resolve  : {
            members: AdminMembersService
        },
        canActivate: [AdminAuthGuard]
    },
    {
        path     : 'admin-members/detail/:id',
        component: DetailComponent,
        resolve  : {
            members: AdminMembersService
        },
        canActivate: [AdminAuthGuard]
    }
];

@NgModule({
    declarations: [
        AdminMembersComponent,
        DetailComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatTableModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        
        FuseSharedModule
    ],
    providers   : [
        AdminMembersService
    ]
})
export class AdminMembersModule
{
}
