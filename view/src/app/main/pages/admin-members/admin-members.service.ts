import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { config } from 'app/server-config'
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AdminMembersService implements Resolve<any>
{
    adminMembers: any;
    adminMembersOnChanged: BehaviorSubject<any>;;
    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
        // Set the defaults
        this.adminMembersOnChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
    }

    /**
     * Get
     */
    getAdminList(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'users/adminList', params);
    }
    getAdminDetail(id: number): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'users/adminDetail', {id: id});
    }
    /**
     * Update
     */
    mergeAdminMember(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'users/mergeAdminMember', params);
    }
    /**
     * Delete
     */
    deleteAdminMember(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'users/deleteAdminMember', params);
    }
}
