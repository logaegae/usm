import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminMembersService } from '../admin-members.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

const path = "pages/admin-members";

@Component({
    selector     : 'detail',
    templateUrl  : './detail.component.html',
    styleUrls    : ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailComponent implements OnInit, OnDestroy
{
    form: FormGroup;
    createdBy: string;
    updatedBy: string;
    pageSize: number = 10;
    pageIndex: number = 0;
    searchText: string = '';
    cDate: string;
    uDate: string;
    submitLoading: boolean = false;
    mode: string;
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor( 
        private router: Router,
        private route: ActivatedRoute,
        private _adminMembersService: AdminMembersService,
        private _formBuilder: FormBuilder
    ){
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    /**
     * On init
     */
    ngOnInit(): void
    {

        this.route
            .queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => {
                this.pageSize = params.size;
                this.pageIndex = params.index;
                this.searchText = params.search;
        });

        
        this.createForm();

        this.route.paramMap
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => {
                const id = params.get('id');
                if(id === 'new')
                {
                    this.mode = 'new';
                    this.createForm();
                }
                else
                {
                    if(parseInt(id)) this.getDetail(parseInt(id));
                }
        });
    }

    createForm(): void
    {
        const pwRegEx = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;
        var idRegEx = /^[A-Za-z0-9+]{4,12}$/;

        let passwordValid = [Validators.pattern(pwRegEx)];
        let idDiasbled = true;

        if(this.mode === "new")
        {
            idDiasbled = false;
            passwordValid.push(Validators.required);
        }

        // Reactive Form
        this.form = this._formBuilder.group({
            id   : [
                {
                    value   : '',
                    disabled: idDiasbled
                }, [Validators.required, Validators.pattern(idRegEx), Validators.maxLength(30)]
            ],
            email : ['', [Validators.required, Validators.email, Validators.maxLength(30)]],
            password: ['', passwordValid],
            memo  : ['', Validators.maxLength(1000)],
            group   : ['', Validators.required]
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    getDetail(id: number): void
    {
        this._adminMembersService.getAdminDetail(id)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((response) => {
                const data = response.data.detail;
                this.form.get('id').setValue(data.userId);
                this.form.get('email').setValue(data.email);
                this.form.get('group').setValue(data.group);
                this.form.get('memo').setValue(data.memo);
                this.createdBy = data.createdBy;
                this.updatedBy = data.updatedBy;
                this.cDate = data.cDate;
                this.uDate = data.uDate;
        });
    }
    backToList(): void
    {
        this.router.navigate([path], { queryParams: {size: this.pageSize, index: this.pageIndex, search: this.searchText}});
    }
    save(form: any): void
    {
        let param = {
            id: this.form.get('id').value,
            email: form.value.email,
            password: form.value.password,
            group: form.value.group,
            memo: form.value.memo
        }

        if(this.mode === 'new') param['mode'] = 'new';

        this.submitLoading = true;
        this._adminMembersService.mergeAdminMember(param)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
            response => {
                this.submitLoading = false;
                alert(response.message);
                if(response.success) this.backToList();
            },
            error => {
                this.submitLoading = false;
                console.log("Error");
                console.log(error);
            }
        );
    }
    delete(form: any): void
    {
        if(confirm("삭제하시겠습니까?"))
        {
            this.submitLoading = true;
            this._adminMembersService.deleteAdminMember({
                id: this.form.get('id').value
            })
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                response => {
                    this.submitLoading = false;
                alert(response.message);
                if(response.success) this.backToList();
                },
                error => {
                    this.submitLoading = false;
                    console.log("Error");
                    console.log(error);
                }
            );
        }
    }
}
