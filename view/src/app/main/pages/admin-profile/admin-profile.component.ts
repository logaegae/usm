import { Component, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/auth.service';

@Component({
    selector     : 'admin-profile',
    templateUrl  : './admin-profile.component.html',
    styleUrls    : ['./admin-profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AdminProfileComponent
{
    userId: string;
    /**
     * Constructor
     */
    constructor(private _authService: AuthService)
    {
        this.userId = _authService.getInfo().id;
    }
}
