import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatDividerModule, MatIconModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatDialogModule, MatProgressSpinnerModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { AdminProfileService } from 'app/main/pages/admin-profile/admin-profile.service';
import { AdminProfileComponent } from 'app/main/pages/admin-profile/admin-profile.component';
import { AdminProfileAboutComponent } from 'app/main/pages/admin-profile/tabs/about/about.component';

import { AdminAuthGuard } from 'app/admin-auth.guard';

const routes = [
    {
        path     : 'admin-profile',
        component: AdminProfileComponent,
        resolve  : {
            profile: AdminProfileService
        },
        canActivate: [AdminAuthGuard]
    }
];

@NgModule({
    declarations: [
        AdminProfileComponent,
        AdminProfileAboutComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatDialogModule,
        MatProgressSpinnerModule,

        FuseSharedModule
    ],
    providers   : [
        AdminProfileService
    ]
})
export class AdminProfileModule
{
}
