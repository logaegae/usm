import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { AdminProfileService } from 'app/main/pages/admin-profile/admin-profile.service';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

@Component({
    selector     : 'profile-about',
    templateUrl  : './about.component.html',
    styleUrls    : ['./about.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AdminProfileAboutComponent implements OnInit, OnDestroy
{
    about: any;
    infoForm: FormGroup;
    changePasswordForm: FormGroup;
    pwChanging: boolean = false;
    infoChanging: boolean = false;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProfileService} _adminProfileService
     */
    constructor(
        private _adminProfileService: AdminProfileService,
        private _formBuilder: FormBuilder
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._adminProfileService.aboutOnChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(response => {
                if(response.success) this.about = response.data.about;
                else alert(response.message);
            });

        const pwRegEx = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;

        this.changePasswordForm = this._formBuilder.group({
            oldPassword         : ['', [Validators.required, Validators.pattern(pwRegEx)]],
            password            : ['', [Validators.required, Validators.pattern(pwRegEx)]],
            passwordConfirm     : ['', [Validators.required, confirmPasswordValidator]]
        });

        this.infoForm = this._formBuilder.group({
            email          : [this.about.email, [Validators.required, Validators.email, Validators.maxLength(30)]]
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.changePasswordForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.changePasswordForm.get('passwordConfirm').updateValueAndValidity();
            });
    }
    changePw(form: any): void
    {
        this.pwChanging = true;
        const info = {
            oldPassword : form.value.oldPassword,
            password : form.value.password
        }
        this._adminProfileService.changePwInfo(info)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((response) => {

            this.pwChanging = false;
            this.changePasswordForm.reset();
            alert(response.message);
        },
        error => {
            this.pwChanging = false;

            console.log("Error");
            console.log(error);
            alert(error);
        });
    }
    changeInfo(form: any): void
    {
        this.infoChanging = true;
        const info = {
            email : form.value.email
        }
        this._adminProfileService.changeAdminInfo(info)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((response) => {

            this.infoChanging = false;
            if(response.success) {
                this.infoForm.markAsPristine(); 
                this.infoForm.get('email').setValue(response.data.email);
            }
            alert(response.message);
        },
        error => {
            this.infoChanging = false;

            console.log("Error");
            console.log(error);
            alert(error);
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return null;
    }

    if ( passwordConfirm.value === '' )
    {
        return null;
    }

    if ( password.value === passwordConfirm.value )
    {
        return null;
    }

    return {'passwordsNotMatching': true};
};
