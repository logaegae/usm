import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';
import { AdminLoginService } from 'app/main/pages/auth/admin-login/admin-login.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/auth.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    selector     : 'admin-login',
    templateUrl  : './admin-login.component.html',
    styleUrls    : ['./admin-login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AdminLoginComponent implements OnInit
{
    // Private
    private _unsubscribeAll: Subject<any>;

    loginForm: FormGroup;
    submitLoading: boolean = false;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _adminLiginService: AdminLoginService,
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private router: Router
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();

        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        //auth token
        const token = this._authService.getToken();
        
        if(token) this._authService.authToken(token)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
            response => {
                this.submitLoading = false;

                if (response.success)
                {
                    this.router.navigate(['/pages/admin-profile']);
                }
                else
                {
                    this._authService.removeToken();
                }
            },
            error => {
                this.submitLoading = false;
                console.log("Error");
                console.log(error);
            }
        );
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        const pwRegEx = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;

        this.loginForm = this._formBuilder.group({
            id   : ['', [Validators.required]],
            password: ['', [Validators.required, Validators.pattern(pwRegEx)]]
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    submit(form: any): void
    {
        this.submitLoading = true;

        this._adminLiginService.adminSignIn({
            id : form.value.id,
            password : form.value.password,
        })
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
            response => {
                this.submitLoading = false;

                if (response.success && response.data.token)
                {
                    this._authService.setToken(response.data.token);
                    this.router.navigate(['/pages/admin-profile']);
                }
                else
                {
                    alert(response.message);
                }
            },
            error => {
                this.submitLoading = false;
                console.log("Error");
                console.log(error);
            }
        );
    }
}
