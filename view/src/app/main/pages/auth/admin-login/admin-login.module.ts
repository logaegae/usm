import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule, MatProgressSpinnerModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { AdminLoginComponent } from './admin-login.component';
import { AdminLoginService } from 'app/main/pages/auth/admin-login/admin-login.service';

const routes = [
    {
        path     : 'auth/admin-login',
        component: AdminLoginComponent,
        resolve  : {
            data: AdminLoginService
        }
    }
];

@NgModule({
    declarations: [
        AdminLoginComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatProgressSpinnerModule,

        FuseSharedModule
    ],
    providers   : [
        AdminLoginService
    ]
})
export class AdminLoginModule
{
}
