import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { config } from 'app/server-config';

@Injectable()
export class AdminLoginService implements Resolve<any>
{
    routeParams: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        
    }

    /**
     * 
     *
     * @param authInfo
     * @returns {Promise<any>}
     */
    adminSignIn(authInfo: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'auth/adminSignIn', authInfo);
    }
}