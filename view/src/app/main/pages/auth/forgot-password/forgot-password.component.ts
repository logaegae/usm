import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';

import { ForgotPasswordService } from 'app/main/pages/auth/forgot-password/forgot-password.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    selector     : 'forgot-password',
    templateUrl  : './forgot-password.component.html',
    styleUrls    : ['./forgot-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ForgotPasswordComponent implements OnInit
{
    // Private
    private _unsubscribeAll: Subject<any>;
    
    forgotPasswordForm: FormGroup;
    submitLoading: boolean = false;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param {RegisterService} _forgotPasswordService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _forgotPasswordService: ForgotPasswordService,
        private router: Router
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.forgotPasswordForm = this._formBuilder.group({
            email          : ['', [Validators.required, Validators.email, Validators.maxLength(30)]],
        });
    }
    submit(form:any): void
    {
        this.submitLoading = true;

        this._forgotPasswordService.sendPwResetMail({
            email : form.value.email
        })
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
            response => {
                this.submitLoading = false;
                if(response.success)
                {
                    this.router.navigate(['/pages/auth/mail-confirm', {email : form.value.email}]);
                }
                else
                {
                    alert(response.message);
                }
            },
            error => {
                this.submitLoading = false;
                console.log("Error");
                console.log(error);
            }
        );
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
