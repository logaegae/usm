import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatProgressSpinnerModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { ForgotPasswordComponent } from './forgot-password.component';

import { ForgotPasswordService } from 'app/main/pages/auth/forgot-password/forgot-password.service';

const routes = [
    {
        path     : 'auth/forgot-password',
        component: ForgotPasswordComponent,
        resolve  : {
            data: ForgotPasswordService
        }
    }
];

@NgModule({
    declarations: [
        ForgotPasswordComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,

        FuseSharedModule,
        MatProgressSpinnerModule
    ],
    providers   : [
        ForgotPasswordService
    ]
})
export class ForgotPasswordModule
{
}
