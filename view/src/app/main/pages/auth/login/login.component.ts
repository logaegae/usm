import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { CookieService } from 'ngx-cookie-service';

import { LoginService } from 'app/main/pages/auth/login/login.service';
import { AuthService } from 'app/auth.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector     : 'login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LoginComponent implements OnInit
{
    loginForm: FormGroup;
    cookieValue: String;
    submitLoading: Boolean = false;
    returnURL: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {LoginService} _loginService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _loginService: LoginService,
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _cookieService: CookieService,
        private router: Router,
        private route: ActivatedRoute
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        //cookie exist
        this.cookieValue = _cookieService.check('USMSUID') ? _cookieService.get('USMSUID') : null;
        //auth token
        const token = this._authService.getToken();
        
        if(token) this._authService.authToken(token)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                response => {
                    this.submitLoading = false;

                    if (response.success)
                    {
                        this.router.navigate(['/pages/seller-profile']);
                    }
                    else
                    {
                        this._authService.removeToken();
                    }
                },
                error => {
                    this.submitLoading = false;
                    console.log("Error");
                    console.log(error);
                }
            );
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        const pwRegEx = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;
        this.loginForm = this._formBuilder.group({
            email   : [this._cookieService.check('USMSUID') ? this._cookieService.get('USMSUID') : '', [Validators.required, Validators.email, Validators.maxLength(30)]],
            password: ['', [Validators.required, Validators.pattern(pwRegEx)]],
            rememberMe: [ this._cookieService.check('USMSUID') ? true : '' ]
        });
        this.route.queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => {
                this.returnURL = params.returnURL;
        });
    }
    submit(form: any): void
    {
        if (form.value.rememberMe)
        {
            this._cookieService.set('USMSUID', form.value.email);
        }
        else
        {
            this._cookieService.delete('USMSUID');
        }

        this.submitLoading = true;

        this._loginService.memberSignIn({
            email : form.value.email,
            password : form.value.password,
        })
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
            response => {
                this.submitLoading = false;

                if (response.success && response.data.token)
                {
                    this._authService.setToken(response.data.token);
                    if(this.returnURL) this.router.navigate([this.returnURL]);
                    else this.router.navigate(['/pages/seller-profile']);
                }
                else
                {
                    alert(response.message);
                }
            },
            error => {
                this.submitLoading = false;
                console.log("Error");
                console.log(error);
            }
        );
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
