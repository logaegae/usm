import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

import { config } from '../../../../server-config';

@Injectable()
export class LoginService implements Resolve<any>
{
    routeParams: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        
    }

    /**
     * Add order
     *
     * @param memberInfo
     * @returns {Promise<any>}
     */
    memberSignIn(memberInfo: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'auth/memberSignIn', memberInfo);
    }
}