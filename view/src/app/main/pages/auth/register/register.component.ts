import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { RegisterService } from 'app/main/pages/auth/register/register.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
    selector     : 'register',
    templateUrl  : './register.component.html',
    styleUrls    : ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class RegisterComponent implements OnInit, OnDestroy
{
    registerForm: FormGroup;
    count: number = 0;
    handleCount: any;
    sendMail: Boolean = false;
    handleAuth: any;
    mailAuth: Boolean = false;
    authCallback: number;
    mailLoading: Boolean = false;
    submitLoading: Boolean = false;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {RegisterService} _registerService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _registerService: RegisterService,
        private _formBuilder: FormBuilder,
        public dialog: MatDialog,
        private router: Router
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        const _this = this;
        const pwRegEx = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;

        _this.registerForm = _this._formBuilder.group({
            name           : ['', [Validators.required, Validators.maxLength(30)]],
            email          : ['', [Validators.required, Validators.email, Validators.maxLength(30)]],
            codeNumber     : ['', [Validators.required, Validators.maxLength(6)]],
            password       : ['', [Validators.required, Validators.pattern(pwRegEx), Validators.maxLength(15)]],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator, Validators.maxLength(15)]],
            agreement      : [false, Validators.required]
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        _this.registerForm.get('password').valueChanges
            .pipe(takeUntil(_this._unsubscribeAll))
            .subscribe(() => {
                _this.registerForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        if (this.handleCount) clearInterval(this.handleCount);
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    //개인정보 정책
    openTerms(): void
    {
        const dialogRef = this.dialog.open(RegisterTerms);
    
        dialogRef.afterClosed()
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });
    }

    secondToMinute(num: number): String
    {
        let remain = num % 60;
        let second = '0' + remain;
        let result;
        if (num) result = ((num-remain)/60) + ":" + second.substr(second.length-2, 2);
        else result = "--:--";
        return result;
    }

    //인증메일 발송
    sendAuthMail(): void
    {
        const _this = this;
        
        const email = _this.registerForm.get('email').value;
        
        _this.registerForm.get('codeNumber').setValue('');

        _this.mailLoading = true;
        _this._registerService.sendAuthMail({
            email: email
        })
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
            response => {

                _this.mailLoading = false;

                if (response.success) 
                {
                    _this.authCallback = response.data.id
                    _this.count = 5 * 60;
                    _this.sendMail = true;
                    if (_this.handleCount) clearInterval(_this.handleCount);

                    _this.handleCount = setInterval(()=>{
                        if (_this.count > 0) _this.count--;
                        else {
                            clearInterval(_this.handleCount);
                            _this.handleCount = null;
                            _this.sendMail = true;
                        }
                    }, 1000);
                    alert("인증메일이 발송되었습니다\n메일을 확인해주세요.");
                }
                else
                {
                    alert(response.message);
                }
            },
            error => {

                _this.mailLoading = false;

                console.log("Error");
                console.log(error);
                alert(error);
            }
        );
    }
    //인증 번호 인증
    authCode(): void 
    {
        const _this = this;
        const code = this.registerForm.get('codeNumber').value;
        const email = this.registerForm.get('email').value;

        _this.mailLoading = true;

        _this._registerService.mailCodeAuth({
            code: code,
            email: email
        })
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
            response => {
                _this.mailLoading = false;

                if (response.success) 
                {
                    _this.count = 0;
                    clearInterval(_this.handleCount);
                    _this.handleCount = null;
                    _this.mailAuth = true;
                    _this.sendMail = false;
                    _this.authCallback = response.data.authCallback;
                    //_this.registerForm.get('email').disable();
                }
                else
                {
                }
                alert(response.message);
            },
            error => {
                _this.mailLoading = false;
                console.log("Error");
                console.log(error);
                alert(error);
            }
        );
    }
    //회원가입하기
    submit(form:any): void 
    {
        this.submitLoading = true;

        this._registerService.memberSignUp({
            companyName : form.value.name,
            email : form.value.email,
            password : form.value.password,
            code : form.value.codeNumber,
            authCallback : this.authCallback,
            from : "web"
        })
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
            response => {
                this.submitLoading = false;

                if(response.success)
                {
                    this.router.navigate(['/pages/auth/signup-complete']);
                }
                else
                {
                    
                }
                alert(response.message);
            },
            error => {
                this.submitLoading = false;
                console.log("Error");
                console.log(error);
            }
        );
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return null;
    }

    if ( passwordConfirm.value === '' )
    {
        return null;
    }

    if ( password.value === passwordConfirm.value )
    {
        return null;
    }

    return {'passwordsNotMatching': true};
};

/**
 * Terms
 *
 */

@Component({
    selector: 'register-terms',
    templateUrl: 'register-terms.html'
})
export class RegisterTerms {}