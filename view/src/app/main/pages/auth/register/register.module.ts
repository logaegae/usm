import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule, MatDialogModule, MatGridList, MatGridTile, MatProgressSpinnerModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { RegisterComponent, RegisterTerms } from 'app/main/pages/auth/register/register.component';
import { RegisterService } from 'app/main/pages/auth/register/register.service';

const routes = [
    {
        path     : 'auth/register',
        component: RegisterComponent,
        resolve  : {
            data: RegisterService
        }
    }
];

@NgModule({
    declarations: [
        RegisterComponent,
        RegisterTerms,
        MatGridList,
        MatGridTile,
    ],
    entryComponents: [RegisterTerms],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatDialogModule,
        FuseSharedModule,
        MatProgressSpinnerModule
    ],
    providers   : [
        RegisterService
    ]
})
export class RegisterModule
{
}
