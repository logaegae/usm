import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { ResetPasswordService } from 'app/main/pages/auth/reset-password/reset-password.service';
import { LoginService } from 'app/main/pages/auth/login/login.service';
import { AuthService } from 'app/auth.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
    selector     : 'reset-password',
    templateUrl  : './reset-password.component.html',
    styleUrls    : ['./reset-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ResetPasswordComponent implements OnInit, OnDestroy
{
    resetPasswordForm: FormGroup;
    email: string;
    code: string;
    submitLoading: boolean = false;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _resetPasswordService: ResetPasswordService,
        private _loginService: LoginService,
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();

        this.route.queryParamMap.subscribe(params => {
            this.email = params.get('email');
            this.code = params.get('code');

            if(!this.email || !this.code)
            {
                alert("잘못된 경로입니다");
                this.router.navigate(['/pages/auth/login']);
            }
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        const pwRegEx = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;

        this.resetPasswordForm = this._formBuilder.group({
            email          : [this.email, [Validators.required, Validators.email, Validators.maxLength(30)]],
            password       : ['', [Validators.required, Validators.pattern(pwRegEx)]],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]],
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.resetPasswordForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.resetPasswordForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Submit
     */
    submit(form: any): void
    {
        this.submitLoading = true;

        this._resetPasswordService.sendNewPassword({
            email : form.value.email,
            password : form.value.password,
            code : this.code
        }).subscribe(
            response => {
                this.submitLoading = false;

                if(response.success)
                {
                    this.login(form);
                }
                alert(response.message);
            },
            error => {
                this.submitLoading = false;
                console.log("Error");
                console.log(error);
            }
        );
    }
    login(form: any): void
    {
        this.submitLoading = true;

        this._loginService.memberSignIn({
            email : form.value.email,
            password : form.value.password
        }).subscribe(
            response => {
                this.submitLoading = false;

                if (response.success && response.data.token)
                {
                    this._authService.setToken(response.data.token);
                    this.router.navigate(['/apps/dashboards/analytics']);
                }
                else
                {
                    alert(response.message);
                }
            },
            error => {
                this.submitLoading = false;
                console.log("Error");
                console.log(error);
            }
        );
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return null;
    }

    if ( passwordConfirm.value === '' )
    {
        return null;
    }

    if ( password.value === passwordConfirm.value )
    {
        return null;
    }

    return {'passwordsNotMatching': true};
};
