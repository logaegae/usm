import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatProgressSpinnerModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { ResetPasswordComponent } from './reset-password.component';
import { ResetPasswordService } from 'app/main/pages/auth/reset-password/reset-password.service';

const routes = [
    {
        path     : 'auth/reset-password',
        component: ResetPasswordComponent,
        resolve  : {
            data: ResetPasswordService
        }
    }
];

@NgModule({
    declarations: [
        ResetPasswordComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,

        FuseSharedModule,
        MatProgressSpinnerModule
    ],
    providers   : [
        ResetPasswordService
    ]
})
export class ResetPasswordModule
{
}
