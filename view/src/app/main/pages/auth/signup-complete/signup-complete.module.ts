import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { SignupCompleteComponent } from './signup-complete.component';

const routes = [
    {
        path     : 'auth/signup-complete',
        component: SignupCompleteComponent
    }
];

@NgModule({
    declarations: [
        SignupCompleteComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatIconModule,

        FuseSharedModule
    ]
})
export class SignupCompleteModule
{
}
