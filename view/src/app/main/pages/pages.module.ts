import { NgModule } from '@angular/core';

import { LoginModule } from 'app/main/pages/auth/login/login.module';
import { AdminLoginModule } from 'app/main/pages/auth/admin-login/admin-login.module';
import { RegisterModule } from 'app/main/pages/auth/register/register.module';
import { ForgotPasswordModule } from 'app/main/pages/auth/forgot-password/forgot-password.module';
import { ResetPasswordModule } from 'app/main/pages/auth/reset-password/reset-password.module';
import { LockModule } from 'app/main/pages/auth/lock/lock.module';
import { MailConfirmModule } from 'app/main/pages/auth/mail-confirm/mail-confirm.module';
import { SignupCompleteModule } from 'app/main/pages/auth/signup-complete/signup-complete.module';
import { Error404Module } from 'app/main/pages/errors/404/error-404.module';
import { Error500Module } from 'app/main/pages/errors/500/error-500.module';
import { SellerProfileModule } from 'app/main/pages/seller-profile/seller-profile.module';
import { AdminProfileModule } from 'app/main/pages/admin-profile/admin-profile.module';
import { AdminMembersModule } from 'app/main/pages/admin-members/admin-members.module';
import { SellerMembersModule } from 'app/main/pages/seller-members/seller-members.module';

@NgModule({
  imports: [
    LoginModule,
    AdminLoginModule,
    RegisterModule,
    ForgotPasswordModule,
    ResetPasswordModule,
    LockModule,
    MailConfirmModule,
    SignupCompleteModule,

    // Errors
    Error404Module,
    Error500Module,

    // Profile
    SellerProfileModule,
    AdminProfileModule,

    //Members
    AdminMembersModule,
    SellerMembersModule,
    
  ]
})
export class PagesModule { }
