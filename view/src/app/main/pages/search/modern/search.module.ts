import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatTableModule, MatTabsModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { SearchComponent } from 'app/main/pages/search/modern/search.component';
import { SearchService } from 'app/main/pages/search/modern/search.service';
import { SellerAuthGuard } from 'app/seller-auth.guard';


const routes = [
    {
        path     : 'search/',
        component: SearchComponent,
        resolve  : {
            search: SearchService
        },
        canActivate: [SellerAuthGuard]
    }
];

@NgModule({
    declarations: [
        SearchComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatTableModule,
        MatTabsModule,

        FuseSharedModule
    ],
    providers   : [
        SearchService
    ]
})
export class SearchModule
{
}
