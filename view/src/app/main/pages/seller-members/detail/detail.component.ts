import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { SellerMembersService } from '../seller-members.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { config } from 'app/server-config'

const path = "pages/seller-members";

@Component({
    selector     : 'detail',
    templateUrl  : './detail.component.html',
    styleUrls    : ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailComponent implements OnInit, OnDestroy
{
    staticDomain: string = config.static.domain;
    form: FormGroup;
    createdBy: string;
    updatedBy: string;
    accountCDate: string;
    accountCreatedBy: string;
    accountUDate: string;
    accountUpdatedBy: string;
    accountVerifiedDate: string;
    accountVerifiedBy: string;
    pageSize: number = 10;
    pageIndex: number = 0;
    searchText: string = '';
    sort1: string;
    sort2: string;
    cDate: string;
    uDate: string;
    lastLoginDate: string;
    submitLoading: boolean = false;
    mode: string;
    bankList = ['NH농협', '국민', '신한', '우리', 'IBK기업', 'SC제일', '부산', '경남', '우체국', '미래에셋대우', '대구', '광주', '전북', '제주', '산업', '새마을', 'KEB하나', '신협', '수협', '씨티', '케이뱅크', '카카오뱅크'];
    uploadFile: FileList;
    valiadteLoading: boolean = false;
    accountLoading: boolean = false;
    basicLoading: boolean = false;
    attachDeleting: boolean = false;
    companyLoading: boolean = false;
    uploadMessage: string;
    attachments: any = [];
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor( 
        private router: Router,
        private route: ActivatedRoute,
        private _SellerMembersService: SellerMembersService,
        private _formBuilder: FormBuilder
    ){
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    /**
     * On init
     */
    ngOnInit(): void
    {

        this.route
            .queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => {
                this.pageSize = params.size;
                this.pageIndex = params.index;
                this.searchText = params.search;
                this.sort1 = params.sort1;
                this.sort2 = params.sort2;
        });

        this.createForm();

        this.route.paramMap
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => {
                const id = params.get('id');
                if(id === 'new')
                {
                    this.mode = 'new';
                    this.createForm();
                }
                else
                {
                    this.mode = id;
                    if(parseInt(id)) this.getDetail(parseInt(id));
                }
        });
    }

    createForm(): void
    {
        const pwRegEx = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;

        let passwordValid = [Validators.pattern(pwRegEx)];
        let idDiasbled = true;

        if(this.mode === "new")
        {
            idDiasbled = false;
            passwordValid.push(Validators.required);
        }

        // Reactive Form
        this.form = this._formBuilder.group({
            companyName   : ['', [Validators.required, Validators.maxLength(100)]
            ],
            email               : [{
                                    value   : '',
                                    disabled: idDiasbled
                                }, [Validators.required, Validators.email, Validators.maxLength(30)]],
            password            : ['', passwordValid],
            sellerPhone         : ['',[Validators.required, Validators.maxLength(20)]],
            isPhoneCertified    : ['N', Validators.required],
            isSellerCertified   : ['N', Validators.required],
            isAccountCertified  : ['N', Validators.required],
            isSentAccount       : [false, Validators.required],
            isSentDocument      : [false, Validators.required],
            representation      : ['', [Validators.required, Validators.maxLength(30)]],
            companyPhone        : ['', [Validators.required,  Validators.maxLength(20)]],
            sector              : ['', [Validators.required, Validators.maxLength(45)]],
            business            : ['', [Validators.required, Validators.maxLength(45)]],
            companyNumber       : ['', [Validators.required,  Validators.maxLength(45)]],
            corporateNumber     : ['', [Validators.maxLength(45)]],
            ownerName           : ['', [Validators.required, Validators.maxLength(30)]],
            bankAccount         : ['', [Validators.required,  Validators.maxLength(30)]]
        });
    }
    
    selectBank = new FormControl('', [
        Validators.required
    ]);

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    deleteAttachment(item: any): void
    {
        console.log(item);
        if(this.mode !== 'new' && !this.attachDeleting && confirm('첨부파일을 삭제하시겠습니까?')) 
        {
            const attachId = parseInt(item.querySelector('a').getAttribute('id').replace('attach-',''));
            this.attachDeleting = true;
            const param = {
                attachId,
                id: this.mode
            }
            this._SellerMembersService.deleteSellerAttachment(param)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                response => {
                    this.attachDeleting = false;
                    this.attachments = response.data.attachments;
                    alert(response.message);
                },
                error => {
                    this.attachDeleting = false;
                    console.log("Error");
                    console.log(error);
                }
            );
        }
    }
    getDetail(id: number): void
    {
        this._SellerMembersService.getSellerDetail(id)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((response) => {
                const detail = response.data.detail;
                const attachments = response.data.attachments;
                this.form.get('companyName').setValue(detail.companyName);
                this.form.get('representation').setValue(detail.representation);
                this.form.get('email').setValue(detail.email);
                this.form.get('companyPhone').setValue(detail.companyPhone);
                this.form.get('sellerPhone').setValue(detail.sellerPhone);
                this.form.get('isPhoneCertified').setValue(detail.isPhoneCertified);
                this.form.get('isAccountCertified').setValue(detail.isAccountCertified);
                this.form.get('isSellerCertified').setValue(detail.isSellerCertified);
                this.form.get('isSentAccount').setValue(detail.isSentAccount === 'Y' ? true : false);
                this.form.get('isSentDocument').setValue(detail.isSentDocument === 'Y' ? true : false);
                this.form.get('representation').setValue(detail.representation);
                this.form.get('sector').setValue(detail.sector);
                this.form.get('business').setValue(detail.business);
                this.form.get('companyNumber').setValue(detail.companyNumber);
                this.selectBank.setValue(detail.bankName);
                this.form.get('ownerName').setValue(detail.ownerName);
                this.form.get('bankAccount').setValue(detail.bankAccount);
                
                this.updatedBy = detail.updatedBy;
                this.cDate = detail.cDate;
                this.uDate = detail.uDate;
                this.lastLoginDate = detail.lastLiginDate;

                this.accountCDate = detail.accountCDate;
                this.accountCreatedBy = detail.accountCreatedBy;
                this.accountUDate = detail.accountUDate;
                this.accountUpdatedBy = detail.accountUpdatedBy;
                this.accountVerifiedDate = detail.accountVerifiedDate;
                this.accountVerifiedBy = detail.accountVerifiedBy || 'N';
                this.attachments = attachments || [];
        });
    }
    backToList(): void
    {
        this.router.navigate([path], { queryParams: {size: this.pageSize, index: this.pageIndex, search: this.searchText, sort1: this.sort1, sort2: this.sort2}});
    }
    save(form: any): void
    {
        let formData:FormData = new FormData();
        if(this.uploadFile && this.uploadFile.length > 0)
        {
            for(let i=0; i<this.uploadFile.length; i++){
                formData.append('uploadFile', this.uploadFile[i], this.uploadFile[i].name);
            }
        }

        for(let key in form.value){
            if(key == 'isSentAccount' || key == 'isSentDocument') formData.append(key, form.value[key] ? 'Y' : 'N');
            else formData.append(key, form.value[key]);
        }
        formData.append('bankName', this.selectBank.value);
        formData.append('email', this.form.get('email').value);
        
        if(this.mode === 'new') formData.append('id', 'new');
        else formData.append('id', this.mode);

        this.submitLoading = true;

        this._SellerMembersService.mergeSellerMember(formData)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
            response => {
                this.submitLoading = false;
                alert(response.message);
                if(response.success && this.mode === 'new') this.backToList();
                else if(response.success) {
                    this.form.markAsPristine();
                    let sendmail = false;
                    if(this.form.get('isAccountCertified').value == 'Y' || this.form.get('isSellerCertified').value == 'Y') sendmail = confirm("사업자 인증 혹은 계좌인증을 승인하셨습니다.\n판매자에게 안내메일을 보내시겠습니까?");
                    if(sendmail) {
                        this._SellerMembersService.sendMailToSeller({
                            email : this.form.get('email').value,
                            isAccountCertified : this.form.get('isAccountCertified').value,
                            isSellerCertified : this.form.get('isSellerCertified').value
                        }).pipe(takeUntil(this._unsubscribeAll))
                        .subscribe(response => {
                            alert(response.message)
                        });
                    }
                }
            },
            error => {
                this.submitLoading = false;
                console.log("Error");
                console.log(error);
            }
        );
    }
    delete(form: any): void
    {
        if(confirm("삭제하시겠습니까?"))
        {
            this.submitLoading = true;
            this._SellerMembersService.deleteSellerMember({
                id: parseInt(this.mode),
                email: this.form.get('email').value,
            })
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                response => {
                    this.submitLoading = false;
                alert(response.message);
                if(response.success) this.backToList();
                },
                error => {
                    this.submitLoading = false;
                    console.log("Error");
                    console.log(error);
                }
            );
        }
    }
    fileChange(event: any): void
    {
        console.log(event.target.files)
        let fileList: FileList = event.target.files;
        let uploadMessage = ""
        if(fileList.length > 0) {
            this.uploadFile = fileList;
            for(let i=0; i<fileList.length; i++){
                uploadMessage += (fileList[i].name + "<br/>");
            }
        }
        this.uploadMessage = uploadMessage;
    }

    onClickAttachBtn(attachInput: any): void
    {
        attachInput.click();
    }
}
