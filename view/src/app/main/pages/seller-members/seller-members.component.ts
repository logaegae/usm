import {Component, OnInit, ViewChild} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {MatPaginator, MatTableDataSource, MatSort, PageEvent} from '@angular/material';
import { SellerMembersService } from './seller-members.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const path = "pages/seller-members";

export interface SellerUserElement {
    id: number;
    companyName: string;
    email: string;
    isAccountCertified: string;
    isSellerCertified: string;
    isSentAccount: string;
    isSentDocument: string;
    cDate: string;
}

@Component({
    selector: 'seller-members',
    styleUrls: ['seller-members.component.scss'],
    templateUrl: 'seller-members.component.html'
})
export class SellerMembersComponent {

    displayedColumns: string[] = ['companyName', 'email', 'isAccountCertified', 'isSentAccount', 'isSellerCertified', 'isSentDocument', 'cDate'];
    dataSource: SellerUserElement[];
    // MatPaginator Inputs
    length: number;
    pageSize: number;
    pageSizeOptions: number[];
    pageIndex: number;
    // MatPaginator Output
    pageEvent: PageEvent;
    searchText: string;
    reloading: boolean;
    listLoading: boolean;
    sort1: string;
    sort2: string;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private _SellerMembersService: SellerMembersService
    ) {

        // Set the private defaults
        this._unsubscribeAll = new Subject();
        
        this.pageIndex = 0;
        this.length = 0;
        this.pageSize = 10;
        this.pageSizeOptions = [10, 25, 100];
        this.reloading = false;
        this.listLoading = true;
    }

    ngOnInit() {
        this.route
            .queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => {
                this.pageSize = params.size || 10;
                this.pageIndex = params.index;
                this.searchText = params.search;
                this.sort1 = params.sort1;
                this.sort2 = params.sort2;
                this.getList();
        });
    }

    sortData(e:any):void
    {
        this.router.navigate([path], { queryParams: {size: this.pageSize, index: this.pageIndex, search: this.searchText, sort1: e.active, sort2: e.direction}});
    }

    getList() {
        this.reloading = true;
        this._SellerMembersService.getSellerList({
            pageIndex: this.pageIndex, 
            pageSize: this.pageSize, 
            searchText: this.searchText
        })
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((response) => {
                if(response.success) 
                {
                    this.dataSource = response.data.list;
                    this.length = response.data.length;
                }
                else
                {
                    this.dataSource = [];
                    this.length = 0;
                }
                this.reloading = false;
                this.listLoading = false;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onPageEvent(e: any): void
    {
        this.router.navigate([path], { queryParams: {size: e.pageSize, index: e.pageIndex, search: this.searchText, sort1: this.sort1, sort2: this.sort2}});
    }

    getRecord(row: SellerUserElement): void
    {
        this.router.navigate([path+"/detail/" + row.id], { queryParams: {size: this.pageSize, index: this.pageIndex, search: this.searchText, sort1: this.sort1, sort2: this.sort2}});
    }
    onChangeSort(): void
    {
        if(this.sort2 && !this.sort1) {
            alert("정렬할 항목을 선택해주세요");
        }else{
            console.log(this.sort1, this.sort2)
            this.router.navigate([path], { queryParams: {size: this.pageSize, index: this.pageIndex, search: this.searchText, sort1: this.sort1, sort2: this.sort2}});
        }
    }
    onKeyAtSearch(event: any) 
    {
        const SYMBOL_REGEX = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;

        if(event.keyCode == 13)
        {
            if(event.target.value.length > 2) 
            {
                if(!SYMBOL_REGEX.test(event.target.value))
                {
                    this.searchText = event.target.value;
                    this.router.navigate([path], { queryParams: {size: this.pageSize, index: 0, search: this.searchText, sort1: this.sort1, sort2: this.sort2}});
                }
                else
                {
                    alert("특수문자를 입력할 수 없습니다");
                }
            }
            else
            {
                alert("검색을 위해 두 글자 이상 입력해주세요");
            }
        }
    }

    onClickSearch(): void 
    {
        this.router.navigate([path], { queryParams: {size: this.pageSize, index: 0, search: this.searchText, sort1: this.sort1, sort2: this.sort2}});
    }

    add(): void 
    {
        this.router.navigate([path+"/detail/new"], { queryParams: {size: this.pageSize, index: this.pageIndex, search: this.searchText, sort1: this.sort1, sort2: this.sort2}});
    }
}
