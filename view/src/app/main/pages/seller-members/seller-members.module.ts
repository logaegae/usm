import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatTableModule, MatProgressSpinnerModule, MatPaginatorModule, MatIconModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatButtonModule, MatRadioModule, MatSortModule, MatChipsModule, MatDividerModule, MatSlideToggleModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { SellerMembersService } from 'app/main/pages/seller-members/seller-members.service';
import { SellerMembersComponent } from 'app/main/pages/seller-members/seller-members.component';
import { DetailComponent } from 'app/main/pages/seller-members/detail/detail.component';
import { AdminAuthGuard } from 'app/admin-auth.guard';
import { DirectivesModule } from 'directives/directives.module';

const routes = [
    {
        path     : 'seller-members',
        component: SellerMembersComponent,
        resolve  : {
            members: SellerMembersService
        },
        canActivate: [AdminAuthGuard]
    },
    {
        path     : 'seller-members/detail/:id',
        component: DetailComponent,
        resolve  : {
            members: SellerMembersService
        },
        canActivate: [AdminAuthGuard]
    }
];

@NgModule({
    declarations: [
        SellerMembersComponent,
        DetailComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatRadioModule,
        MatButtonModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatChipsModule,
        MatDividerModule,
        MatSlideToggleModule,
        
        FuseSharedModule,
        
        //Directives
        DirectivesModule
    ],
    providers   : [
        SellerMembersService
    ]
})
export class SellerMembersModule
{
}
