import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { config } from 'app/server-config'
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class SellerMembersService implements Resolve<any>
{
    SellerMembers: any;
    SellerMembersOnChanged: BehaviorSubject<any>;;
    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
        // Set the defaults
        this.SellerMembersOnChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
    }

    /**
     * Get
     */
    getSellerList(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'users/sellerList', params);
    }
    getSellerDetail(id: number): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'users/sellerDetail', {id: id});
    }
    /**
     * Update
     */
    mergeSellerMember(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'users/mergeSellerMember', params);
    }
    sendMailToSeller(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'users/sendmailtocustomer', params);
    }
    /**
     * Delete
     */
    deleteSellerMember(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'users/deleteSellerMember', params);
    }
    deleteSellerAttachment(params: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'users/deleteSellerAttachment', params);
    }
}
