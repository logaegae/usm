import { Component, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/auth.service';

@Component({
    selector     : 'seller-profile',
    templateUrl  : './seller-profile.component.html',
    styleUrls    : ['./seller-profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class SellerProfileComponent
{
    userId: string;
    /**
     * Constructor
     */
    constructor(private _authService: AuthService)
    {
        this.userId = _authService.getInfo().id;
    }
}
