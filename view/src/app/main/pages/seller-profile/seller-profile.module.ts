import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatChipsModule, MatButtonModule, MatDividerModule, MatIconModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatDialogModule, MatProgressSpinnerModule, MatTabsModule, MatSelectModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { SellerProfileService } from 'app/main/pages/seller-profile/seller-profile.service';
import { SellerProfileComponent } from 'app/main/pages/seller-profile/seller-profile.component';
import { SellerProfileAboutComponent } from 'app/main/pages/seller-profile/tabs/about/about.component';
import { SellerProfilePasswordComponent } from 'app/main/pages/seller-profile/tabs/password/password.component';
import { SellerAuthGuard } from 'app/seller-auth.guard';
import { DirectivesModule } from 'directives/directives.module';

const routes = [
    {
        path     : 'seller-profile',
        component: SellerProfileComponent,
        resolve  : {
            profile: SellerProfileService
        },
        canActivate: [SellerAuthGuard]
    }
];

@NgModule({
    declarations: [
        SellerProfileComponent,
        SellerProfileAboutComponent,
        SellerProfilePasswordComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatChipsModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatTabsModule,

        FuseSharedModule,
        
        //Directives
        DirectivesModule
    ],
    providers   : [
        SellerProfileService
    ]
})
export class SellerProfileModule
{
}
