import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { config } from 'app/server-config'
import { AuthService } from 'app/auth.service';

@Injectable()
export class SellerProfileService implements Resolve<any>
{
    timeline: any;
    about: any;
    photosVideos: any;

    timelineOnChanged: BehaviorSubject<any>;
    aboutOnChanged: BehaviorSubject<any>;
    photosVideosOnChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _authService: AuthService
    )
    {
        // Set the defaults
        this.timelineOnChanged = new BehaviorSubject({});
        this.aboutOnChanged = new BehaviorSubject({});
        this.photosVideosOnChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getAbout()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get about
     */
    getAbout(): Promise<any[]>
    {
        return new Promise((resolve, reject) => {

            this._httpClient.get(config.api.domain + 'profile/aboutSeller')
                .subscribe((about: any) => {
                    this.about = about;
                    this.aboutOnChanged.next(this.about);
                    if(this.about.success) resolve(this.about);
                    else reject(this.about.message);
                }, reject);
        });
    }
    getAboutHttp(): Observable<any>
    {
        return this._httpClient.get<any>(config.api.domain + 'profile/aboutSeller');
    }
    changeSellerInfo(info: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'profile/changeSellerInfo', info);
    }
    changePwInfo(info: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'profile/changePwInfo', info);
    }
    changeSellerPhone(info: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'profile/changeSellerPhone', info);
    }
    changeSellerAccount(info: any): Observable<any>
    {
        return this._httpClient.post<any>(config.api.domain + 'profile/changeSellerAccount', info);
    }
}
