import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { SellerProfileService } from 'app/main/pages/seller-profile/seller-profile.service';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';

@Component({
    selector     : 'profile-about',
    templateUrl  : './about.component.html',
    styleUrls    : ['./about.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class SellerProfileAboutComponent implements OnInit, OnDestroy
{
    about: any;
    basicForm: FormGroup;
    infoForm: FormGroup;
    accountForm: FormGroup;
    accountChanging: boolean = false;
    infoChanging: boolean = false;
    basicFormChanging: boolean = false;
    needAttach: boolean = true;
    uploadFile: File
    uploadMessage: string = "사업자 등록 증명서를 업로드 해 주세요";
    bankList = ['NH농협', '국민', '신한', '우리', 'IBK기업', 'SC제일', '부산', '경남', '우체국', '미래에셋대우', '대구', '광주', '전북', '제주', '산업', '새마을', 'KEB하나', '신협', '수협', '씨티', '케이뱅크', '카카오뱅크']

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProfileService} _sellerProfileService
     */
    constructor(
        private _sellerProfileService: SellerProfileService,
        private _formBuilder: FormBuilder
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._sellerProfileService.aboutOnChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(response => {
                if(response.success) {
                    this.about = response.data.about;
                    this.selectBank.setValue(this.about.bankName);
                }
                else alert(response.message);
            });

        this.basicForm = this._formBuilder.group({
            sellerPhone         : [this.about.sellerPhone, [Validators.required,  Validators.maxLength(20)]],
            sellerAlarmEmail    : [this.about.sellerAlarmEmail, [Validators.required, Validators.email, Validators.maxLength(30)]],
        });

        this.infoForm = this._formBuilder.group({
            companyName         : [this.about.companyName, [Validators.required, Validators.maxLength(30)]],
            representation      : [this.about.representation, [Validators.required, Validators.maxLength(30)]],
            companyPhone        : [this.about.companyPhone, [Validators.required,  Validators.maxLength(20)]],
            sector              : [this.about.sector, [Validators.required, Validators.maxLength(45)]],
            business            : [this.about.business, [Validators.required, Validators.maxLength(45)]],
            companyNumber       : [this.about.companyNumber, [Validators.required,  Validators.maxLength(45)]],
            corporateNumber     : [this.about.corporateNumber, [Validators.maxLength(45)]]
        });

        this.accountForm = this._formBuilder.group({
            //bankName         : [this.about.bankName, [Validators.required, Validators.maxLength(30)]],
            ownerName        : [this.about.ownerName, [Validators.required, Validators.maxLength(30)]],
            bankAccount      : [this.about.bankAccount, [Validators.required,  Validators.maxLength(30)]]
        });
    }

    selectBank = new FormControl('', [
        Validators.required
    ]);
    
    getAbout(): void
    {
        this._sellerProfileService.getAboutHttp()
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(response => {
                if(response.success) this.about = response.data.about;
                else alert(response.message);
            });
    }
    fileChange(event: any): void
    {
        let fileList: FileList = event.target.files;
        if(fileList.length > 0) {
            this.uploadFile = fileList[0];
            this.uploadMessage = this.uploadFile.name;
            this.needAttach = false;
        }
    }
    changeInfo(form: any): void
    {
        let changeConfirm;

        if(!this.uploadFile){
            alert('사업자 등록 증명서 파일을 선택해주세요');
            changeConfirm = false;
        }
        else if(this.about.isSellerCertified === 'Y' && this.about.sentDocument !== 'Y') changeConfirm = confirm("사업자 정보를 변경할 경우 인증 완료시 까지 서비스를 이용하실 수 없습니다.\n인증은 영업일 기준 1일 이내에 완료됩니다.\n변경하시겠습니까?");
        else if(this.about.sentDocument === 'Y') changeConfirm = confirm("이미 사업자 인증 신청이 접수되었습니다.\n다시 요청하시겠습니까?");
        else changeConfirm = confirm("인증은 영업일 기준 1일 이내에 완료됩니다.\n사업자 인증을 요청 하시겠습니까?");
        if(changeConfirm) {
            this.infoChanging = true;

            let formData:FormData = new FormData();
            formData.append('uploadFile', this.uploadFile, this.uploadFile.name);
            formData.append('companyName', form.value.companyName);
            formData.append('representation', form.value.representation);
            formData.append('companyPhone', form.value.companyPhone + "");
            formData.append('sector', form.value.sector);
            formData.append('business', form.value.business);
            formData.append('companyNumber', form.value.companyNumber);
            formData.append('corporateNumber', form.value.corporateNumber);
            
            this._sellerProfileService.changeSellerInfo(formData)
            .pipe(
                take(1)
            )
            .subscribe((response) => {

                this.infoChanging = false;
                this.getAbout();
                this.infoForm.markAsPristine();
                alert(response.message);
            },
            error => {
                this.infoChanging = false;

                console.log("Error");
                console.log(error);
                alert(error);
            });
        }
    }
    changeAccount(form: any): void
    {
        let changeConfirm;
        if(!this.selectBank.value) {
            alert("은행을 선택해주세요");
            changeConfirm = false;
        }
        else if(this.about.isAccountCertified === 'Y' && this.about.sentAccountInfo !== 'Y') changeConfirm = confirm("계정 정보를 변경할 경우 인증 완료시 까지 정산 업무가 지연 됩니다.\n인증은 영업일 기준 1일 이내에 완료됩니다.\n변경하시겠습니까?");
        else if(this.about.sentDocument === 'Y') changeConfirm = confirm("이미 은행계좌 인증 신청이 접수되었습니다.\n다시 요청하시겠습니까?");
        else changeConfirm = confirm("인증은 영업일 기준 1일 이내에 완료됩니다.\은행계좌 인증을 요청 하시겠습니까?");
        if(changeConfirm) {
            this.accountChanging = true;
            const info = {
                bankName : this.selectBank.value,
                ownerName : form.value.ownerName,
                bankAccount : form.value.bankAccount
            }
            
            this._sellerProfileService.changeSellerAccount(info)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((response) => {
                this.accountChanging = false;
                this.getAbout();
                this.accountForm.markAsPristine();
                alert(response.message);
            },
            error => {
                this.accountChanging = false;

                console.log("Error");
                console.log(error);
                alert(error);
            });
        }
    }
    changeBasic(form: any): void
    {
        this.basicFormChanging = true;
        const info = {
            sellerPhone : form.value.sellerPhone + "",
            sellerAlarmEmail : form.value.sellerAlarmEmail + ""
        }
        
        this._sellerProfileService.changeSellerPhone(info)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((response) => {
            this.basicFormChanging = false;
            this.getAbout();
            this.basicForm.markAsPristine();
            alert(response.message);
        },
        error => {
            this.basicFormChanging = false;

            console.log("Error");
            console.log(error);
            alert(error);
        });
    }
    onClickAttachBtn(attachInput: any): void
    {
        attachInput.click();
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return null;
    }

    if ( passwordConfirm.value === '' )
    {
        return null;
    }

    if ( password.value === passwordConfirm.value )
    {
        return null;
    }

    return {'passwordsNotMatching': true};
};
export const confirmCompanyNumberValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const companyNumber = control.parent.get('companyNumber');
    if ( !companyNumber.value)
    {
        return null;
    }

    if ( companyNumber.value === '' )
    {
        return null;
    }

    let re = /-/g;
    let bizID = companyNumber.value.replace(re,'');
    let checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
    let tmpBizID, i, chkSum=0, c2, remander;

    for (i=0; i<=7; i++){
        chkSum += checkID[i] * bizID.charAt(i);
    }

    c2 = "0" + (checkID[8] * bizID.charAt(8));
    c2 = c2.substring(c2.length - 2, c2.length);

    chkSum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));

    remander = (10 - (chkSum % 10)) % 10 ;

    if (Math.floor(bizID.charAt(9)) !== remander){
        return null;
    }

    return {'companyNumberValid': true};
};

export const confirmCorporateNumberValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const corporateNumber = control.parent.get('corporateNumber');
    if ( !corporateNumber.value)
    {
        return null;
    }

    if ( corporateNumber.value === '' )
    {
        return null;
    }

    let re = /-/g;
    let sRegNo = corporateNumber.value.replace('-','');

    if (sRegNo.length != 13){
        return null;
    }

    let arr_regno  = sRegNo.split("");
    let arr_wt   = new Array(1,2,1,2,1,2,1,2,1,2,1,2);
    let iSum_regno  = 0;
    let iCheck_digit = 0;

    for (let i = 0; i < 12; i++){
        iSum_regno +=  eval(arr_regno[i]) * (arr_wt[i]);
    }

    iCheck_digit = 10 - (iSum_regno % 10);

    iCheck_digit = iCheck_digit % 10;

    if (iCheck_digit != arr_regno[12]){
        return null;
    }

    return {'corporateNumberValid': true};
};