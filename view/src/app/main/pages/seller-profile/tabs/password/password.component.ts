import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { SellerProfileService } from 'app/main/pages/seller-profile/seller-profile.service';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

@Component({
    selector     : 'profile-password',
    templateUrl  : './password.component.html',
    styleUrls    : ['./password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class SellerProfilePasswordComponent implements OnInit, OnDestroy
{
    about: any;
    changePasswordForm: FormGroup;
    pwChanging: boolean = false;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProfileService} _sellerProfileService
     */
    constructor(
        private _sellerProfileService: SellerProfileService,
        private _formBuilder: FormBuilder
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._sellerProfileService.aboutOnChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(response => {
                if(response.success) this.about = response.data.about;
                else alert(response.message);
            });

        const PASSWORD_REGEXP = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;

        this.changePasswordForm = this._formBuilder.group({
            oldPassword    : ['', [Validators.required, Validators.pattern(PASSWORD_REGEXP), Validators.maxLength(15)]],
            password       : ['', [Validators.required, Validators.pattern(PASSWORD_REGEXP), Validators.maxLength(15)]],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator, Validators.maxLength(15)]]
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.changePasswordForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.changePasswordForm.get('passwordConfirm').updateValueAndValidity();
            });
    }
    changePw(form: any): void
    {
        this.pwChanging = true;
        const info = {
            oldPassword : form.value.oldPassword,
            password : form.value.password
        }
        this._sellerProfileService.changePwInfo(info)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((response) => {

            this.pwChanging = false;
            this.changePasswordForm.reset();
            alert(response.message);
        },
        error => {
            this.pwChanging = false;

            console.log("Error");
            console.log(error);
            alert(error);
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return null;
    }

    if ( passwordConfirm.value === '' )
    {
        return null;
    }

    if ( password.value === passwordConfirm.value )
    {
        return null;
    }

    return {'passwordsNotMatching': true};
};