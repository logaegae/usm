import {Component} from '@angular/core';

export interface PeriodicElement {
  name: string;
  Id: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {Id: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {Id: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {Id: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {Id: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {Id: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {Id: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {Id: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {Id: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {Id: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {Id: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

/**
 * @title Basic use of `<table mat-table>`
 */
@Component({
  selector: 'sample',
  styleUrls: ['./sample.component.scss'],
  templateUrl: './sample.component.html',
})
export class SampleComponent {
  displayedColumns: string[] = ['Id', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;
}