import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    // 방문자 네비게이션
    {
        id       : 'usm',
        title    : 'Sales Management',
        type     : 'group',
        icon     : 'apps',
        group    : 'visitor',
        children : [
            {
                id       : 'account',
                title    : '계정 관리',
                type     : 'collapsable',
                icon     : 'person',
                group    : 'visitor',
                children : [
                    {
                        id   : 'profile',
                        title: '계정 정보',
                        type : 'item',
                        url  : '/pages/seller-profile',
                        group: 'visitor'
                    }
                ]
            },
        ]
    },
    // 인증받은 판매자 네비게이션
    {
        id       : 'usm',
        title    : 'Sales Management',
        type     : 'group',
        icon     : 'apps',
        group    : 'seller',
        children : [
            {
                id       : 'account',
                title    : '계정 관리',
                type     : 'collapsable',
                icon     : 'person',
                group    : 'seller',
                children : [
                    {
                        id   : 'profile',
                        title: '계정 정보',
                        type : 'item',
                        url  : '/pages/seller-profile',
                        group: 'seller'
                    }
                ]
            },
            {
                id       : 'sales',
                title    : '판매 관리',
                type     : 'collapsable',
                icon     : 'insert_chart',
                group    : 'seller',
                children : [
                    {
                        id   : 'analytics',
                        title: '접속자분석',
                        type : 'item',
                        url  : '/apps/dashboards/analytics',
                        group: 'seller'
                    },
                    {
                        id   : 'project',
                        title: '판매현황',
                        type : 'item',
                        url  : '/apps/dashboards/project',
                        group: 'seller'
                    }
                ]
            },
            {
                id       : 'product',
                title    : '상품 관리',
                type     : 'collapsable',
                icon     : 'local_mall',
                group    : 'seller',
                children : [
                    {
                        id   : 'registration',
                        title: '상품 등록',
                        type : 'item',
                        url  : '/apps/product/detail/new',
                        group: 'seller'
                    },
                    {
                        id   : 'management',
                        title: '상품 관리',
                        type : 'item',
                        url  : '/apps/product/management',
                        group: 'seller'
                    }
                ]
            },
            {
                id       : 'order',
                title    : '주문 관리',
                type     : 'collapsable',
                icon     : 'shopping_cart',
                group    : 'seller',
                children : [
                    {
                        id       : 'new-order',
                        title    : '신규주문',
                        type     : 'item',
                        url      : '/apps/product/new-order',
                        group    : 'seller',
                        badge    : {
                            title    : '25',
                            bg       : '#F44336',
                            fg       : '#FFFFFF'
                        }
                    },
                    {
                        id       : 'packing',
                        title    : '발송처리',
                        type     : 'item',
                        url      : '/apps/product/packing',
                        group    : 'seller',
                        badge    : {
                            title    : '25',
                            bg       : '#F44336',
                            fg       : '#FFFFFF'
                        }
                    },
                    {
                        id       : 'deliver',
                        title    : '배송중/배송완료',
                        type     : 'item',
                        url      : '/apps/product/delivery',
                        group    : 'seller',
                        badge    : {
                            title    : '25',
                            bg       : '#F44336',
                            fg       : '#FFFFFF'
                        }
                    },
                    {
                        id       : 'complete',
                        title    : '구매결정완료',
                        type     : 'item',
                        url      : '/apps/product/complete',
                        group    : 'seller',
                        badge    : {
                            title    : '25',
                            bg       : '#F44336',
                            fg       : '#FFFFFF'
                        }
                    }
                ]
            },
            {
                id       : 'settlement',
                title    : '정산 관리',
                type     : 'collapsable',
                icon     : 'local_atm',
                group    : 'seller',
                children : [
                    {
                        id       : 'history',
                        title    : '정산내역',
                        type     : 'item',
                        url      : '/apps/settlement/history',
                        group    : 'seller'
                    }
                ]
            }
        ],
    },
    // 사이트 관리자 네비게이션
    {
        id       : 'administration',
        title    : 'Administration',
        type     : 'group',
        icon     : 'apps',
        group    : 'admin',
        children : [
            {
                id       : 'account',
                title    : '계정 관리',
                type     : 'collapsable',
                icon     : 'person',
                group    : 'admin',
                children : [
                    {
                        id   : 'profile',
                        title: '계정 정보',
                        type : 'item',
                        url  : '/pages/admin-profile',
                        group: 'admin'
                    },
                    {
                        id   : 'adminMembers',
                        title: '관리자 계정 관리',
                        type : 'item',
                        url  : '/pages/admin-members',
                        group: 'admin'
                    },
                    {
                        id   : 'sellerMembers',
                        title: '판매자 계정 관리',
                        type : 'item',
                        url  : '/pages/seller-members',
                        group: 'admin'
                    }
                ]
            },
            {
                id       : 'settlement',
                title    : '정산 관리',
                type     : 'collapsable',
                icon     : 'local_atm',
                group    : 'admin',
                children : [
                    {
                        id       : 'history',
                        title    : '정산 입력',
                        type     : 'item',
                        url      : '/apps/settlement/history',
                        group    : 'admin'
                    }
                ]
            }
        ],
    },
    //사이트 운영자 네비게이션
    {
        id       : 'administration',
        title    : 'Administration',
        type     : 'group',
        icon     : 'apps',
        group    : 'operator',
        children : [
            {
                id       : 'account',
                title    : '계정 관리',
                type     : 'collapsable',
                icon     : 'person',
                group    : 'operator',
                children : [
                    {
                        id   : 'profile',
                        title: '계정 정보',
                        type : 'item',
                        url  : '/pages/admin-profile',
                        group: 'operator'
                    },
                    {
                        id   : 'adminMembers',
                        title: '관리자 계정 관리',
                        type : 'item',
                        url  : '/pages/admin-members',
                        group: 'operator'
                    },
                    {
                        id   : 'sellerMembers',
                        title: '판매자 계정 관리',
                        type : 'item',
                        url  : '/pages/seller-members',
                        group: 'operator'
                    }
                ]
            },
            {
                id       : 'settlement',
                title    : '정산 관리',
                type     : 'collapsable',
                icon     : 'local_atm',
                group    : 'operator',
                children : [
                    {
                        id       : 'history',
                        title    : '정산 입력',
                        type     : 'item',
                        url      : '/apps/settlement/history',
                        group    : 'operator'
                    }
                ]
            }
        ],
    }
];
