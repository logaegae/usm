import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpErrorResponse, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { AuthService } from './auth.service';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    constructor(
        private router: Router,
        private _authService: AuthService
    )
    {
    }

    private handleAuthError(err: HttpErrorResponse): Observable<any> {
        console.log(err)
        if (err.status === 401 || err.status === 403) {
            //navigate /delete cookies or whatever
            const info = this._authService.getInfo();
            this._authService.removeToken();
            alert(err.error.message)
            if(info.group == 'admin' || info.group == 'operator') this.router.navigate(['/pages/auth/admin-login'],{queryParams: {returnURL: location.pathname}});
            else this.router.navigate(['/pages/auth/login'],{queryParams: {returnURL: location.pathname}});
            // if you've caught / handled the error, you don't want to rethrow it unless you also want downstream consumers to have to handle it as well.
            return Observable.of(err.message);
        }
        return Observable.throw(err);
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> 
    {
        let token = this._authService.getToken();
        let newHeader: HttpHeaders = req.headers;
        // const ct = req.headers.get('Content-Type');
        // if(token && ct !== 'multipart/form-data' ) newHeader = newHeader.set('Content-Type', 'application/json');
        if(token) newHeader = newHeader.set('Authorization', token);
        const newReq = req.clone({headers: newHeader});
        return next.handle(newReq).catch(err => this.handleAuthError(err));
    }
}