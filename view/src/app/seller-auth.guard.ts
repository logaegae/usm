import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable()
export class SellerAuthGuard implements CanActivate {

  constructor(private router: Router, private _authService: AuthService) {
  }

  canActivate() : Observable<boolean>
  {
    const token = this._authService.getToken();
    if(token)
    {
        return this._authService.authToken(token)
        .pipe(map(response => {
                if(response.success && response.data.auth) 
                {
                    if(response.data.info) this._authService.setInfo(response.data.info);
                    const info = this._authService.getInfo();
                    
                    if(info && info.group && (info.group == 'seller' || info.group == 'visitor'))
                    {
                        return true;
                    }
                    else 
                    {
                        this._authService.removeToken();
                        this.router.navigate(['/pages/auth/login'],{queryParams: {returnURL: location.pathname}});
                        return false;
                    }
                }
                else{
                    this._authService.removeToken();
                    this.router.navigate(['/pages/auth/login'],{queryParams: {returnURL: location.pathname}});
                    return false;
                }
            }

        ));
    }
    else{
        this.router.navigate(['/pages/auth/login'],{queryParams: {returnURL: location.pathname}});
        return Observable.of(false);
    }
  }
}