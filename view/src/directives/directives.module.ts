import { NgModule } from '@angular/core';

import { OnlyNumberDirective } from 'directives/onlyNumber.directive';

@NgModule({
    declarations: [
        OnlyNumberDirective
    ],
    imports     : [],
    exports     : [
        OnlyNumberDirective
    ]
})
export class DirectivesModule
{
}
